FROM node:12.18-alpine
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY ./package.json /usr/src/app
COPY . .
#RUN chmod -R 777 /usr/src/app/node_modules/.bin/react 
#-scripts
EXPOSE 4000
CMD ["npm", "start"]