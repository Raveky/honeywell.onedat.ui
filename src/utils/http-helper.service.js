import axios from 'axios';

const debugMode = false;
let baseURL = '';

if (debugMode) {
  baseURL = `http://internal-uai3027660-fw2-alb-web-dev-lb-303578408.us-east-1.elb.amazonaws.com/api/`
}
else {
  baseURL = `http://localhost:49192/`
}

export default axios.create({
 // baseURL: baseURL,
});