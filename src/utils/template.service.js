import API from './http-helper.service';

export default class TemplateService {

    CommonApiCall(url,data) {
        return API.post(url, data).then(res => {
            return res;
        },
            error => {
                return error;
            }
        );
    }

    getMethod(url) {
        console.log(url)
        return API.get(url).then(res => {
            return res;
        },
            error => {
                return error;
            }
        );
    }
}