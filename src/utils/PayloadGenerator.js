export default class PayloadGenerator {    

    generatePayload(operationType, templateName, tableName, data, type) {
        let today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            let username=localStorage.getItem('username')
        let payload = {};
        payload.OperationType = operationType;

        payload.Messages = [];
        let mesgObj = {
            template_name: templateName,
            table_name: tableName,            
            fields: [],
            filter: []
        };
        if(type === "moduleData"){
            const tableNames = tableName.split(".");
            if (operationType === 'PUT') {
                mesgObj.filter.push({
                    "field_name": tableNames[1] + "_id_sq",
                    "field_type": "int",
                    "field_value": data.render_seq_id.toString()
                })
    
                mesgObj.fields.push({
                    "field_name": tableNames[1] + "_id_sq",
                    "field_type": "int",
                    "field_value": data.render_seq_id.toString()
                })
            }
    
    
            mesgObj.fields.push({
                "field_name": "created_by_user_id",
                "field_type": "string",
                "field_value": username
                // "field_value": props.userName
            });
    
            mesgObj.fields.push({
                "field_name": "created_on_dt",
                "field_type": "datetime",
                "field_value": date
            });
    
            if (tableName === 'catalyst_state') {
                mesgObj.fields.push({
                    "field_name": "DSPLY_ORD_NUM",
                    "field_type": "int",
                    "field_value": "1"
                });
                mesgObj.fields.push({
                    "field_name": "catalyst_state_cd",
                    "field_type": "string",
                    "field_value": "1"
                })
            }
    
            data.fields.forEach(item => {
                if (item.holds_state) {
                    mesgObj.fields.push({
                        "field_name": item.field_col_name,
                        "field_type": item.field_data_type,
                        "field_value": item.field_value
                    });
                }
            });
            data.sections.forEach(secItem => {
                secItem.fields.forEach(item => {
                        if (item.field_col_name) {
                            mesgObj.fields.push({
                                "field_name": item.field_col_name,
                                "field_type": item.field_data_type,
                                "field_value": item.field_value
                            });
                        }
                    
                });
            });
            data.sections.forEach(secItem => {
                secItem.sub_sections.forEach(subItem => {
                    subItem.fields.forEach(item => {
                        if (item.holds_state) {
                            mesgObj.fields.push({
                                "field_name": item.field_col_name,
                                "field_type": item.field_data_type,
                                "field_value": item.field_value
                            });
                        }
                    });
                });
            });
        } else if (type === "deleteRowData"){
            const tableNames = tableName.split(".");
            mesgObj.filter.push({
                "field_name": tableNames[1].toLowerCase() + "_id_sq",
                "field_type": "int",
                "field_value": data.rowData[tableNames[1].toLowerCase() + "_id_sq"].toString()
            });
        }else if (type === "findData"){
            data.fields.forEach(item => {
                if (item.holds_state && item.field_value !== '') {
                    mesgObj.filter.push({
                        "field_name": item.field_col_name,
                        "field_type": item.field_data_type,
                        "field_value": item.field_value
                    });
                }
            });
            data.sections.forEach(secItem => {
                secItem.fields.forEach(item => {
                    if (item.holds_state && item.field_value !== '') {
                        mesgObj.filter.push({
                            "field_name": item.field_col_name,
                            "field_type": item.field_data_type,
                            "field_value": item.field_value
                        });
                    }
                });
            });
            data.sections.forEach(secItem => {
                secItem.sub_sections.forEach(subItem => {
                    subItem.fields.forEach(item => {
                        if (item.holds_state && item.field_value !== '') {
                            mesgObj.filter.push({
                                "field_name": item.field_col_name,
                                "field_type": item.field_data_type,
                                "field_value": item.field_value
                            });
                        }
                    });
                });
            });
        }else if (type === "submitData"){
            // mesgObj = {...mesgObj, ...data}            
        } 
        
        payload.Messages.push(mesgObj);
        
        return payload;

    }
    
}