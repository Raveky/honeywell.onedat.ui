import React, { useState, useEffect } from 'react';
import { Card, Button, Input, Search, Loader, Select } from '@scuf/common';
import { connect } from 'react-redux';
import {
    getTemplateNames, getTemplateVersion, getTemplateDescription, resetTemplate, updateTblName,
    findTemplate, createTemplate, addTechnology
} from '../actions/Template';
import TemplateService from '../utils/template.service';
import GetAllTemplates from '../components/GetAllTemplates';
import { useHistory } from "react-router-dom";
import { ApiUrl } from '../constants'
import PayloadGenerator from '../utils/PayloadGenerator';
import { showGlobalLoader, hideGlobalLoader } from './../actions/Template';

function mapStateToProps(state) {
    const templateState = state.templateReducer;
    console.log('this.props 1', templateState);
    return {
        templateName: templateState.template.template_name,
        templateVersion: templateState.template.version,
        templateDescription: templateState.template.template_description,
        templateTechnology: templateState.template.technology_name,
        showHideFindTable: templateState.showHideFindTable,
        find_results: templateState.find_results,
        templateArray: templateState.template,
        technologyArray: templateState.technologyArray,
        tableValue: templateState.template.tbl_comment_name
    }
}

const mapDispatchToProps = (dispatch) => ({
    getTemplateName: (value) => dispatch(getTemplateNames(value)),
    addTechnology: (value) => dispatch(addTechnology(value)),
    getTemplateVersion: (id) => dispatch(getTemplateVersion(id)),
    getTemplateDescription: (desc) => dispatch(getTemplateDescription(desc)),
    resetTemplate: () => dispatch(resetTemplate()),
    updateTblName: (value) => { dispatch(updateTblName(value)) },
    findTemplate: () => dispatch(findTemplate()),
    createTemplate: (obj) => dispatch(createTemplate(obj)),
    showGlobalLoader: () => dispatch(showGlobalLoader()),
    hideGlobalLoader: () => dispatch(hideGlobalLoader()),
    // updateRenderUrl: (fObject, value)=>dispatch(updateRenderUrl(fObject, value))
});


const templateService = new TemplateService();
const payloadGenerator = new PayloadGenerator();

const CreateTemplateForm = (props) => {
    let history = useHistory();
    const [tempateErr, setTempateErr] = useState("");
    const [table_value, setTableValue] = useState("");
    const [table_results, setTableResult] = useState([]);
    const [tableNames, setTableNames] = useState([]);
    const [templateList, setTemplateList] = useState([]);
    const createTemplateFunction = () => {
        if (!props.templateName) {
            setTempateErr("Please Enter the Template Name!");
        } else {
            setTempateErr("");
            props.createTemplate({
                template_name: props.templateName,
                template_description: props.templateDescription
            });
            // props.getBuild();
            history.push('/buildTemplate')
        }
    }

    const editAction = (id) => {
        history.push(`/buildTemplate/${id}`);
    }

    useEffect(() => {
        props.showGlobalLoader();
        let url = ApiUrl.getTableNames;
        let operationType = "GET"
        const displayData = payloadGenerator.generatePayload(operationType, '', '', null, "getTableNames")
        templateService.CommonApiCall(url, displayData).then((res) => {
            if (res) {
                // res.data.map((item, index) => {
                if (res.data.dataResponse.length === 0) {
                    setTableNames([]);
                } else {
                    let data = [];
                    res.data.dataResponse[0].data.map((item, index) => {
                        if (item.title != null) {
                            data.push(item);
                        }
                        return data;
                    });
                    setTableNames(data);
                    console.log("Module data from API =>", data);
                }
                //  })
            }
        })
            .finally(() => {
                props.resetTemplate();
                props.hideGlobalLoader();
            });
        getListOfTemplates()

    }, [])
    const getListOfTemplates = () => {
        props.showGlobalLoader();
        let displayData = {};
        displayData.OperationType = "GET";
        displayData.Messages = [];
        let mesgObj = {
            template_name: "",
            table_name: "sc_template.template_master",
            fields: []
        };
        displayData.Messages.push(mesgObj);
        let url = ApiUrl.templateAPi
        templateService.CommonApiCall(url, displayData).then((res) => {
            if (res.data.dataResponse.length > 0) {
                let ModifiedArray = res.data.dataResponse[0].data;
                setTemplateList([...ModifiedArray.reverse()])
            } else {
                console.log('error');
            }
        })
            .finally(() => props.hideGlobalLoader());
    }

    const handleSearchChange = (value) => {
        if (tableNames === undefined) {
            setTableNames([]);
        }
        setTableResult([]);
        const data = tableNames.filter(item => item.title.toLowerCase().includes(value.toLowerCase()))
        setTableResult(data);
    }

    const handleSearchSelect = (value) => {
        setTableValue(value.title);
        props.updateTblName(value);
    }
    // console.log('this.props', props)
    const findData = () => {
        let url = ApiUrl.templateAPi
        let displayData = {};
        displayData.OperationType = "GET";
        displayData.Messages = [];
        let mesgObj = {
            template_name: "",
            table_name: "sc_template.template_master",
            fields: [],
            filter: []
        };
        // let arr = templateList.filter((obj) => {
        //     console.log('arr 1',obj);
        //     if ((obj.template_name === props.templateName) || (obj.template_desc === props.templateDescription)) {
        //         return obj
        //     }
        //     // return obj
        // })
        // console.log('arr',arr);
        if (props.templateName !== "") {
            mesgObj.filter.push({
                "field_name": "template_name",
                "field_type": "string",
                "field_value": props.templateName
            });
        }
        if (props.templateDescription !== '') {
            mesgObj.filter.push({
                "field_name": "template_desc",
                "field_type": "string",
                "field_value": props.templateDescription
            });
        }

        displayData.Messages.push(mesgObj);
        templateService.CommonApiCall(url, displayData).then((res) => {
            if (res) {
                console.log('res===', res);
                // this.getListOfTemplates();
                setTemplateList(res.data.dataResponse[0].data)
            }
        });
    }
    const resetTemplate = () => {
        props.resetTemplate()
        getListOfTemplates()
    }
    return (
        <>
            <Card interactive={false}>
                <Card.Header title="Create or Find Template" style={templateFormStyle.headerStyle} />
                <Card.Content >
                    <div>
                        <div className="col-sm-4 d-inline-block align-top">

                            <Input fluid={true} value={props.templateName} style={{ ...templateFormStyle.nameBox, ...templateFormStyle.inputBoxCommonStyles }} type="text" placeholder="Hint Text"
                                label="Template Name" onChange={props.getTemplateName} indicator="required" error={tempateErr} />
                        </div>
                        <div className="col-sm-4 d-inline-block align-top">
                            <Input fluid={true} value={props.templateDescription} style={{ ...templateFormStyle.descriptionBox, ...templateFormStyle.inputBoxCommonStyles }} type="text"
                                placeholder="Hint Text" label="Description" onChange={props.getTemplateDescription} />
                        </div>
                        <div className="col-sm-4 d-inline-block align-top search global-search-type">
                            <label className="create-temp-tab-name"><strong>Module Name</strong><div id="red-batch" className="ui red circular empty label badge  circle-padding"></div></label>
                            <Search fluid={true}
                                results={table_results}
                                value={props.tableValue}
                                onResultSelect={(value) => { handleSearchSelect(value) }}
                                onSearchChange={(value) => { handleSearchChange(value) }}
                            />
                        </div>

                        <div className="col-sm-4 d-inline-block align-down">
                            <Select fluid={true} label="Technology" multiple={false}
                                style={{ ...templateFormStyle.descriptionBox, ...templateFormStyle.inputBoxCommonStyles }}
                                placeholder="Select Technologies" size="small" value={props.templateTechnology}
                                options={props.technologyArray}
                                onChange={
                                    (value) => {
                                        props.addTechnology(value)
                                    }}
                            /></div>

                    </div>
                </Card.Content>
                <Card.Footer style={templateFormStyle.footerButtonStyle}>
                    <Button type="secondary" size="small" content="RESET" onClick={resetTemplate} />
                    <Button type="secondary" size="small" content="FIND" onClick={findData} />
                    <Button type="primary" size="small" content="CREATE" onClick={createTemplateFunction} />
                </Card.Footer>
            </Card>
            <GetAllTemplates editAction={editAction} templateAllList={templateList} ></GetAllTemplates>
        </>
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateTemplateForm);

const templateFormStyle = {
    headerStyle: {
        background: '#80808024',
        padding: '6px 10px',
        fontWeight: 'bold',
        fontSize: '13px'
    },
    footerButtonStyle: {
        justifyContent: 'flex-start'
    },
    nameBox: {
        flex: 2
    },
    versionBox: {
        flex: 1
    },
    descriptionBox: {
        flex: 3
    },
    cardContent: {
        display: 'flex',
        justifyContent: 'space-around',
        gap: '10px'
    },
    inputBoxCommonStyles: {
        //margin: '0px 10px'
    }
}