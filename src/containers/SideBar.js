import React, { Fragment } from 'react';
import HeaderArea from '../components/Header';
import { SidebarLayout, Icon, BadgedIcon, Loader } from '@scuf/common';
import { BrowserRouter as Router } from "react-router-dom";
import { connect } from 'react-redux';
import { Routes, myRoutePath } from '../Routes/Routes';
import { updateActiveSideBar } from '../actions/Template';
import TemplateService from '../utils/template.service';
import { ApiUrl } from '../constants';

function mapStateToProps(state) {
    console.log(state);
    return {
        stateVal: state,
        loading: state.templateReducer.isShowGlobalLoader
    }
}

const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    updateActiveSideBar: (value) => dispatch(updateActiveSideBar(value))
})

class ExampleContainer extends React.Component { 
    constructor(props) {
        super(props);
        this.templateService = new TemplateService();
        this.state = {
            collapsed: true,
            expand: [],
            menuList: []
        };
        this.handleCollapse = this.handleCollapse.bind(this);
        this.handleSubmenuClick = this.handleSubmenuClick.bind(this);
        this.router = undefined;
    }

    componentDidMount() {
        this.fetchInitialData();
    }

    fetchInitialData() {
        let jsonData = {};
        jsonData.OperationType = "GET";
        jsonData.Messages = [];
        let mesgObj = {
            template_name: '',
            table_name: "sc_template.module_master",
            filter: []
        };
        mesgObj.filter.push({
            "field_name": "tech_id",
			"field_type": "int",
			"field_value": "1"
        })
        jsonData.Messages.push(mesgObj);
       // this.props.showGlobalLoader();
        let url=ApiUrl.getModuleConfigurationData
        this.templateService.CommonApiCall(url,jsonData).then((res) => {
            console.log(res);
            if(res.data.dataResponse.length > 0) {
               /*  const keysArray = Object.keys(res.data.dataResponse[0].data[0]);
                res.data.dataResponse[0].data.forEach((dItem, dIndex)=> {
                    keysArray.forEach((item, index) => {
                        res.data.dataResponse[0].data[dIndex][item.toLowerCase()] = res.data.dataResponse[0].data[dIndex][item];
                        delete res.data.dataResponse[0].data[dIndex][item];
                    });
                }); */
                let result = res.data.dataResponse[0].data;
                result = result.reverse();
                let menuDropdown = [];
                result.forEach((item, index) => {
                   const menudd = menuDropdown.filter((menuitem) => menuitem.module_id === item.module_id)
                    if(item.template_id !== null &&  menudd.length === 0) {
                      menuDropdown.push(item)
                    }
                });
                this.props.dispatch({
                    type: 'SET_MODULE_CONFIGURATION',
                    payload: result
                });
                menuDropdown.forEach((mItem, mIndex) => {
                    let resultSet = [];
                    result.forEach((rItem, rIndex) => {
                        const menudd = resultSet.filter((menuitem) => menuitem.sub_module_id === rItem.sub_module_id);
                        if(rItem.template_id !== null && rItem.module_id === mItem.module_id && menudd.length === 0) {
                            resultSet.push(Object.assign({}, rItem));
                        }
                    });
                    menuDropdown[mIndex].subMenuItem = [...resultSet]
                });
                this.setState({ menuList: [...menuDropdown ]})
                console.log('menuList' , this.state.menuList);
            } else {
                console.log('error');
            }
        })
           // .finally(() => this.props.hideGlobalLoader());
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // Do something if any updates
       const moduleBoolean =  sessionStorage.getItem('moduleDef');
        if(moduleBoolean === 'true') {
            sessionStorage.setItem('moduleDef', false);
            this.fetchInitialData();
        }
    }

    handleCollapse() {
        const { collapsed } = this.state;
        this.setState({ collapsed: !collapsed, expand: [] });
    }

    handleItemClick(event, itemProps) {
        const { name } = itemProps;
        // console.log(id);
        this.props.updateActiveSideBar(name);
        if(this.state.menuList.length > 0) {
            this.state.menuList.forEach((mItem, mIndex) => {
                mItem.subMenuItem.forEach((subItem, subIndex) => {
                    if(subItem.sub_module_name === name) {
                        const urlName =  subItem.sub_module_name.split(" ").join("");
                            this.router.history.push(`/${urlName}/${subItem.template_id}`);
                    } else {
                        let currentPath = myRoutePath.find((val, ind) => {
                            return val.path === itemProps.name;
                        });
                        if (currentPath) {
                            this.router.history.push(currentPath.url);
                        }
                    }
                });
            });
        } else {
            let currentPath = myRoutePath.find((val, ind) => {
                return val.path === itemProps.name;
            });
            if (currentPath) {
                this.router.history.push(currentPath.url);
            }
        }
    }

    handleSubmenuClick(event, itemProps) {
        console.log(itemProps)
        const expand = [...this.state.expand];
        const { name } = itemProps;
        const expanderIndex = expand.findIndex(element => element === name);
        if (expanderIndex >= 0) {
            expand.splice(expanderIndex, 1);
        } else {
            expand.push(name);
        }
        this.setState({ collapsed: false, expand });
    }

    goHome() {
        localStorage.setItem("username", '');
    }

    render() {
        const { collapsed, expand } = this.state;
        const activeName = this.props.stateVal.templateReducer.activeSideBar

        const styleSheet = {
            contentStyle: {
                padding: '10px',
                width: '100%',
                height: '100%',
                overflow: 'auto'
            },
            sidebarLayout: {
                flexShrink: '15',
                transition: 'all 1s ease 0s'
            }
        }
        return (
            <Fragment>
                <div className="side-bar-comp" style={{ height: 'calc(100% - 59px)' }}>
                    <Loader loading={this.props.loading} text=" " overlayColor="#a0a0a0" overlayOpacity="0.5">
                        <HeaderArea onMenuToggle={this.handleCollapse} />
                        <SidebarLayout style={{ height: '100%' }}>
                            <SidebarLayout.Sidebar style={collapsed ? styleSheet.sidebarLayout : null}>
                                <SidebarLayout.Sidebar.Item
                                    content="Home"
                                    activeName={activeName}
                                    name="Home"

                                    icon={<BadgedIcon root="common" name="home1" />}
                                    onClick={this.handleItemClick.bind(this)}
                                />

                                <SidebarLayout.Sidebar.Submenu
                                    content="Manage"

                                    activeName={activeName}
                                    name="ManageExpander"
                                    icon={<Icon root="global" name="tools" />}
                                    onClick={this.handleSubmenuClick}
                                    open={expand.includes('ManageExpander')}
                                >
                                    <SidebarLayout.Sidebar.Item
                                        content="User"
                                        activeName={activeName}
                                        name="User"
                                        onClick={this.handleItemClick.bind(this)}
                                    />
                                    <SidebarLayout.Sidebar.Item
                                    content="UOM"
                                    activeName={activeName}
                                    name="UOM"
                                    onClick={this.handleItemClick.bind(this)}
                                />
                                 <SidebarLayout.Sidebar.Item
                                    content="UOM Template"
                                    activeName={activeName}
                                    name="UOMTemplate"
                                    onClick={this.handleItemClick.bind(this)}
                                />
								<SidebarLayout.Sidebar.Item
                                        content="HW Calculations"
                                        activeName={activeName}
                                        name="HW Calculations"
                                        onClick={this.handleItemClick.bind(this)}
                                    />

                                    <SidebarLayout.Sidebar.Item
                                        content="Module Configuration"
                                        activeName={activeName}
                                        name="Module Configuration"
                                        onClick={this.handleItemClick.bind(this)}
                                    />

                                    <SidebarLayout.Sidebar.Item
                                        content="Templates"
                                        activeName={activeName}
                                        name="Templates"
                                        onClick={this.handleItemClick.bind(this)}
                                    />
                                </SidebarLayout.Sidebar.Submenu>
                                    {this.state.menuList.map((menuItem, menuIndex) => {
                                            return <React.Fragment key={menuIndex}>
                                                <SidebarLayout.Sidebar.Submenu content={menuItem.module_name}
                                                activeName={activeName}
                                                name={menuItem.module_name}
                                                icon={<BadgedIcon root="common" name="lights" />}
                                                onClick={this.handleSubmenuClick}
                                                open={expand.includes(menuItem.module_name)}> 
                                                    {menuItem.subMenuItem.map((subMenulist, subMenuIndex) => {
                                                        return <React.Fragment key={subMenuIndex}>
                                                            <SidebarLayout.Sidebar.Item
                                                                content={subMenulist.sub_module_name}
                                                                activeName={activeName}
                                                                name={subMenulist.sub_module_name}
                                                                onClick={this.handleItemClick.bind(this)}>

                                                            </SidebarLayout.Sidebar.Item>
                                                        </React.Fragment>
                                                    })}
                                                </SidebarLayout.Sidebar.Submenu>
                                            </React.Fragment>
                                    })} 
                            </SidebarLayout.Sidebar>
                            <SidebarLayout.Content style={styleSheet.contentStyle}>
                                <Router ref={el => this.router = el}>
                                    <Routes goHome={this.goHome.bind(this)} />
                                </Router>
                            </SidebarLayout.Content>
                        </SidebarLayout>
                    </Loader>
                </div>
            </Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ExampleContainer)
