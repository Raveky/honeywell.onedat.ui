import React from 'react';
import { DataTable } from '@scuf/datatable';
import { Icon, Select, Button } from '@scuf/common';
import { connect } from 'react-redux';
import TemplateService from './../utils/template.service';
import { showGlobalLoader, hideGlobalLoader ,updateActiveSideBar } from './../actions/Template';
import { ApiUrl } from '../constants';
import { myRoutePath } from '../Routes/Routes';
import { BrowserRouter as Router } from "react-router-dom";
import { withRouter } from 'react-router-dom';

let subModuleOptions = [];
let moduleData = [];
let techData = '';
let templateList = [];
let templateVersionOptions = [];
const templateDrivenOptions = [{ value: 'Y', text: 'True' }, { value: 'N', text: 'False' }];
let today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            let username=localStorage.getItem('username')
const Item = DataTable.ActionBar.Item;

const mapStateToProps = (state) => {
    console.log(state);
    return {
        technology: state.templateReducer.template.technology_name,
        reducerList: state.templateReducer
    }
}

const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    showGlobalLoader: ()=>dispatch(showGlobalLoader()),
    hideGlobalLoader: ()=>dispatch(hideGlobalLoader()),
    updateActiveSideBar: (value) => dispatch(updateActiveSideBar(value)),
})

class ModuleConfiguration extends React.Component {
    constructor(props) {
        super(props);
        this.templateService = new TemplateService();
        this.state = {
            selectedRow: null,
            selectedAll: false,
            data: [],
            templateListOptions: []
        }
        // this.router = undefined;
    }

    componentDidMount() {
        techData =  this.props.reducerList.technologyArray.find((item, ind)=> +item.value === +this.props.technology);
        let displayData = {};
        displayData.OperationType = "GET";
        displayData.Messages = [];
        let mesgObj = {
            template_name: "",
            table_name: "sc_template.template_master",
            filter: []
        };
        mesgObj.filter.push({
            "field_name": "tech_id_sq",
            "field_type": "int",
            "field_value": techData.value.toString()
        });
        displayData.Messages.push(mesgObj);
        this.props.showGlobalLoader();
       let url=ApiUrl.templateAPi
        this.templateService.CommonApiCall(url,displayData).then((res) =>{
           if(res.data.dataResponse.length > 0) {
               res.data.dataResponse[0].data.forEach((item, index) => {
                    templateList.push({
                        value: item.template_id_sq,
                        text: item.template_name,
                        version: item.version,
						temp_description: item.template_desc,
                        tbl_name: (item.template_data != null) ? JSON.parse(item.template_data).tbl_comment_name : ""
                    });
               });
               this.setState({templateListOptions: [...templateList]});
               this.initialSetup();
           } else {
               console.log('error');
           }
       }).finally(() => this.props.hideGlobalLoader());
    }

    initialSetup() {
        let dataList = [];
		let subModuleData = [];
        this.props.reducerList.module_definition.forEach((item, index) => {
            const menudd = moduleData.filter((menuitem) => menuitem.value === item.module_id)
            if(menudd.length === 0) {
                moduleData.push({
                    value: item.module_id,
                    text: item.module_name
                });
            }
			subModuleData.push({
                module_id: item.module_id,
                module_name: item.module_name,
                sub_module_id: item.sub_module_id,
                sub_module_name: item.sub_module_name
            });
            if(item.template_id !== null) {
                item.technology =  this.props.reducerList.technologyArray.find((itm, ind)=> +itm.value === +item.tech_id).text;
                item.moduleName =  moduleData.find((itm, ind)=> +itm.value === +item.module_id).text;
                item.subModuleName =  subModuleData.find((itm, ind)=> (+itm.sub_module_id === +item.sub_module_id) && (+itm.module_id === +item.module_id)).sub_module_name;
                item.templateName = templateList.find((itm, ind)=> +itm.value === +item.template_id).text;
                item.templateDescription = templateList.find((itm, ind)=> +itm.value === +item.template_id).temp_description;
                item.isTempDriven = templateDrivenOptions.find((itm, ind)=> itm.value === item.is_template_driven).text;
                dataList.push(item);
            }
        });
        this.setState({data: [...dataList]});
    }

    technologyEditRenderer(cellData) {
        return (
            <>
                <div className="cell-data-wrap">{cellData.value}</div>
            </>
        );
    }
    moduleEditRenderer(cellData) {
        return (
            <>
                {(cellData.selected) ? <div className="ui-cell-data">
                    <Select placeholder="Choose a Module" options={moduleData} search={true} onChange={(chData) => {
                        this.initChange(chData, cellData)
                    }} />
                </div> : <div className="cell-data-wrap">{cellData.value}</div>}
            </>
        );
    }
    templateEditRenderer(cellData) {
        return (
            <>
                {(cellData.selected && cellData.rowData.isTempDriven === 'True') ? <div className="ui-cell-data">
                    <Select placeholder="Choose a Template" options={this.state.templateListOptions} search={true} onChange={(chData) => {
                        this.initChange(chData, cellData)
                    }} />
                </div> : <div className="cell-data-wrap">{cellData.value}</div>}
            </>
        );
    }
	templateDescRenderer(cellData) {
        return (
            <>
                <div className="cell-data-wrap">{cellData.value}</div>
            </>
        );
    }
    submoduleEditRenderer(cellData) {
        return (
            <>
                {(cellData.selected) ? <div className="ui-cell-data">
                    <Select placeholder="Choose a Sub Module" options={subModuleOptions} search={true} onChange={(chData) => {
                        this.initChange(chData, cellData)
                    }} />
                </div> : <div className="cell-data-wrap">{cellData.value}</div>}
            </>
        );
    }

    templateVersionEditRenderer(cellData) {
        return (
            <>
                {(cellData.selected && cellData.rowData.isTempDriven === 'True') ? <div className="ui-cell-data">
                    <Select placeholder="Choose a Template Version" options={templateVersionOptions} search={true} onChange={(chData) => {
                        this.initChange(chData, cellData)
                    }} />
                </div> : <div className="cell-data-wrap">{cellData.value}</div>}
            </>
        );
    }

    templateDrivenEditRender(cellData) {
        return (
            <>
                {(cellData.selected) ? <div className="ui-cell-data">
                    <Select placeholder="" options={templateDrivenOptions} search={true} onChange={(chData) => {
                        this.initChange(chData, cellData)
                    }} />
                </div> : <div className="cell-data-wrap">{(cellData.value === 'Y')? 'True' : 'False'}</div>}
            </>
        );
    }

    initChange(chData, cellData) {
        let activeVal = '';
        //let curIndex =  this.state.data.findIndex((val, ind)=> chData == val.id)
        switch(cellData.field){
            case "moduleName": 
               let module =  moduleData.find((item, ind)=> +item.value === chData);
                // let resultSet = [];
                subModuleOptions = [];
                this.props.reducerList.module_definition.forEach((rItem, rIndex) => {
                    const menudd = subModuleOptions.filter((menuitem) => +menuitem.value === +rItem.sub_module_id);
                    if(+rItem.module_id === +module.value && menudd.length === 0) {
                        subModuleOptions.push({
                            value: rItem.sub_module_id,
                            text: rItem.sub_module_name
                        });
                    }
                });
               activeVal = module.text;
                break;
            case "templateName": 
               let template =  this.state.templateListOptions.find((item, ind)=> +item.value === chData);
               activeVal = template.text;
				templateVersionOptions = [];
              this.state.templateListOptions.forEach((rItem, rIndex) => {
                if(+rItem.value === +template.value) {
                    templateVersionOptions.push({
                        value: rItem.version,
                        text: rItem.version
                    });
					cellData.templateDescription = rItem.temp_description;
                    cellData.rowData.templateDescription = rItem.temp_description;
                }
            });
                break;
            case "subModuleName": 
               let subMod =  subModuleOptions.find((item, ind)=> +item.value === chData);
			    const getNewTemplateList = templateList.filter((menuitem) => menuitem.tbl_name === subMod.text);
                this.setState({templateListOptions: [...getNewTemplateList]})
               activeVal = subMod.text;
                break;
            case "template_version": 
               let tempVer =  templateVersionOptions.find((item, ind)=> +item.value === chData);
               activeVal = tempVer.text;
                break;
            case "isTempDriven": 
               let tempDriven = templateDrivenOptions.find((item, ind)=> item.value === chData);
               activeVal = tempDriven.text;
                break;
            default: 
        }
        cellData.rowData[cellData.field] = activeVal;
        let tempData = this.state.data;
        tempData[cellData.rowIndex] = cellData.rowData
        this.setState({
            data: [...tempData]
        })  
        console.log(cellData);
    }

    editData(newVal, cellData) {
        console.log(newVal, cellData)
        const data = this.state.data;
        const status = newVal ? 'Active' : 'Retired';
        data[cellData.rowIndex].status = status;
        this.setState({ data: data })
    }

    onAdd(pilot, status) {
        this.setState({
            data: [{ id: this.state.data.length + 1, technology: techData.text}, ...this.state.data]
        });
    }

    isEdittableField (cellData){
        console.log(cellData)
        return true;
    }

    submitModuleDefinition() {
        console.log(this.state.selectedRow);
        let moduleDefData = {};
        moduleDefData.OperationType = 'PUT';
        moduleDefData.Messages = [];
        let mesgObj = {
            template_name: "",
            table_name: "sc_template.module_master",
            fields: [],
            filter: []
        };
        this.state.selectedRow.forEach((rowItem, index) => {
            console.log(rowItem);
                mesgObj.filter.push({
                    "field_name": "tech_id",
                    "field_type": "int4",
                    "field_value": this.props.reducerList.technologyArray.find((item, ind)=> item.text === rowItem.technology).value.toString()
                });
                mesgObj.filter.push({
                    "field_name": "module_id",
                    "field_type": "int4",
                    "field_value": moduleData.find((item, ind)=> item.text === rowItem.moduleName).value.toString()
                });
               
                mesgObj.filter.push({
                    "field_name": "sub_module_id",
                    "field_type": "int4",
                    "field_value": subModuleOptions.find((item, ind)=> item.text === rowItem.subModuleName).value.toString()
                });
              
                mesgObj.fields.push({
                    "field_name": "is_template_driven",
                    "field_type": "varchar",
                    "field_value": (rowItem.isTempDriven === 'True')? 'Y': 'N'
                });
                mesgObj.fields.push({
                    "field_name": "template_id",
                    "field_type": "int4",
                    "field_value": this.state.templateListOptions.find((item, ind)=> item.text === rowItem.templateName).value.toString()
                });
                mesgObj.fields.push({
                    "field_name": "template_version",
                    "field_type": "float8",
                     "field_value": rowItem.template_version.toString()
                });
                mesgObj.fields.push({
                    "field_name": "active_ind",
                    "field_type": "varchar",
                    "field_value": 'Y'
                });
                mesgObj.fields.push({
                    "field_name": "updated_by_user_id",
                    "field_type": "string",
                    "field_value": username
                });
                mesgObj.fields.push({
                    "field_name": "updated_on_dt",
                    "field_type": "datetime",
                    "field_value": date
                });
        })

        moduleDefData.Messages.push(mesgObj);
        console.log(moduleDefData);
        let url = ApiUrl.getModuleConfigurationData;
        this.templateService.CommonApiCall(url, moduleDefData).then((res) => {
            console.log(res);
            if(res.data) {
                sessionStorage.setItem('moduleDef', true);
                let getName = myRoutePath.find((v, i) => v.url === '/');
                this.props.updateActiveSideBar(getName.path);
                this.props.history.push(getName.url);
            } else {
                console.log('error');
            }
        });
    }

    goToHome() {
        sessionStorage.setItem('moduleDef', true);
        let getName = myRoutePath.find((v, i) => v.url === '/');
        this.props.updateActiveSideBar(getName.path);
        this.props.history.push(getName.url);
    }

    render() {
        return (
            <>
                <div className="module-config-title">Menu Configuration</div>
                <DataTable
                    data={this.state.data}
                    selection={this.state.selectedRow}
                    selectionMode="multiple"
                    onSelectAll={(e) => this.setState({ selectedAll: e })}
                    onSelectionChange={(e) => { console.log(e); this.setState({ selectedRow: e }) }}
                    onEdit={(newData, cellData) => this.editData(newData, cellData)}
                    className="module-conf-table"
                >
                    <DataTable.HeaderBar>
                        <Item
                            content="Add"
                            iconSize="small"
                            icon="slidercontrols-plus"
                            onClick={() => this.onAdd()}
                            className="module-config-action-bar"
                        />
                        <Item
                            icon={<Icon name="edit" size="small" root="common" />}
                            content="Edit"
                            className="module-config-action-bar"
                        />
                    </DataTable.HeaderBar>

                    <DataTable.Column field="technology" header="Technology" editable={true} customEditRenderer={(cellData) => this.technologyEditRenderer(cellData)} />
                    <DataTable.Column field="moduleName" header="Module Name" editable={true} customEditRenderer={(cellData) => this.moduleEditRenderer(cellData)} />
                    <DataTable.Column field="subModuleName" header="Sub Module Name" editable={true} customEditRenderer={(cellData) => this.submoduleEditRenderer(cellData)} />
                    <DataTable.Column field="isTempDriven" header="Is Template Driven" editable={true} customEditRenderer={(cellData) => this.templateDrivenEditRender(cellData)} />
                    <DataTable.Column field="templateName" header="Template Name" editable={true} customEditRenderer={(cellData) => this.templateEditRenderer(cellData)} />
					<DataTable.Column field="templateDescription" header="Template Description" editable={true} customEditRenderer={(cellData) => this.templateDescRenderer(cellData)} />
                    <DataTable.Column field="template_version" header="Template Version" editable={true} customEditRenderer={(cellData) => this.templateVersionEditRenderer(cellData)} />
                </DataTable>
                <br />
                <Button type="primary" size="small" content="SUBMIT" onClick={this.submitModuleDefinition.bind(this)}/>
                <Button type="primary" size="small" content="BACK" onClick={this.goToHome.bind(this)}/>
            </>
        )
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ModuleConfiguration));