import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Grid, Icon, Card } from '@scuf/common';
import Draggable from 'react-draggable';
import { Header } from '@scuf/common';
import { Accordion } from '@scuf/common';


const GridComponent = () => {
    const [array, setArray] = useState([0, 1, 2, 3, 5])
    return (
        <div>
            <Header title="Manage Catalyst Storage Location" menu={true}>
            </Header>
            <h1>Manage Catalyst Storage Location</h1>
            <Accordion>
                <Accordion.Content title="Catalyst Storage Location Details">
                    <Grid >
                        <Card>
                            {/* <Card.Header> Section1</Card.Header> */}
                            <Grid.Row >
                                <Grid.Column width={6} sWidth={4} mOrder={2}>
                                    <Card>
                                        <Card.Header> Section 1</Card.Header>
                                        {
                                            array && array.length ? array.map((arr, i) => {
                                                return (
                                                    <Draggable
                                                        handle=".handle"
                                                        zIndex={100}
                                                        defaultPosition={{ x: 0, y: 0 }}
                                                        // deposition={{x: 0, y: 0}}
                                                        grid={[25, 25]}
                                                        scale={1}
                                                        onStart={(e) => console.log('e', e)}
                                                        onDrag={(e, position) => console.log('e', position)}
                                                        onStop={(e) => console.log('e', e)}
                                                    >
                                                        <Card.Content className="handle drag-cursor" key={i}>
                                                            Proin leo ligula, volutpat molestie dolor quis.
                                                    </Card.Content>
                                                    </Draggable>

                                                )
                                            }) : null
                                        }
                                    </Card>
                                </Grid.Column>
                                <Grid.Column width={6} sWidth={4} mOrder={2}>
                                    <Card>
                                        <Card.Header> Section 2</Card.Header>
                                        {
                                            array && array.length ? array.map((arr, i) => {
                                                return (
                                                    <Draggable
                                                        handle=".handle"
                                                        zIndex={100}
                                                        defaultPosition={{ x: 0, y: 0 }}
                                                        position={null}
                                                        grid={[25, 25]}
                                                        scale={1}
                                                        onStart={(e) => console.log('e', e)}
                                                        onDrag={(e) => console.log('e', e)}
                                                        onStop={(e) => console.log('e', e)}>
                                                        <Card.Content className="handle drag-cursor" key={i}>
                                                            Proin leo ligula, volutpat molestie dolor quis.
                                                    </Card.Content>
                                                    </Draggable>

                                                )
                                            }) : null
                                        }
                                    </Card>
                                </Grid.Column>
                            </Grid.Row>
                        </Card>

                    </Grid>
                    <Grid >
                        <Card>
                            <Card.Header> Section2</Card.Header>
                            <Grid.Row >
                                <Grid.Column width={6} sWidth={4} mOrder={2}>
                                    <Card>
                                        {
                                            array && array.length ? array.map((arr, i) => {
                                                return (
                                                    <Draggable
                                                        handle=".handle"
                                                        zIndex={100}
                                                        defaultPosition={{ x: 0, y: 0 }}
                                                        position={null}
                                                        grid={[25, 25]}
                                                        scale={1}
                                                        onStart={(e) => console.log('e', e)}
                                                        onDrag={(e) => console.log('e', e)}
                                                        onStop={(e) => console.log('e', e)}>
                                                        <Card.Content className="handle drag-cursor" key={i}>
                                                            Proin leo ligula, volutpat molestie dolor quis.
                                                    </Card.Content>
                                                    </Draggable>

                                                )
                                            }) : null
                                        }
                                    </Card>
                                </Grid.Column>
                            </Grid.Row>
                        </Card>

                    </Grid>
                    <Grid >
                        <Card>
                            <Card.Header> Section3</Card.Header>
                            <Grid.Row >
                                <Grid.Column width={6} sWidth={12} mOrder={2}>
                                    <Card>
                                        {
                                            array && array.length ? array.map((arr, i) => {
                                                return (
                                                    <Draggable
                                                        handle=".handle"
                                                        zIndex={100}
                                                        defaultPosition={{ x: 0, y: 0 }}
                                                        position={null}
                                                        grid={[25, 25]}
                                                        scale={1}
                                                        onStart={(e) => console.log('e', e)}
                                                        onDrag={(e) => console.log('e', e)}
                                                        onStop={(e) => console.log('e', e)}>
                                                        <Card.Content className="handle drag-cursor" key={i}>
                                                            Proin leo ligula, volutpat molestie dolor quis.
                                                    </Card.Content>
                                                    </Draggable>

                                                )
                                            }) : null
                                        }
                                    </Card>
                                </Grid.Column>
                            </Grid.Row>
                        </Card>

                    </Grid>

                </Accordion.Content>
            </Accordion>

        </div>
    );
};

GridComponent.propTypes = {};

export default GridComponent;