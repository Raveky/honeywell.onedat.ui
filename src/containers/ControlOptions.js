import React from 'react';
import { Icon, Popup } from '@scuf/common';

const ControlOptions = (props) => {
    const controlsArr = [
        {
            name: 'dvr',
            root: 'common',
            spanName: 'Textbox',
            key: 'TextBox',
            // type: "string"
        },
        {
            name: 'calendar',
            root: 'global',
            spanName: 'Date & Time',
            key: 'DateTimePicker',
            // type: "datetime"
        },
        {
            name: 'search',
            root: 'common',
            spanName: 'Lookup',
            key: 'Lookup',
            // type: "string"
        },
        {
            name: 'dash',
            root: 'global',
            spanName: 'Table',
            key: 'Table',
            // type: "string"
        },
        {
            name: 'message',
            root: 'building',
            spanName: 'Comment Box',
            key: 'Comments',
            // type: "string"
        },
        {
            name: 'measure',
            root: 'common',
            spanName: 'UOM',
            key: 'UOM',
            // type: "string"
        },
        {
            name: 'calendar',
            root: 'global',
            spanName: 'Date',
            key: 'DatePicker',
            // type: "date"
        },
        {
            name: 'menu-dropdown',
            root: 'common',
            spanName: 'Drop Down',
            key: 'Drop Down',
            // type: "string"
        },
        {
            name: 'menu-dropdown',
            root: 'common',
            spanName: 'Radio Button',
            key: 'Radio Button',
            // type: "string"
        },
        {
            name: 'menu-dropdown',
            root: 'common',
            spanName: 'Toggle',
            key: 'Toggle',
            // type: "string"
        },
        {
            name: 'tag',
            root: 'common',
            spanName: 'Label',
            key: 'Label',
            // type: "string"
        }
        ,
        {
            name: 'close-circled',
            root: 'building',
            spanName: 'Button',
            key: 'Button',
            // type: "string"
        },
        {
            name: 'close-circled',
            root: 'building',
            spanName: 'Action Buttons',
            key: 'Action Buttons',
            // type: "string"
        },
        {
            name: 'menu-dropdown',
            root: 'common',
            spanName: 'Triple Dropdowns',
            key: 'Triple Dropdowns',
            // type: "string"
        },
        {
            name: 'flight-attendant',
            root: 'aero',
            spanName: 'Flyout',
            key: 'Flyout'
        },
        {
            name: 'display',
            root: 'common',
            spanName: 'Popup',
            key: 'Popup'
        },
        {
            name: 'menu-dropdown',
            root: 'common',
            spanName:'MultiDropDown',
            key: 'MultipleDropDown',
            // type: "string"
        },
        {
            name: 'link',
            root: 'common',
            spanName: 'link',
            key: 'link',
            //  type: "string"
        }
     
    ]

    const controlClick = (e, key) => {
        e.stopPropagation();
        console.log(props);
        console.log("key", key);
        if(key === 'Table') {
            //props.updateTableFlyoutVisibility(true, props.addSubSections);
            props.assignAddTableFunctionality(props.addSubSections)
            props.updateTableFlyoutVisibility(true);            
            console.log('table');
        } else {
            props.addSubSections(key);
        }
    }

    const locStyle = {
        marginTop: props.setStyle.top,
        right: props.setStyle.right
    }

    return (

        <>
            <div style={{ ...controlStyle.controlPageCover, ...locStyle }}>
                <div style={controlStyle.triangleBox}></div>
                {
                    controlsArr.map((i, v) => {
                        return (<div style={controlStyle.individualControlCover} key={v} onClick={(e) => {
                            controlClick(e,i.key)
                        }}>
                            <Icon style={controlStyle.iconStyle} name={i.name} size="small" root={i.root} />
                            <span style={controlStyle.nameAlign}>{i.spanName} </span>
                        </div>)
                    })
                }
            </div>
        </>
    )
}

export default ControlOptions;
const controlStyle = {
    controlPageCover: {
        display: 'flex',
        position: 'absolute',
        background: 'white',
        right: '23px',
        width: '250px',
        flexWrap: 'wrap',
        padding: '10px',
        marginTop: '31px',
        boxShadow: 'grey 0px 0px 8px 1px',
        zIndex: '999',
        fontWeight: 'normal',
        color: 'black'
    },
    iconStyle: {
        marginTop: '2px'
    },
    individualControlCover: {
        width: '50%',
        cursor: 'pointer'
    },
    nameAlign: {
        padding: '0px 5px',
        fontSize: '10px'
    },
    triangleBox: {
        position: 'absolute',
        width: 0,
        height: 0,
        borderLeft: '10px solid transparent',
        borderRight: '10px solid transparent',
        borderBottom: '10px solid white',
        top: '-10px',
        right: '53px'
    }
}

