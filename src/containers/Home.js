import React, { useEffect, useState } from 'react';
import { Icon } from '@scuf/common';
import TechnologySelectionModel from '../components/TechnologySelectionModel';
import { connect } from 'react-redux';
import { addTechnology, showGlobalLoader, hideGlobalLoader  } from '../actions/Template';
import TemplateService from '../utils/template.service';
import PayloadGenerator from '../utils/PayloadGenerator';
import { ApiUrl } from '../constants';

function mapStateToProps(state) {
    const templateState = state.templateReducer;
    return {
        tempArray: state,
        templateTechnology: templateState.template.technology_name
    }
}
const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    addTechnology: (value) => dispatch(addTechnology(value)),
    showGlobalLoader: ()=>dispatch(showGlobalLoader()),
    hideGlobalLoader: ()=>dispatch(hideGlobalLoader())


});

const payloadGenerator = new PayloadGenerator();

const Home = (props) => {
    const colorArr = ["#50c9bd", "#f1a686", "#7870a4", "#f6d598", "#596174", "#e88788", "#6acddb", "#dcdcc4", "#9fdfff", "#4ca5c5", "#ffe6ae", "#de75c7"];
    const [isTechnologyModelShow, setIsTechnologyModelShow] = useState(false)
    const [selectedTechnology, setSelectedTechnology] = useState([]);
    const [techErr, setTechErr] = useState("");
    const [techArray, setTechArray] = useState([]);
    const [technologyDropdownArr, setTechnologyDropdownArr] = useState([]);
    const templateService = new TemplateService();
    const [loading, setLoading] = useState(false);
    const updateTechnologyModelVisibility = () => {
        setIsTechnologyModelShow(false);
        localStorage.setItem("showPopup", "false");
    }

    const reportsArray = [];
    reportsArray.length = 9;
    reportsArray.fill("Report Name Sample");
    const graphsArray = [];
    graphsArray.length = 9;
    graphsArray.fill("Graph Name Sample");
    const getRandomInt = (max) => {
        return Math.floor(Math.random() * Math.floor(max));
    }

const mapTechnologyArray = (respData) =>{
        setTechArray(respData);
        if (Array.isArray(respData)) {
            let techDropArr = [];
            respData.forEach((val, ind) => {
                console.log('home page////////', val);
                techDropArr.push({ text: val.tech_desc, value: val.tech_id_sq })
                //sessionArr.push(val.tech_desc);
            });
            props.dispatch({
                type: 'UPDATE_TECHNOLOGY',
                value: techDropArr
            })
            setTechnologyDropdownArr(techDropArr)

        }
    }

    useEffect(() => {
          let data = {
              "OperationType": "GET",
              "Messages": [
                  {
                      "template_name": "",
                      "table_name": "sc_template.tech_master",
                      "filter": [
  
                      ]
                  }
              ]
          }
           if (sessionStorage.getItem("techDropArr") === null) {
              let url = ApiUrl.oneDatPostMethod
              props.showGlobalLoader()
              
              templateService.CommonApiCall(url, data).then((res) => {
                  if (res) {
                      /* const keysArray = Object.keys(res.data.dataResponse[0].data[0]);
                      res.data.dataResponse[0].data.forEach((dItem, dIndex)=> {
                          keysArray.forEach((item, index) => {
                              res.data.dataResponse[0].data[dIndex][item.toLowerCase()] = res.data.dataResponse[0].data[dIndex][item];
                              delete res.data.dataResponse[0].data[dIndex][item];
                          });
                      }); */
                      //let sessionArr = [];
                      let respData = res.data.dataResponse[0].data;
                      console.log(respData)
                      sessionStorage.setItem("techDropArr", JSON.stringify(respData))
                      mapTechnologyArray(respData);
                      console.log(technologyDropdownArr)
                      isShowTechnologyPop();
                  }
              }).finally(() => {
                  props.hideGlobalLoader()
                  console.log('');
                  
                  
                
              })
          } else {
              
              let sessionArr = sessionStorage.getItem("techDropArr");
              let respData = JSON.parse(sessionArr);
              console.log("respData=>", respData);
              mapTechnologyArray(respData);
              isShowTechnologyPop();
          }
      }, []);
  
      const isShowTechnologyPop = ()=> {
          const checkValue = localStorage.getItem("showPopup");
          if (checkValue === 'true' && selectedTechnology.length === 0) {
              setIsTechnologyModelShow(true);
          } else {
              setIsTechnologyModelShow(false);
          }
      } 


    const closeModel = (data) => {

        if (data === null) {
            setTechErr("Please Enter the Technology!");
        }
        else {

            setTechErr("");
            console.log("data =>", data);
            updateTechnologyModelVisibility();
            //setSelectedTechnology(prevNames => [...prevNames, ...data]);        
            setSelectedTechnology(data);
            console.log(data)
            console.log(selectedTechnology);
            props.addTechnology(data);
        }


    }
    return (
        <div>
            <TechnologySelectionModel isTechnologyModelShow={isTechnologyModelShow} closeModel={closeModel} techErr={techErr}  technologyDropdownArr={technologyDropdownArr}/>



            <div className="home-technical-part">
                <p className="home-main-title">Technologies</p>

                {
                    techArray.map((i, v) => {
                        return (
                            <div key={v} className="home-technical-tab-each" style={{ borderLeft: '4px solid ' + colorArr[getRandomInt(colorArr.length)] + '' }}>
                                {(props.templateTechnology === i.tech_id_sq || selectedTechnology === i.tech_id_sq) ? <Icon className="home-technology-green-check" root="common" name="badge-check" size="small" color="white" /> : ""}
                                <div className="home-tech-title">{i.tech_desc}</div>
                                <div className="home-tech-desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                            </div>
                        )
                    })
                }
            </div>
            <div className="home-reports-part">
                <div className="home-main-title reports-cover">
                    <div>Reports</div>
                    <div className="home-main-title-view-all">View All
                    <Icon root="building" name="caret-down" size="small" color="#1274be" /></div>
                </div>

                <div className="home-reports-tab-each home-reports-add">
                    <Icon className="home-reports-ico" root="building" name="slider-controls-plus" size="medium" color="#4caae9" />
                    <div className="home-reports-title">Add Report</div>
                </div>
                {/* {
                    reportsArray.map((i, v) => {
                        return (
                            <div key={v} className="home-reports-tab-each" style={{ borderLeft: '4px solid ' + colorArr[getRandomInt(colorArr.length)] + '' }}>
                                <Icon className="home-reports-ico" root="building" name="document" size="medium" />
                                <div className="home-reports-title">{i}</div>
                            </div>
                        )
                    })
                } */}
            </div>
            <div className="home-graphs-part">
                <div className="home-main-title reports-cover">
                    <div>Graphs</div>
                    <div className="home-main-title-view-all">View All
                    <Icon root="building" name="caret-down" size="small" color="#1274be" /></div>
                </div>

                <div className="home-reports-tab-each home-reports-add">
                    <Icon className="home-reports-ico" root="building" name="slider-controls-plus" size="medium" color="#4caae9" />
                    <div className="home-reports-title">Add Graphs</div>
                </div>
                {
                    graphsArray.map((i, v) => {
                        return (
                            <div key={v} className="home-reports-tab-each" style={{ borderLeft: '4px solid ' + colorArr[getRandomInt(colorArr.length)] + '' }}>
                                <Icon className="home-reports-ico" root="building" name="graph-alt" size="medium" />
                                <div className="home-reports-title">{i}</div>
                            </div>
                        )
                    })
                }
            </div>

        </div>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);