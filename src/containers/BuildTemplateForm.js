import React from 'react';
import { Card, Button, Icon, Search, Loader , Radio } from '@scuf/common';
import ControlOptions from './ControlOptions';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
    getSectionName, addSection, deleteSection, toggleControls,
    resetTemplateBuild, updateTableFlyoutVisibility, handleSearchSelect,
    handleSearchChange, addResult, updateActiveSideBar, addTemplateColData, updateTemplateStatus, updateHoldstate
} from '../actions/Template';
import Sections from '../components/Sections';
import Fields from '../components/Fields';
import TableNameFlyOut from '../components/Flyouts/TableNameFlyOut';
import TemplateGrid from '../components/TemplateGrid';
import TemplateService from '../utils/template.service';
import { withRouter } from 'react-router-dom';
import { myRoutePath } from '../Routes/Routes';
import ConfigurationModal from '../components/ConfigurationModal';
import PayloadGenerator from '../utils/PayloadGenerator';
import { ApiUrl } from '../constants';
import { showGlobalLoader, hideGlobalLoader } from './../actions/Template';


function mapStateToProps(state) {
    //   console.log(state);
    const templateState = state.templateReducer;
    return {
        sections: templateState.sections,
        templateArray: templateState.template,
        results: templateState.results,
        value: templateState,
        sampleData: templateState.sampleData
    }
}


const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    getSectionName: (value) => dispatch(getSectionName(value)),
    addSection: () => {
        dispatch(addSection())
        dispatch(addTemplateColData())
    },
    deleteSection: (v, e) => {
        e.stopPropagation();
        dispatch(deleteSection(v))
    },
    toggleControls: (index, e) => {
        console.log(index, e);
        e.stopPropagation();
        dispatch(toggleControls(index))
    },
    resetTemplate: () => dispatch(resetTemplateBuild()),
    updateTableFlyoutVisibility: (flag) => {
        dispatch(updateTableFlyoutVisibility(flag))
    },
    handleSearchSelect: (result) => dispatch(handleSearchSelect(result)),
    handleSearchChange: (value) => dispatch(handleSearchChange(value)),
    addResult: (value) => dispatch(addResult(value)),
    updateActiveSideBar: (value) => dispatch(updateActiveSideBar(value)),
    updateTemplateStatus: (fObject, value) => dispatch(updateTemplateStatus(fObject, value)),
    updateHoldstate: (fObject, value) => dispatch(updateHoldstate(fObject, value)),
    showGlobalLoader: () => dispatch(showGlobalLoader()),
    hideGlobalLoader: () => dispatch(hideGlobalLoader()),
});

const payloadGenerator = new PayloadGenerator();
class BuildTemplateForm extends React.Component {

    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props);
        this.templateService = new TemplateService();
        console.log("Props =>", this.props);
        this.state = {
            hasError: false,
            showConfigurationModal: false,
            id: this.props.id,
            templateList: [],
            columnList: []
        }
    }
    componentDidMount() {
        // To get dropdown values of select
        this.getDropdownValues();
        this.resetTemplate();

    }
    getDropdownValues = () => {
        this.props.showGlobalLoader();

        let sectionDropdown = {};
        sectionDropdown.OperationType = "GET";
        sectionDropdown.Messages = [];
        let mesgObj = {
            template_name: '',
            table_name: "sc_template.section_master",
            filter: []
        };
        sectionDropdown.Messages.push(mesgObj);
        let url = ApiUrl.sectionMasterDropdown
        this.templateService.CommonApiCall(url, sectionDropdown).then((res) => {
            if (res) {
                if (res && res.data.dataResponse && res.data.dataResponse[0].data) {
                    let respData = res.data.dataResponse[0].data;
                    let dataArray = [];
                    let tempArr = [];
                    respData.forEach((value, ind) => {
                        if (tempArr.indexOf(value.section_name) === -1) {
                            tempArr.push(value.section_name)
                            dataArray.push({ title: value.section_name })
                        }
                        //console.log("respData =>", respData);                       
                    })
                    this.props.dispatch({
                        type: 'SELECT_MASTER_DROPDOWN',
                        payload: dataArray
                    })
                }

            }
        }).finally(() => this.props.hideGlobalLoader());

    }
    configure() {
        let hasError = false;
        console.log();
        if (this.props.templateArray.sections.length > 0 || this.props.templateArray.fields.length > 0) {
            this.props.templateArray.sections.forEach(secItem => {
                if (secItem.section_name !== "Template Grid") {
                    secItem.fields.forEach((item, index) => {
                        console.log(item, !item.field_label)
                        if (!item.field_label) {
                            hasError = true
                        }
                        if (item.field_type !== "Triple Dropdowns" && item.field_type !== "Label" && item.field_type !== "Action Buttons") {
                            this.props.updateHoldstate(item, true)
                        }
                    })
                }

            })

            this.props.templateArray.sections.forEach(secItems => {
                secItems.sub_sections.forEach(sub_item => {
                    sub_item.fields.forEach(field => {
                        if (!field.field_label) {
                            hasError = true

                        }

                    })
                })
            })
            this.props.templateArray.fields.forEach(fieldIt => {
                //console.log(item, !item.field_label )
                if (!fieldIt.field_label) {
                    hasError = true

                }
                if (fieldIt.field_type !== "Triple Dropdowns" && fieldIt.field_type !== "Label" && fieldIt.field_type !== "Action Buttons") {
                    this.props.updateHoldstate(fieldIt, true)
                }
            })

            if (!hasError) {
                this.setState(
                    {
                        showConfigurationModal: true, hasError: false
                    })

            } else {
                this.setState(
                    {
                        showConfigurationModal: false, hasError: true
                    })
            }

        }
    }

    closeConfigurationModal = () => {
        this.setState({
            showConfigurationModal: false,
            flag: true
        },
            () => this.props.dispatch({
                type: 'TEMPLATE_ERROR',
                payload: this.state.flag
            }))
    }

    resetTemplate = () => {
        this.setState({ hasError: false })
        this.props.resetTemplate();

    }

    //setLoading has been changed to reducer call showGlobalLoader()
    submitData = (val, method = "save") => {
        if (this.state.id && val === 'preview') {
            this.gotoHomePage(val);
            return;
        }

        console.log("val =>", val, method);
        this.setState({ showConfigurationModal: false });
        let techData =  this.props.value.technologyArray.find((item, ind)=> +item.value === +this.props.templateArray.technology_name);
        this.props.showGlobalLoader();
        if (val === "module") {
            console.log("module type");
            this.props.updateTemplateStatus(this.props.templateArray, true);
        }
        let today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        let displayData = {};
        displayData.OperationType = "GET";
        displayData.Messages = [];
        let mesgObj = {
            template_name: "",
            table_name: "template_master"
        };
        displayData.Messages.push(mesgObj);
        let url = ApiUrl.getTableColNames;
        console.log("displayData =>", displayData);
        this.templateService.CommonApiCall(url, displayData).then((res) => {
            if (res.data.dataResponse.length > 0) {
                let data = [];
                res.data.dataResponse[0].data.map((item, index) => {
                    if (item.title != null) {
                        data.push(item);
                    }
                    // data.push(item);
                    return data;
                });
                this.setState({ columnList: [...this.state.columnList, ...data] })
            }


            let submitData = {};
            if (method === 'edit') {
                submitData.OperationType = "PUT";
            } else {
                submitData.OperationType = "POST";
            }

            submitData.Messages = [];
            let mesgObj = {
                template_name: "",
                table_name: "sc_template.template_master",
                fields: [],
                filter: []
            };
            if (this.state.id) {
                //console.log("templateState =>", this.props.templateArray);
                submitData.OperationType = "PUT";
                //return;
                mesgObj.filter.push({
                    "field_name": "template_id_sq",
                    "field_type": "int",
                    "field_value": this.state.id.toString()
                });
            }
            let username=localStorage.getItem('username');
            console.log("Col List =>",this.state.columnList);
            this.state.columnList.forEach((item, index) => {
                console.log('item',item);

                switch (item.columnname) {
                    case 'module_id':
                        mesgObj.fields.push({
                            "field_name": item.columnname,
                            "field_type": item.datatype,
                            "field_value": '0'.toString()
                        });
                        return mesgObj;
                    case 'tech_id_sq':
                        mesgObj.fields.push({
                            "field_name": item.columnname,
                            "field_type": item.datatype,
                            "field_value": techData.value.toString()
                        });
                        return mesgObj;
                    case 'version':
                        mesgObj.fields.push({
                            "field_name": item.columnname,
                            "field_type": item.datatype,
                            "field_value": this.props.templateArray.version
                        });
                        return mesgObj;
                    case 'template_name':
                        mesgObj.fields.push({
                            "field_name": item.columnname,
                            "field_type": item.datatype,
                            "field_value": this.props.templateArray.template_name
                        });
                        return mesgObj;
                    case 'template_desc':
                        mesgObj.fields.push({
                            "field_name": item.columnname,
                            "field_type": item.datatype,
                            "field_value": this.props.templateArray.template_description
                        });
                        return mesgObj;
                    // case 'template_data':
                    //     mesgObj.fields.push({
                    //         "field_name": item.columnname,
                    //         "field_type": item.datatype,
                    //         "field_value": JSON.stringify(this.props.templateArray).replace(/^"/g, '\\"')
                    //     });
                    //     return mesgObj;
                    case 'active_ind':
                        mesgObj.fields.push({
                            "field_name": item.columnname,
                            "field_type": item.datatype,
                            "field_value": "Y"
                        });
                        return mesgObj;
                    case 'created_by_user_id':
                        mesgObj.fields.push({
                            "field_name": item.columnname,
                            "field_type": item.datatype,
                            "field_value": username
                        });
                        return mesgObj;
                    case 'created_on_dt':
                        mesgObj.fields.push({
                            "field_name": item.columnname,
                            "field_type": item.datatype,
                            "field_value": date
                        });
                        return mesgObj;
                    case 'updated_by_user_id':
                        mesgObj.fields.push({
                            "field_name": item.columnname,
                            "field_type": item.datatype,
                            "field_value": username
                        });
                        return mesgObj;
                    case 'updated_on_dt':
                        mesgObj.fields.push({
                            "field_name": item.columnname,
                            "field_type": item.datatype,
                            "field_value": date
                        });
                        return mesgObj;
                    case 'status':
                        mesgObj.fields.push({
                            "field_name": item.columnname,
                            "field_type": item.datatype,
                            "field_value": (val === 'preview')?'Draft':'Published'
                        });
                        return mesgObj;

                    default:
                        return mesgObj;
                }

            })
            // case 'template_data':
                mesgObj.fields.push({
                    "field_name": "template_data",
                    "field_type": "nvarchar",
                    "field_value": JSON.stringify(this.props.templateArray).replace(/^"/g, '\\"')
                });
                // return mesgObj;
            submitData.Messages.push(mesgObj);


            console.log("Save/Update Template this.props.templateArray: ", submitData);
            // console.log(JSON.stringify(this.props.templateArray));
            if (this.state.id) {
                let url = ApiUrl.templateAPi
                this.templateService.CommonApiCall(url, submitData).then((res) => {
                    if (res.statusText === "OK") {
                        toast.success("  TEMPLATE UPDATED SUCCESSFULLY!");
                        console.log("updateTemplate res after submit data: ", res);
                        // this.updateTemplate(res);
                        this.gotoHomePage(val);
                    } else {
                        toast.error("TEMPLATE NOT UPDATED SUCCESSFULLY !", {
                            draggable: true,
                            position: toast.POSITION.TOP_RIGHT
                        });
                        console.log('error');
                    }
                })
                    .finally(() => this.props.hideGlobalLoader())
            } else {
                let url = ApiUrl.templateAPi
                this.templateService.CommonApiCall(url, submitData).then((res) => {
                    if (res.statusText === "OK") {
                        toast.success("TEMPLATE CREATED SUCCESSFULLY!");
                        console.log("createTemplate res after submit data: ", res);
                        // this.updateTemplate(res);
                        this.gotoHomePage(val);
                    } else {
                        toast.error("Template Not Created")
                        console.log('error');
                        throw new Error()
                    }
                })
                .catch(err => {
                    console.log("errorrrrrrrr", err)
 
                })
                    .finally(() => this.props.hideGlobalLoader())
            }
        })
    }

    gotoHomePage(val) {
        let getName = myRoutePath.find((v, i) => v.url === '/');
        this.props.updateActiveSideBar(getName.path)
        if (val === "preview") {
            this.props.showGlobalLoader();
            let url = ApiUrl.templateAPi
            let displayData = {};
            displayData.OperationType = "GET";
            displayData.Messages = [];
            let mesgObj = {
               template_name: "",
               table_name: "sc_template.template_master",
               fields: []
           };
           displayData.Messages.push(mesgObj);
            this.templateService.CommonApiCall(url,displayData).then((res) => {
                if (res) {
                    // console.log(res);
                    this.setState({ templateList: [...this.state.templateList, ...res.data.dataResponse[0].data] })
                } else {
                    console.log('error');
                }
            })
                .finally(() => {
                    this.props.hideGlobalLoader();
                    this.showPreview(this.state.id ? this.state.id : this.state.templateList[this.state.templateList.length - 1].template_id_sq)
                })

        } else {
            this.props.history.push('/')
        }

    }

    showPreview(id) {
        if (this.props.templateArray.sections.length > 0 || this.props.templateArray.fields.length > 0) {
            this.props.history.push(`/previewTemplate/${id}`);
        }
    }


    showTempLevelControls() {
        this.props.dispatch({
            type: 'SHOW_HIDE_TEMP_CONTROLS',
            payload: !this.props.templateArray.showHideFieldControls
        });
    }

    addTempLevelControls(fType) {
        this.props.dispatch({
            type: 'ADD_TEMP_CONTROLS',
            payload: fType
        });
    }
    addTableFunc = (tableName) => {
        console.log(this.props);
        this.props.templateArray.addTableFunc("Table", tableName)
    }
    AddItem = () => {
        let displayData = {};
        displayData.OperationType = "GET";
        displayData.Messages = [];
        let mesgObj = {
            template_name: "",
            table_name: "section_master"
        };
        displayData.Messages.push(mesgObj);
        let url = ApiUrl.getTableColNames
        this.templateService.CommonApiCall(url, displayData).then((res) => {
            if (res.data.dataResponse.length > 0) {
                console.log('res', res);
                let data = [];
                res.data.dataResponse[0].data.map((item, index) => {
                    if (item.title != null) {
                        data.push(item);
                    }
                    return data;
                });
                console.log('data', data);
                // this.setState({ columnList: [...this.state.columnList, ...data] })
                let moduleData = {};
                moduleData.OperationType = "POST";
                moduleData.Messages = [];
                let mesgObj = {
                    template_name: "",
                    table_name: "sc_template.section_master",
                    fields: [],
                    filter: []
                };
            let username=localStorage.getItem('username')
            let today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                data.forEach((item) => {
                    console.log('data1', item);
                    switch (item.columnname) {
                        case 'section_name':
                            mesgObj.fields.push({
                                "field_name": item.columnname,
                                "field_type": item.datatype,
                                "field_value": this.props.value.autoCompleteBoxValue
                            });
                            return mesgObj;
                        case 'created_by_user_id':
                            mesgObj.fields.push({
                                "field_name": item.columnname,
                                "field_type": item.datatype,
                                "field_value":username
                            });
                            return mesgObj;
                        case 'created_on_dt':
                            mesgObj.fields.push({
                                "field_name": item.columnname,
                                "field_type": item.datatype,
                                "field_value":date
                            });
                            return mesgObj;

                        default:
                    }
                })
                moduleData.Messages.push(mesgObj);
                let url = ApiUrl.sectionMasterDropdown
                this.templateService.CommonApiCall(url, moduleData).then((res) => {
                    if (res && res.data.dataResponse && res.data.dataResponse[0] && res.data.dataResponse[0].data) {
                        let respData = res.data.dataResponse[0].data;
                        let dataArray = [];
                        let tempArr = [];
                        respData.forEach((value, ind) => {
                            if (tempArr.indexOf(value.section_name) === -1) {
                                tempArr.push(value.section_name)
                                dataArray.push({ title: value.section_name })
                            }
                            //console.log("respData =>", respData);                       
                        })
                        this.props.dispatch({
                            type: 'SELECT_MASTER_DROPDOWN',
                            payload: dataArray
                        })
                    }
                    this.props.value.autoCompleteBoxValue = ''
                    this.getDropdownValues()
                });
            } else {
                console.log('error');
            }
        })
        // .finally();
    }

    displayView(value) {
        this.props.dispatch({
            type: 'RENDER_VIEW',
            payload: value
        }) 
    }

    render() {
        return (
            <>
                <TableNameFlyOut addTableFunc={this.addTableFunc} isTableFlyoutOpen={this.props.templateArray.isTableFlyoutOpen} updateTableFlyoutVisibility={this.props.updateTableFlyoutVisibility} />
                <div className="build-template-cover" style={buildTemplateStyle.titleCover}>
                    <div style={buildTemplateStyle.templateArea}>
                        <p style={buildTemplateStyle.smallTitle}>Template Name</p>
                        <p>{this.props.templateArray.template_name}</p>
                    </div>
                    <div style={buildTemplateStyle.versionArea}>
                        <p style={buildTemplateStyle.smallTitle}>Version</p>
                        <p> {this.props.templateArray.version} </p>

                    </div>
                    <div style={buildTemplateStyle.descriptionArea}>
                        <p style={buildTemplateStyle.smallTitle}>Description</p>
                        <p>{this.props.templateArray.template_description}</p>
                    </div>
                </div>

                <Card interactive={true}>
                    <Card.Content style={buildTemplateStyle.cardCover}>
                        <div className="create-tempalte-add-controls-style">
                            <div className="">
                                <Search fluid={true}
                                    results={this.props.value.results}
                                    value={this.props.value.autoCompleteBoxValue}
                                    onResultSelect={(value) => this.props.handleSearchSelect(value)}
                                    onSearchChange={(value) => this.props.handleSearchChange(value)}
                                />
                                {(this.props.value.results.length === 0) ? <div className="">
                                    <Button type="primary" size="small" icon={<Icon root="global" name="slider-controls-plus" size="medium" />}
                                        iconPosition="left" content="ADDITEM" onClick={this.props.addResult.bind(this, this.props.value.autoCompleteBoxValue), this.AddItem} />
                                </div> : null}
                            </div>

                            <div className="addButtonStyle">
                                <Button type="primary" size="small" icon={<Icon root="global" name="slider-controls-plus" size="medium" />}
                                    iconPosition="left" content="ADD" onClick={this.props.addSection.bind(this)} />
                            </div>
                            <div className="">
                                <Radio label="Tab" checked={this.props.templateArray.render_as === "Tab"} onChange= {this.displayView.bind(this, "Tab")}/>
                                <Radio label="Section" checked={this.props.templateArray.render_as === "Section"} onChange={this.displayView.bind(this, "Section")}/>
                            </div>
                            <div className="">
                                <Button size="small"
                                    content="ADD CONTROLS" onClick={this.showTempLevelControls.bind(this)} />
                                {(this.props.templateArray.showHideFieldControls) ? <ControlOptions addSubSections={this.addTempLevelControls.bind(this)}
                                    setStyle={{ top: '10px', right: '50px' }} /> : ''}
                            </div>
                        </div>
                    </Card.Content>
                </Card>
                {/* {
                    this.props.templateArray.fields.map((item, index) => {
                        return <div key={index}>
                            <Fields key={index} fieldIndex={index} fieldItem={item}
                                parentArray={this.props.templateArray} buildTemplateStyle={buildTemplateStyle} type={'template'} hasError={this.state.hasError}> </Fields>
                        </div>
                    })
                } */}

                {

                    this.props.templateArray.sections.map((secItem, secIndex) => {
                        return <div key={secIndex}>
                            {(secItem.section_name === 'Template Grid') ?
                                <div>
                                    <TemplateGrid sectionName={secItem.section_name} mainIndex={null} secIndex={secIndex}></TemplateGrid>
                                </div>
                                :
                                <Sections key={secIndex} secIndex={secIndex} secItem={secItem}
                                    parentArray={this.props.templateArray} buildTemplateStyle={buildTemplateStyle} mainIndex={null} hasError={this.state.hasError}></Sections>
                            }
                        </div>
                    })
                }

                <div style={buildTemplateStyle.footerButtonStyle}>
                    <Button type="secondary" size="small" content="RESET" onClick={this.resetTemplate} />
                    <Button type="secondary" size="small" content="PREVIEW" /*onClick={this.showPreview.bind(this)}*/ onClick={this.submitData.bind(this, "preview")} />
                    <Button type="primary" size="small" content="SAVE" onClick={this.configure.bind(this)} />
                </div>
                {this.state.showConfigurationModal ?
                    <ConfigurationModal
                        showConfigurationModal={this.state.showConfigurationModal}
                        submitData={this.submitData.bind(this, "module")}
                        closeConfigurationModal={this.closeConfigurationModal.bind(this)}
                    /> : null}

            </>
        );
    }


};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BuildTemplateForm));

const stateOptions = [{ value: 'General Information', text: 'General Information', sub: false },
{ value: 'Mixing', text: 'Mixing', sub: true }, { value: 'Powder Mix', text: 'Powder Mix', sub: true },
{ value: 'Water Addition', text: 'Water Addition', sub: false },
{ value: 'Mixing Instructions', text: 'Mixing Instructions', sub: false },
{ value: 'Template Grid', text: 'Template Grid' }];

const buildTemplateStyle = {
    titleCover: {
        display: 'flex',
        fontWeight: 'bold',
        gap: 12,
    },
    footerButtonStyle: {
        justifyContent: 'flex-start',
        margin: '10px 0px'
    },
    templateArea: {
        padding: '0 10px',
        borderRight: '1px solid #80808066'
    },
    versionArea: {
        padding: '0 10px',
        borderRight: '1px solid #80808066'
    },
    descriptionArea: {
        padding: '0 10px',
    },
    smallTitle: {
        fontSize: '11px',
        fontWeight: 'normal'
    },
    cardCover: {
        padding: '10px 10px'
    },
    selectBoxStyle: {
        width: '40%',
        float: 'left'
    },
    rightToButton: {
        float: 'right'
    },
    mainSectionbar: {
        //background: '#d1dee894',
        //padding: '10px',
        display: 'flex',
        justifyContent: 'space-around',
        position: 'relative'
    },
    mainSectionheader: {
        color: '#5f7d95',
        fontWeight: 'bold',
        flex: 1,
        //paddingLeft: '30px'
    },
    mainSectionControl: {
        borderRight: '1px solid #80808066',
        padding: '3px 10px',
        color: 'black',
        fontWeight: 'bold',
        cursor: 'pointer'
    },
    mainSectionControlAccord: {
        borderRight: '1px solid #80808066',
        padding: '3px 10px',
        color: 'black',
        fontWeight: 'bold',
        cursor: 'pointer',
        marginRight: '35px'
    },
    makeBorderZero: {
        border: 0
    },
    cardContent: {
        display: 'flex',
        justifyContent: 'flex-start',
        border: '0px solid',
        position: 'relative'
    },
    selectStyle: {
        width: '24%',
        margin: '0 0.5%',
    },
    avselectStyle: {
        width: '10%',
        margin: '0 0.5%',
    },

    checkBoxStyle: {
        top: '20px'
    },
    checkBoxTitle: {
        fontWeight: 'bold'
    }
}
