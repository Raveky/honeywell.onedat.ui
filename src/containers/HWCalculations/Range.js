import React, { useEffect, useState } from 'react';
import { Accordion } from '@scuf/common';
import { Grid, SidebarLayout, Footer, Header, Input, Icon, Modal, Button,TextArea, Select, DatePicker  } from '@scuf/common';
import { DataTable } from '@scuf/datatable';
import { Tree } from '@scuf/common';
import SelectField from '../../components/common/SelectField';
import TextAreaField from '../../components/common/TextAreaField';
import DataGrid from '../../components/common/DataGrid';
import InputDate from '../../components/common/InputDate';
import DataModal from '../../components/common/DataModal';
import TableRowGroup from '../../components/common/TableRowGroup'; 
const Range = (props) => {

        const handleTransmitLevel = () => {
           return true
        }
        const options=[ { value: 'AL', text: 'Alabama' }, {value: 'GA', text:'Georgia' }, {value:'HI', text:'Hawaii'} ]

        return (
                <Modal style={{height:"50%", width:"65%"}} closeIcon={true} onClose={() => props.onClose()} open={props.open} closeOnDimmerClick={false}>
                    <div className="range-data-modal">
                        <h5>Description: The page will allows the user to select the range of WCs to be calculated</h5>
                    </div>
                    <Modal.Content>
                        <Grid.Row>
                            <Grid.Column width="4">
                                <SelectField placeholder="--Select--" indicator="required" label="Phase" options={options} onChange={handleTransmitLevel}/>
                            </Grid.Column>
                            <Grid.Column width="4">
                                <SelectField placeholder="Start Test" indicator="required" label="Start Test" options={options} onChange={handleTransmitLevel}/>
                            </Grid.Column>
                            <Grid.Column width="4">
                                    <SelectField placeholder="End Test" indicator="required" label="End Test" options={options} onChange={handleTransmitLevel}/>
                            </Grid.Column>
                            <Grid.Column width="4">
                                <SelectField placeholder="--Select--" indicator="required" label="SimDist" options={options} onChange={handleTransmitLevel}/>
                            </Grid.Column>
                        </Grid.Row>
                    </Modal.Content>
                    <Modal.Footer style={{justifyContent:"center"}}> 
                        <Button onClose={() => props.onClose()} type="secondary" content="PI Retransmit" size="small"/>
                            <Button onClose={() => props.onClose()} type="primary" content="LIMS Retransmit" size="small" />
                            <Button onClose={() => props.onClose()}type="secondary" content="Calculate" size="small" />
                            <Button onClose={() => props.onClose()} type="secondary" content="Reset" size="small" />
                            <Button onClose={() => props.onClose()} type="secondary" content="Close" size="small" />
                    </Modal.Footer>
                </Modal>

        )
    }


const buildTemplateStyle = {
    searchItems: {
        display: 'flex',
        flexDirection:'column',
    },
}

export default Range;