import React, { useEffect, useState } from 'react';
import { Accordion } from '@scuf/common';
import { Grid, SidebarLayout, Footer, Header, Input, Icon, Modal, Button,TextArea, Select, DatePicker  } from '@scuf/common';
import { Table, Badge } from '@scuf/common';

const feedMassData={
    input:[{name:"Fresh Feed Mass",module:"F2",value:2347.51},{name:"Make Gas Mass",module:"MT1",value:64.29947}],
    output:[{name:"Bleed Gas Mass",module:"RO1",value:20.3177},
    {name:"Stabilizer Bottoms Product Can Weight ",module:"P2",value:2270},
    {name:"Flash Gas Mass",module:"G1",value:35.7795},
    {name:"Feed H2S Weight",module:"",value:53.37263}
    , {name:"Feed NH3 Weight",module:"",value:2.40763},
    {name:"Feed H2O Weight",module:"",value:0.0000}]
}
const EstimatedWeightCheck=(props)=>{

        return (
            <Modal  closeIcon={true} onClose={() => props.onClose()} open={props.open} closeOnDimmerClick={false}>
                <Modal.Header>
                  Estimated Weight Check
                </Modal.Header>
                <Modal.Content>
                <Grid.Row>
                <Grid.Column width="4">Plant: ###</Grid.Column>
                <Grid.Column width="4">Run: ###</Grid.Column>
                <Grid.Column width="4">Test: ###</Grid.Column>
            </Grid.Row>

            <Table style={buildTemplateStyle.estimatedTable}>
                <Table.Header>
                    <Table.HeaderCell content="Variable"/>
                    <Table.HeaderCell content="Module"/>
                    <Table.HeaderCell content="Value(in grams)"/>
                </Table.Header>
                <Table.Body>
                    {feedMassData.input.map(item=>{
                        return(<Table.Row>
                            <Table.Cell>
                                {item.name}
                            </Table.Cell>
                            <Table.Cell>
                                {item.module}
                            </Table.Cell>
                            <Table.Cell textAlign="center">
                                {item.value}
                            </Table.Cell>
                        </Table.Row>)
                    })}
                    
                    <Table.Row>
                        <Table.Cell colSpan={2} textAlign="left">
                            Total Mass of Inputs
                        </Table.Cell>
                        <Table.Cell textAlign="center">
                        2411.80947
                        </Table.Cell>
                    </Table.Row>
                    {feedMassData.output.map(item=>{
                        return(<Table.Row>
                            <Table.Cell>
                                {item.name}
                            </Table.Cell>
                            <Table.Cell>
                                {item.module}
                            </Table.Cell>
                            <Table.Cell textAlign="center">
                                {item.value}
                            </Table.Cell>
                        </Table.Row>)
                    })}
                    <Table.Row>
                        <Table.Cell colSpan={2} textAlign="left">
                            Total Mass of Inputs
                        </Table.Cell>
                        <Table.Cell textAlign="center">
                        2382.05380
                        </Table.Cell>
                    </Table.Row>
                </Table.Body>
            
                <Table.Footer>
                    <Table.Row align="left">
                        <Table.HeaderCell colSpan={5}>
                        Estimated Weight Recovery = 98.76625
                        </Table.HeaderCell>
                    </Table.Row>
                </Table.Footer>
            </Table>

                </Modal.Content>
                  {/* <div>Estimated Weight Recovery = 98.76625</div>
    <div>Estimated wt, recovery does not take into account accumulation in HPS, LPS and columns</div>
    <div>All feeds S,N,O are assumed to be completly hydrogenerated to H2S, NH3 and H2O</div>
               */}
                <Modal.Footer style={{justifyContent:"center"}}> 
                    <Button onClose={() => props.onClose()} type="secondary" content="Save Results" size="small"/>
                    <Button onClose={() => props.onClose()} type="primary" content="Close" size="small" />
                </Modal.Footer>
            </Modal>

        )
    }


const buildTemplateStyle = {
    searchItems: {
        display: 'flex',
        flexDirection:'column',
    },
    estimatedTable:{
        '& td':{
            lineHeight: "1.2rem !imortant",
        }
    }
}

export default EstimatedWeightCheck;