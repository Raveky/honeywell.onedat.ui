import React, { useEffect, useState } from 'react';
import { Accordion } from '@scuf/common';
import { Grid, SidebarLayout, Footer, Header, Icon, Button,TextArea, Select, DatePicker  } from '@scuf/common';
import { DataTable } from '@scuf/datatable';
import InputField from '../../components/common/InputField';
import SelectField from '../../components/common/SelectField';
import TextAreaField from '../../components/common/TextAreaField';
import DataGrid from '../../components/common/DataGrid';
import InputDate from '../../components/common/InputDate';
import DataModal from '../../components/common/DataModal';
import TableRowGroup from '../../components/common/TableRowGroup'; 
import Range from './Range'; 
import { myRoutePath } from '../../Routes/Routes';
import { withRouter } from 'react-router-dom';
import EstimatedWeightCheck from './EstimatedWeightCheck'
  const data = [
    {field1: 's-1', field2: '03-20-2019', field3: '03-20-2019', field4: '2',field5: '3',
    field6: 'Low',field7: '03-20-2019',field8: '03-20-2019',field9: '03-20-2019',field10: 'Configure',
    field11: '',field12: '',field13: '',field14: '',field15: ''},
    {field1: 's-1', field2: '03-20-2019', field3: '03-20-2019', field4: '2',field5: '3',
    field6: 'Low',field7: '03-20-2019',field8: '03-20-2019',field9: '03-20-2019',field10: 'Configure',
    field11: '',field12: '',field13: '',field14: '',field15: ''},
    {field1: 's-1', field2: '03-20-2019', field3: '03-20-2019', field4: '2',field5: '3',
    field6: 'Low',field7: '03-20-2019',field8: '03-20-2019',field9: '03-20-2019',field10: 'Configure',
    field11: '',field12: '',field13: '',field14: '',field15: ''},
    {field1: 's-1', field2: '03-20-2019', field3: '03-20-2019', field4: '2',field5: '3',
    field6: 'Low',field7: '03-20-2019',field8: '03-20-2019',field9: '03-20-2019',field10: 'Configure',
    field11: '',field12: '',field13: '',field14: '',field15: ''},
    {field1: 's-1', field2: '03-20-2019', field3: '03-20-2019', field4: '2',field5: '3',
    field6: 'Low',field7: '03-20-2019',field8: '03-20-2019',field9: '03-20-2019',field10: 'Configure',
    field11: '',field12: '',field13: '',field14: '',field15: ''},
    {field1: 's-1', field2: '03-20-2019', field3: '03-20-2019', field4: '2',field5: '3',
    field6: 'Low',field7: '03-20-2019',field8: '03-20-2019',field9: '03-20-2019',field10: 'Configure',
    field11: '',field12: '',field13: '',field14: '',field15: ''},
];

const header = [
    {
        field:"field1",
        header:"Test",

    },
    {
        field:"field2",
        header:"Test Start Time",

    },
    {
        field:"field3",
        header:"Test End Time",

    },
    {
        field:"field4",
        header:"Test Duration",

    },
    {
        field:"field5",
        header:"Line Out Duration",

    },
    {
        field:"field6",
        header:"Weight Check Quality",

    },
    {
        field:"field7",
        header:"Test Start Time",

    },
    {
        field:"field8",
        header:"Last Completed LIMS Transfer",
 
    },
    {
        field:"field9",
        header:"Last Completed Calculation",
    },
    {
        field:"field10",
        header:"",
    },
    {
        field:"field11",
        header:"Last Completed Calculation",
        icon:"template"
    },
    {
        field:"field12",
        header:"Transmit PI",
        icon:"template"
    },
    {
        field:"field13",
        header:"Transmit LIMS",
        icon:"template"
    },
    {
        field:"field14",
        header:"",
        icon:"document-delete"
    },
    {
        field:"field15",
        header:"Delete",
        icon:"delete"
    },
]
const wcVariableData = [
    {name:"Feed Tank and Weigh Scale", variables:[{value:"Tank_Weight"},{value:"LO_Tank_Delta_Weight_Per_Time"}]},
    {name:"Spent Caustic Vessel", variables:[{value:"Caustic Weight"},{value:"Tank_Pressure"},{value:"Tank_Temparature"}]}
]
class TestCreation extends React.PureComponent{
    constructor(){
        super()
        this.state={
            options:[ { value: 'AL', text: 'Alabama' }, {value: 'GA', text:'Georgia' }, {value:'HI', text:'Hawaii'} ],
            displayFormat: 'MM/DD/YY, h:mm a',
            minuteStep: 30,
            wcInput:false,
            range:false,
            weightCheck:false,
        }
    }

    statusRenderer = (cellData) => {
        return <span>  <Select  options={this.state.options} /></span>;
    }

    handleCheckRawData = () =>{
        this.props.history.push('/checkRawData');
    }

    handleWeightCheck = (value) =>{
        console.log("dhasdhsahdas", value)
    }

    handleTransmitLevel = (val) => {
        console.log("fsdghfsdh")
    }

    validateWcInput = () =>{
        this.setState({wcInput:true})
    }

    handleModalClose = () =>{
        this.setState({wcInput:false})
        this.setState({range:false})
        this.setState({weightCheck:false})
    }

    selectRange = () =>{
        this.setState({range:true})
    }
    handleEstimatedWeightCheck = () =>{
        this.setState({weightCheck:true})
    }


    goToHome() {
        sessionStorage.setItem('moduleDef', true);
        let getName = myRoutePath.find((v, i) => v.url === '/');
        this.props.updateActiveSideBar(getName.path);
        this.props.history.push(getName.url);
    }

    render(){
        const { displayFormat, minuteStep } = this.state;
            return (
            <div>
                    <Accordion arrowPosition="left" active={false}>
                        <Accordion.Content title="Search Criteria" arrowPosition="left">
                            <Grid.Row>
                        <Grid.Column width={4} >
                            <InputField label="Plant Code" indicator="required" placeholder="Plant Code" />
                            <InputDate
                                key={displayFormat + minuteStep}
                                label="Test Start Time"
                                type="datetime"
                                displayFormat={displayFormat}
                                placeholder={displayFormat}
                                minuteStep={minuteStep}
                                indicator="required"
                            />
                            <SelectField placeholder="Weight Check Quality" indicator="required" label="Weight Check Quality" options={this.state.options} onChange={this.handleWeightCheck}/>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <InputField label="Run" indicator="required" placeholder="Run" />
                            <InputDate
                                key={displayFormat + minuteStep}
                                label="Test End Time"
                                type="datetime"
                                displayFormat={displayFormat}
                                placeholder={displayFormat}
                                minuteStep={minuteStep}
                                indicator="required"
                            />
                            <SelectField placeholder="Select a State" indicator="required" label="Transmit Level" options={this.state.options} onChange={this.handleTransmitLevel}/>
                        </Grid.Column>
                        <Grid.Column width={4}>
                            <InputField label="Test" indicator="required" placeholder="Test" />
                            <TextAreaField label="Test Comments"  placeholder=""/>
                        </Grid.Column>
                    </Grid.Row>
                </Accordion.Content>
                </Accordion>
                <DataGrid headers={header} data={data} />
    
                <div className="test-creation-footer">
                    <Button onClick={this.handleCheckRawData} type="secondary" content="Save" size="small"/>
                    <Button onClick={this.handleCheckRawData} type="primary" content="Check Raw Data" size="small" />
                    <Button onClick={this.validateWcInput} type="secondary" content="Validate WC Input" size="small" />
                    <Button onClick={this.handleEstimatedWeightCheck} type="secondary" content="Quick WC Estimate" size="small" />
                    <Button onClick={this.handleCheckRawData} type="secondary" content="Calculate" size="small" />
                    <Button onClick={this.selectRange} type="secondary" content="Calculate By Range" size="small" />
                    <Button onClick={this.handleCheckRawData} type="secondary" content="Cancel" size="small" />
                    <Button onClick={this.handleCheckRawData} type="secondary" content="View WC Result Summary" size="small" />
                    <Button onClick={this.handleCheckRawData} type="secondary" content="Help" size="small" />
                </div>
                <DataModal open={this.state.wcInput} onClose={this.handleModalClose} title="Validation Result">
                    <Grid.Row>
                        <Grid.Row>
                            <Grid.Column width="3">PI Variables</Grid.Column>
                            <Grid.Column width="3">Plant: ###</Grid.Column>
                            <Grid.Column width="3">Run: ###</Grid.Column>
                            <Grid.Column width="3">Test: ###</Grid.Column>
                        </Grid.Row>
                        <Grid.Column>
                        <TableRowGroup headers={["Vessel Name","Variable"]} data={wcVariableData} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Column>
                        LIMS Variables
                        </Grid.Column>
                        <Grid.Column>
                            <TableRowGroup headers={["Vessel Name","Variable"]} data={wcVariableData} />
                    </Grid.Column>
                </DataModal>
                <EstimatedWeightCheck open={this.state.weightCheck} onClose={this.handleModalClose} />
                <Range open={this.state.range} onClose={this.handleModalClose} />
            </div>
        )
    }
}
export default withRouter(TestCreation);

const buildTemplateStyle = {
    searchItems: {
        display: 'flex',
        flexDirection:'column',
    },
    footerItem: {
        padding:"10px 0px"
    }
}
