import React, { Fragment } from 'react';
import HeaderArea from './Header';
import { SidebarLayout, Icon, BadgedIcon, Loader } from '@scuf/common';
import { BrowserRouter as Router } from "react-router-dom";
import { connect } from 'react-redux';
import { Routes, myRoutePath } from './Routes/Routes';
import { updateActiveSideBar } from './../actions/Template';

function mapStateToProps(state) {
    console.log(state);
    return {
        stateVal: state,
        loading: state.templateReducer.isShowGlobalLoader
    }
}

const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    updateActiveSideBar: (value) => dispatch(updateActiveSideBar(value))
})

class ExampleContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: true,
            expand: []
        };
        this.handleCollapse = this.handleCollapse.bind(this);
        this.handleSubmenuClick = this.handleSubmenuClick.bind(this);
        this.router = undefined;
    }

    handleCollapse() {
        const { collapsed } = this.state;
        this.setState({ collapsed: !collapsed, expand: [] });
    }

    handleItemClick(event, itemProps) {
        const { name } = itemProps;

        this.props.updateActiveSideBar(name);
        let currentPath = myRoutePath.find((val, ind) => {
            return val.path === itemProps.name;
        });
        if (currentPath) {
            this.router.history.push(currentPath.url);
        }
    }

    handleSubmenuClick(event, itemProps) {
        console.log(itemProps)
        const expand = [...this.state.expand];
        const { name } = itemProps;
        const expanderIndex = expand.findIndex(element => element === name);
        if (expanderIndex >= 0) {
            expand.splice(expanderIndex, 1);
        } else {
            expand.push(name);
        }
        this.setState({ collapsed: false, expand });
    }

    goHome() {
        localStorage.setItem("username", '');
    }

    render() {
        const { collapsed, expand } = this.state;
        const activeName = this.props.stateVal.activeSideBar

        const styleSheet = {
            contentStyle: {
                padding: '10px',
                width: '100%'
            },
            sidebarLayout: {
                flexShrink: '15',
                transition: 'all 1s ease 0s'
            }
        }
        return (
            <Fragment>
                <div className="side-bar-comp" style={{ height: 'calc(100% - 59px)' }}>
                    <Loader loading={this.props.loading} text=" " overlayColor="#a0a0a0" overlayOpacity="0.5">
                        <HeaderArea onMenuToggle={this.handleCollapse} />
                        <SidebarLayout style={{ height: '100%' }}>
                            <SidebarLayout.Sidebar style={collapsed ? styleSheet.sidebarLayout : null}>
                                <SidebarLayout.Sidebar.Item
                                    content="Home"
                                    activeName={activeName}
                                    name="Home"

                                    icon={<BadgedIcon root="common" name="home1" />}
                                    onClick={this.handleItemClick.bind(this)}
                                />

                                <SidebarLayout.Sidebar.Submenu
                                    content="Manage"

                                    activeName={activeName}
                                    name="ManageExpander"
                                    icon={<Icon root="global" name="tools" />}
                                    onClick={this.handleSubmenuClick}
                                    open={expand.includes('ManageExpander')}
                                >
                                    <SidebarLayout.Sidebar.Item
                                        content="User"
                                        activeName={activeName}
                                        name="User"
                                        onClick={this.handleItemClick.bind(this)}
                                    />
                                    <SidebarLayout.Sidebar.Item
                                    content="UOM"
                                    activeName={activeName}
                                    name="UOM"
                                    onClick={this.handleItemClick.bind(this)}
                                />
                                 <SidebarLayout.Sidebar.Item
                                    content="UOM Template"
                                    activeName={activeName}
                                    name="UOMTemplate"
                                    onClick={this.handleItemClick.bind(this)}
                                />
                                    <SidebarLayout.Sidebar.Item
                                        content="Technology"
                                        activeName={activeName}
                                        name="Technology"
                                        onClick={this.handleItemClick.bind(this)}
                                    />
                                    <SidebarLayout.Sidebar.Item
                                        content="Project"
                                        activeName={activeName}
                                        name="Project"
                                        onClick={this.handleItemClick.bind(this)}
                                    />

                                    <SidebarLayout.Sidebar.Item
                                        content="Module Configuration"
                                        activeName={activeName}
                                        name="Module Configuration"
                                        onClick={this.handleItemClick.bind(this)}
                                    />

                                    <SidebarLayout.Sidebar.Item
                                        content="Catalyst Size"
                                        activeName={activeName}
                                        name="Catalyst Size"
                                        onClick={this.handleItemClick.bind(this)}
                                    />

                                    <SidebarLayout.Sidebar.Item
                                        content="Catalyst Scale"
                                        activeName={activeName}
                                        name="Catalyst Scale"
                                        onClick={this.handleItemClick.bind(this)}
                                    />

                                    <SidebarLayout.Sidebar.Item
                                        content="Catalyst State"
                                        activeName={activeName}
                                        name="Catalyst State"
                                        onClick={this.handleItemClick.bind(this)}
                                    />

                                    <SidebarLayout.Sidebar.Item
                                        content="Catalyst Shape"
                                        activeName={activeName}
                                        name="Catalyst Shape"
                                        onClick={this.handleItemClick.bind(this)}
                                    />

                                    <SidebarLayout.Sidebar.Item
                                        content="Templates"
                                        activeName={activeName}
                                        name="Templates"
                                        onClick={this.handleItemClick.bind(this)}
                                    />
                                </SidebarLayout.Sidebar.Submenu>
                                <SidebarLayout.Sidebar.Submenu
                                    content="Materials"

                                    activeName={activeName}
                                    name="MaterialsExpander"
                                    icon={<Icon root="common" name="temperature-freezing" />}
                                    onClick={this.handleSubmenuClick}
                                    open={expand.includes('MaterialsExpander')}
                                >
                                    <SidebarLayout.Sidebar.Item
                                        content="List"
                                        activeName={activeName}
                                        name="MoviesList"

                                        onClick={this.handleItemClick.bind(this)}
                                    />
                                    <SidebarLayout.Sidebar.Item
                                        content="Create"
                                        activeName={activeName}
                                        name="MoviesCreate"

                                        onClick={this.handleItemClick.bind(this)}
                                    />
                                    <SidebarLayout.Sidebar.Item
                                        content="Share"
                                        activeName={activeName}
                                        name="MoviesShare"

                                        onClick={this.handleItemClick.bind(this)}
                                    />
                                </SidebarLayout.Sidebar.Submenu>
                                <SidebarLayout.Sidebar.Submenu
                                    content="Experiment"

                                    activeName={activeName}
                                    name="ExperimentExpander"
                                    icon={<Icon root="global" name="globe" />}
                                    onClick={this.handleSubmenuClick}
                                    open={expand.includes('ExperimentExpander')}
                                >
                                    <SidebarLayout.Sidebar.Item
                                        content="List"
                                        activeName={activeName}
                                        name="MoviesList"

                                        onClick={this.handleItemClick.bind(this)}
                                    />
                                    <SidebarLayout.Sidebar.Item
                                        content="Create"
                                        activeName={activeName}
                                        name="MoviesCreate"

                                        onClick={this.handleItemClick.bind(this)}
                                    />
                                    <SidebarLayout.Sidebar.Item
                                        content="Share"
                                        activeName={activeName}
                                        name="MoviesShare"

                                        onClick={this.handleItemClick.bind(this)}
                                    />
                                </SidebarLayout.Sidebar.Submenu>
                            </SidebarLayout.Sidebar>
                            <SidebarLayout.Content style={styleSheet.contentStyle}>
                                <Router ref={el => this.router = el}>
                                    <Routes goHome={this.goHome.bind(this)} />
                                </Router>
                            </SidebarLayout.Content>
                        </SidebarLayout>
                    </Loader>
                </div>
            </Fragment>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ExampleContainer)
