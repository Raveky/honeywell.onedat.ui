import React, { useState , useEffect} from 'react';
import { Card, Button, Input, Search, Loader, Select } from '@scuf/common';
import { connect } from 'react-redux';
import { getTemplateNames, getTemplateVersion, getTemplateDescription, resetTemplate, updateTblName,
   findTemplate, createTemplate, addTechnology} from '../actions/Template';
import TemplateService from '../utils/template.service';
import FindTable from './FindTable';
import { useHistory } from "react-router-dom";

function mapStateToProps(state) {
    const templateState = state.templateReducer;
    return {
        templateName: templateState.template.template_name,
        templateVersion: templateState.template.template_version,
        templateDescription: templateState.template.template_description,
        templateTechnology: templateState.template.technology_name,
		showHideFindTable: templateState.showHideFindTable,
        find_results: templateState.find_results
    }
}

const mapDispatchToProps = (dispatch) => ({
    getTemplateName: (value) => dispatch(getTemplateNames(value)),
    addTechnology : (value ) => dispatch(addTechnology(value)),
    getTemplateVersion: (id) => dispatch(getTemplateVersion(id)),
    getTemplateDescription: (desc) => dispatch(getTemplateDescription(desc)),
    resetTemplate: () => dispatch(resetTemplate()),
    updateTblName: (value) => {dispatch(updateTblName(value))},
	findTemplate: () => dispatch(findTemplate()),
    createTemplate: (obj)=>dispatch(createTemplate(obj))
});


const templateService = new TemplateService();
const CreateTemplateForm = (props) => {
    let history = useHistory();
    const [tempateErr, setTempateErr] = useState("");
    const [table_value, setTableValue] = useState("");
    const [table_results, setTableResult] = useState([]);
    const [tableNames, setTableNames] = useState([]);
    const [loading, setLoading] = useState(false);
    const createTemplateFunction = () => {
        if (!props.templateName) {
            setTempateErr("Please Enter the Template Name!");
        } else {
            setTempateErr("");
			 props.createTemplate({
                template_name: props.templateName,
                template_description: props.templateDescription
            });
            // props.getBuild();
            history.push('/buildTemplate')
        }
    }

    const editAction = () => {
        history.push('/buildTemplate/');
    }

   /* useEffect(() => {
        setLoading(true);
        templateService.getTableNames().then((res) => {
            if(res) {
               console.log('table names ' , res.data);
              // res.data.map((item, index) => {
                  if(res.data === undefined) {
                    setTableNames([]);
                  } else {
                    setTableNames(res.data);
                  }

                    
             //  })
             
            }
       })
       .finally(()=>setLoading(false));
      
    },[])*/

    const handleSearchChange = (value) => {
        console.log(tableNames);
        if(tableNames === undefined) {
            setTableNames([]);
        } 
        console.log(tableNames);
        const data = tableNames.filter(item => item.title.toLowerCase().includes(value.toLowerCase()))
        setTableResult(data);
    }

    const handleSearchSelect = (value) => {
        console.log(value);
        setTableValue(value.title);
        props.updateTblName(value);
    }

    return (
        <>
        <Loader loading={loading} text=" " overlayColor="#a0a0a0" overlayOpacity="0.5">
            <Card interactive={false}>
                <Card.Header title="Create or Find Template" style={templateFormStyle.headerStyle} />
                <Card.Content >
                    <div>
                        <div className="col-sm-4 d-inline-block align-top">
                            <Input fluid={true} value={props.template_name} style={{ ...templateFormStyle.nameBox, ...templateFormStyle.inputBoxCommonStyles }} type="text" placeholder="Hint Text"
                                label="Template Name" onChange={props.getTemplateName} indicator="required" error={tempateErr} />
                        </div>
                        <div className="col-sm-4 d-inline-block align-top">
                            <Input fluid={true} value={props.template_description} style={{ ...templateFormStyle.descriptionBox, ...templateFormStyle.inputBoxCommonStyles }} type="text"
                                placeholder="Hint Text" label="Description" onChange={props.getTemplateDescription} />
                        </div>
                        <div className="col-sm-4 d-inline-block align-top search global-search-type">
                            <label className="create-temp-tab-name"><strong>Module Name</strong><div id="red-batch" className="ui red circular empty label badge  circle-padding"></div></label>
                            <Search fluid={true}
                                results={table_results}
                                value={table_value}
                                onResultSelect={(value) => { handleSearchSelect(value) }}
                                onSearchChange={(value) => { handleSearchChange(value) }}
                            />
                        </div>
                       { /*<div className="col-sm-4 d-inline-block align-top">
                            <Input fluid={true} value={props.templateTechnology} style={{ ...templateFormStyle.descriptionBox, ...templateFormStyle.inputBoxCommonStyles }} type="text"
                                placeholder="Hint Text" label="Technology" disabled={true} />
    </div> */}

<div className="col-sm-4 d-inline-block align-down">
<Select fluid={true} label="Technology" multiple={false}style={{ ...templateFormStyle.descriptionBox, ...templateFormStyle.inputBoxCommonStyles }} placeholder="Select Technologies" size="small" value={props.templateTechnology} options={avaialbleControlOptions} 
onChange={
    (value) => {

        props.addTechnology(value)
    }}
                    /></div>

                    </div>
                </Card.Content>
                <Card.Footer style={templateFormStyle.footerButtonStyle}>
                    <Button type="secondary" size="small" content="RESET" onClick={props.resetTemplate} />
                    <Button type="secondary" size="small" content="FIND" onClick={props.findTemplate}/>
                    <Button type="primary" size="small" content="CREATE" onClick={createTemplateFunction} />
                </Card.Footer>
            </Card>
			<FindTable editAction= {editAction}></FindTable>
        </Loader>
        </>
    );
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateTemplateForm);
const avaialbleControlOptions = [{ value: 'FEEDSTOCKDAT', text: 'FEEDSTOCKDAT' }, { value: 'NAPHTHDAT', text: 'NAPHTHDAT' }, { value: 'HYDAT', text: 'HYDAT' }, { value: 'HCYE', text: 'HCYE' }, { value: 'ABSORBENTS', text: 'ABSORBENTS' }, { value: 'MINIDAT', text: 'MINIDAT' }, { value: 'HEAVIER', text: 'HEAVIER' }, { value: 'FCC', text: 'FCC' },{ value: 'AROMATICS', text: 'AROMATICS' }, { value: 'HPPA', text: 'HPPA' }, { value: 'HPT', text: 'HPT' }];


const templateFormStyle = {
    headerStyle: {
        background: '#80808024',
        padding: '6px 10px',
        fontWeight: 'bold',
        fontSize: '13px'
    },
    footerButtonStyle: {
        justifyContent: 'flex-start'
    },
    nameBox: {
        flex: 2
    },
    versionBox: {
        flex: 1
    },
    descriptionBox: {
        flex: 3
    },
    cardContent: {
        display: 'flex',
        justifyContent: 'space-around',
        gap: '10px'
    },
    inputBoxCommonStyles: {
        //margin: '0px 10px'
    }
}