import React from 'react';
import Sections from './Sections';

const  SubSection = (props) =>{
    console.log(props);
    return(
        <>
            <Sections mainIndex={props.mainIndex} secIndex={props.secIndex} secItem = {props.secItem} parentArray={props.parentArray} buildTemplateStyle={props.buildTemplateStyle} hasError={props.hasError} />
        </>
    )
}

export default SubSection;