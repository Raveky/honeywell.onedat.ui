import React from 'react';
import { Select, Icon, Input, Checkbox, Search, Loader } from '@scuf/common';

import { connect } from 'react-redux';
import ButtonDetailFlyOut from './Flyouts/ButtonDetailFlyOut';
import ConfigureFieldsFlyOut from './Flyouts/ConfigureFieldsFlyOut';
import {
    clearFieldName, updateFieldName, updateFieldType, updateSourceGroup, updateMandatory, openHideButtonDetails,
    addButtonDetails, cloneControl, addFieldLevelControl, deleteControl, addFieldControls, hideIcons, displayIcons,
    updateTableFlyoutVisibility, assignAddTableFunctionality, deleteColumn, updateColumnName, updateColumnType,
    updateColumnSourceGroup, addColumn, updateBtnProperty, addConfigureFields, createTripleDropdown,
    addTemplateColData, configureFields
} from '../actions/Template';
import TemplateService from '../utils/template.service';
import Confirmation from './custom-popups/DeleteConfirmation';
import { ApiUrl } from '../constants';
import { showGlobalLoader, hideGlobalLoader  } from './../actions/Template';

const mapDispatchToProps = (dispatch) => ({
    updateSourceGroup: (fObject, value) => dispatch(updateSourceGroup(fObject, value)),
    updateFieldName: (fObject, value) => {
        console.log(fObject, value);
        dispatch(updateFieldName(fObject, value))
        dispatch(addTemplateColData())
    },
    clearFieldName: (fObject, value) => {
        dispatch(clearFieldName(fObject, value))
    },
    updateFieldType: (fObject, value) => dispatch(updateFieldType(fObject, value)),
    updateMandatory: (fObject, value) => dispatch(updateMandatory(fObject, value)),
    openHideButtonDetails: (fObject, value) => { dispatch(openHideButtonDetails(fObject, value)) },
    addButtonDetails: (fObject, value) => { dispatch(addButtonDetails(fObject, value)) },
    cloneControl: (fObject, fArray, value) => {
        dispatch(cloneControl(fObject, fArray, value))
        dispatch(addTemplateColData())
    },
    addFieldLevelControl: (fObject, value) => { dispatch(addFieldLevelControl(fObject, value)) },
    deleteControl: (fObject, value) => {
        dispatch(deleteControl(fObject, value))
        dispatch(addTemplateColData())
    },
    addFieldControls: (fObject, fArray, value, ftype, colType) => {
        dispatch(addFieldControls(fObject, fArray, value, ftype, colType))
        dispatch(addTemplateColData())
    },
    displayIcons: (fObject, value) => { dispatch(displayIcons(fObject, value)) },
    hideIcons: (fObject, value) => { dispatch(hideIcons(fObject, value)) },
    updateBtnProperty: (fObject, value) => { dispatch(updateBtnProperty(fObject, value)) },
    deleteColumn: (fObject, index) => {
        dispatch(deleteColumn(fObject, index))
    },
    updateColumnName: (fObject, columnIndex, value) => {
        dispatch(updateColumnName(fObject, columnIndex, value))
    },
    updateColumnType: (fObject, columnIndex, value) => {
        dispatch(updateColumnType(fObject, columnIndex, value))
    },
    updateColumnSourceGroup: (fObject, columnIndex, value) => {
        dispatch(updateColumnSourceGroup(fObject, columnIndex, value))
    },
    updateTableFlyoutVisibility: (flag) => {
        dispatch(updateTableFlyoutVisibility(flag))
    },
    assignAddTableFunctionality: (func) => {
        dispatch(assignAddTableFunctionality(func))
    },
    addColumn: (fObject) => {
        dispatch(addColumn(fObject));
    },
    addConfigureFields: (fObject, value) => {
        dispatch(addConfigureFields(fObject, value))
    },
    createTripleDropdown: (fObject, value) => {
        console.log(fObject, value);
        dispatch(createTripleDropdown(fObject, value))
    },
    configureFields: (fObject, value) => {
        dispatch(configureFields(fObject, value))
    },
    showGlobalLoader: ()=>dispatch(showGlobalLoader()),
    hideGlobalLoader: ()=>dispatch(hideGlobalLoader()),
});

function mapStateToProps(state) {
    const templateState = state.templateReducer;
    return {
        sections: templateState.template.sections,
        templateArray: templateState.template
    }
}

class TableField extends React.Component {
    constructor(props) {
        super(props);
        this.templateService = new TemplateService();
        this.state = {
            isTableFlyoutOpen: false,
            addTableFunc: null,
            results: [],
            searchList: [
                { columnName: null, title: 'All fields marked with * are mandatory' },
                { columnName: null, title: 'RESET' },
                { columnName: null, title: 'FIND' },
                { columnName: null, title: 'SAVE' },
                { columnName: null, title: 'Catalyst Family' },
                { columnName: null, title: 'Popup Grid' }
            ],
            
            showConfirmation: false,
            fields: null,
            fieldIndex: null,
            fieldItem: null,
            tabColIndex: null,
            tabCol: false
        }
    }

    handleSearchChange(value) {
        console.log("handle search change =>", value);
        let convertedvalue = value.toLowerCase()
        let results;
        results = this.state.searchList.filter(item => {
            return item.title.toLowerCase().includes(convertedvalue)
        });
        console.log(results)
        this.setState({
            results
        })
    }

    showConfirmationPopup = (item, index, isTabCol) => {
        isTabCol ?
            this.setState({
                showConfirmation: true,
                fieldItem: item,
                tabColIndex: index,
                tabCol: true
            })
            :
            this.setState({
                showConfirmation: true,
                fields: item,
                fieldIndex: index,
                tabCol: false
            })
    }

    cancelDelete = () => {
        this.setState({
            showConfirmation: false,
            fields: null,
            fieldIndex: null,
            fieldItem: null,
            tabColIndex: null,
            tabCol: false
        })
    }

    proceedDelete = () => {
        this.setState({
            showConfirmation: false
        })
        this.state.tabCol ? this.props.deleteColumn(this.state.fieldItem, this.state.tabColIndex)
            : this.props.deleteControl(this.state.fields, this.state.fieldIndex)
    }

    isError = (item) => {
        console.log(item.field_label)
        console.log(this.props)
        if (!item.field_label && this.props.hasError) {
            return "Please enter the field value!"
        } else {
            return "";
        }
    }

    render() {
        return (
            <div>
               
                    <div key={this.props.fieldIndex} className={this.props.fieldItem.showHideIcons ? "card-border" : "card-without-border"}
                        style={this.props.buildTemplateStyle.cardContent}
                        onMouseLeave={this.props.hideIcons.bind(this, this.props.fieldItem, false)}
                        onMouseEnter={this.props.displayIcons.bind(this, this.props.fieldItem, true)}>

                        <>
                            <div className="template-table-flex">
                                <div className="tempalte-table-title">
                                    {this.props.fieldItem.table[0].table_name}
                                    {console.log('inside tablefield')}
                                </div>
                                <div className="table-column-cover">
                                    {this.props.fieldItem.table[0].rows.map((tabColVal, tabColInd) => {
                                        return (
                                            <div className="template-each-tab-col" key={tabColInd}>
                                                <div className="template-each-tab-col-upper-division">
                                                    <div className="template-each-tab-col-delete" /*onClick={this.props.deleteColumn.bind(this, this.props.fieldItem, tabColInd)}*/ onClick={this.showConfirmationPopup.bind(this, this.props.fieldItem, tabColInd, true)} >
                                                        <Icon className="button-detail-settings" name="delete" root="common" size="small" />
                                                    </div>
                                                    <div className="template-each-tab-col-name">
                                                        <Input fluid={true} value={this.props.fieldItem.table[0].rows[tabColInd].column_name} type="text" placeholder="Column Name" onChange={
                                                            this.props.updateColumnName.bind(this, this.props.fieldItem, tabColInd)
                                                        } />
                                                    </div>
                                                </div>
                                                <div className="template-each-tab-col-type">
                                                    <Select fluid={true} placeholder="Column Type" size="small" value={this.props.fieldItem.table[0].rows[tabColInd].column_type} options={avaialbleControlOptions} onChange={this.props.updateColumnType.bind(this, this.props.fieldItem, tabColInd)} />
                                                    {(() => {
                                                        switch (this.props.fieldItem.table[0].rows[tabColInd].column_type) {
                                                            case 'UOM':
                                                            case 'Lookup':
                                                            case 'Drop Down':
                                                                return (
                                                                    <Input fluid={true} value={this.props.fieldItem.table[0].rows[tabColInd].source_group} onChange={this.props.updateColumnSourceGroup.bind(this, this.props.fieldItem, tabColInd)} type="text" placeholder="Hint Text" search={true} />
                                                                );
                                                            // break;
                                                            default:
                                                        }
                                                    })()}
                                                </div>
                                            </div>)
                                    })}
                                    <div className="template-add-col-cover" onClick={this.props.addColumn.bind(this, this.props.fieldItem)}>
                                        <span>+</span><br /> Add Column
                                                    </div>
                                </div>
                            </div>
                        </>

                    </div>


                    {this.state.showConfirmation ?
                        <Confirmation showConfirmation={this.state.showConfirmation} cancelDelete={this.cancelDelete} proceedDelete={this.proceedDelete} />
                        : null
                    }
                
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TableField);
const avaialbleControlOptions = [{ value: 'TextBox', text: 'Textbox' }, { value: 'DateTimePicker', text: 'Date & Time' }, { value: 'Lookup', text: 'Lookup' }, { value: 'Comments', text: 'Comment Box' }, { value: 'Table', text: 'Table' },
{ value: 'UOM', text: 'UOM' }, { value: 'DatePicker', text: 'Date' }, { value: 'Drop Down', text: 'Drop Down' }, { value: 'Action Buttons', text: 'Action Buttons' },
{ value: 'Radio Button', text: 'Radio Button' }, { value: 'Toggle', text: 'Toggle' }, { value: 'Label', text: 'Label' }, { value: 'Button', text: 'Button' }, { value: 'Triple Dropdowns', text: 'Triple Dropdowns' }, { value: 'Flyout', text: 'Flyout' }, { value: 'Popup', text: 'Popup' }, { value: 'MultipleDropDown', text: 'MultipleDropDown' }, { value: 'link', text: 'Link' }]   
