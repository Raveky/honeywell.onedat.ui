import React from 'react';
import { Icon, Loader } from '@scuf/common';
import { connect } from 'react-redux';
import { DataTable } from '@scuf/datatable';
/* nidhi: added for compact theme of datatable, styles not working without this */
import '@scuf/datatable/honeywell-compact/theme.css'
import TemplateService from '../utils/template.service';
import Confirmation from './custom-popups/DeleteConfirmation';
import { ApiUrl } from '../constants';
import { showGlobalLoader, hideGlobalLoader } from './../actions/Template';


// TODO: add reducer actions for edit, clone and delete actions
const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    showGlobalLoader: () => dispatch(showGlobalLoader()),
    hideGlobalLoader: () => dispatch(hideGlobalLoader())
});


class GetAllTemplates extends React.Component {
    constructor(props) {
        super(props);
        this.templateService = new TemplateService();
        this.state = {
            find_results: 0,
            templateList: [],
            showConfirmation: false,
            cellData: null
        };
    }

    componentDidMount() {
        this.getListOfTemplates();
    }

    getListOfTemplates() {
        this.props.showGlobalLoader();
        let displayData = {};
        displayData.OperationType = "GET";
        displayData.Messages = [];
        let mesgObj = {
            template_name: "",
            table_name: "sc_template.template_master",
            fields: []
        };
        displayData.Messages.push(mesgObj);
        let url = ApiUrl.templateAPi
        this.templateService.CommonApiCall(url, displayData).then((res) => {
            if (res.data.dataResponse.length > 0) {
                let ModifiedArray = res.data.dataResponse[0].data;
                console.log('props 0', ModifiedArray);
                console.log('props====', this.props.templatename + "" + this.props.templatedesc + "" + this.props.modulename);
                this.setState({ templateList: [...ModifiedArray.reverse()] })
            } else {
                console.log('error');
            }
        })
            .finally(() => this.props.hideGlobalLoader());
    }
  
    editAction = (data, click) => {
        // nidhi: call reducer action for edit
        this.props.dispatch({
            type: 'EDIT_TEMPLATE',
            payload: data.rowData.template_data
        });
        this.props.editAction(data.rowData.template_id_sq);
    };
    cloneAction = (data, click) => {
        // nidhi: call reducer action for clone
        this.props.dispatch({
            type: 'CLONE_TEMPLATE',
            payload: data.rowData
        });
    };


    deleteAction = (cellData, event) => {
        let url = ApiUrl.templateAPi
        let displayData = {};
        displayData.OperationType = "DELETE";
        displayData.Messages = [];
        let mesgObj = {
            template_name: "",
            table_name: "sc_template.template_master",
            fields: [],
            filter: []
        };
        mesgObj.filter.push({
            "field_name": "template_id_sq",
            "field_type": "int",
            "field_value": cellData.rowData["template_id_sq"]
        });
        displayData.Messages.push(mesgObj);
        this.templateService.CommonApiCall(url, displayData).then((res) => {
            if (res) {
                this.getListOfTemplates();
            }
        });
    };

    actionsRenderer = (cellData) => {
        if (cellData.rowData.template_data != null) {
            // let templateData = JSON.parse(cellData.rowData.template_data);
            // console.log(templateData, cellData);
            let rowStatus = cellData.rowData.status;
            return <span className="find-table-actions">
                {rowStatus === 'Published' ? "" : <Icon root="common" name="edit" size="small" onClick={this.editAction.bind(this, cellData)} />}
                <Icon root="common" name="duplicate" size="small" onClick={this.cloneAction.bind(this, cellData)} />
                {rowStatus === 'Published' ? "" : <Icon root="common" name="delete" size="small" onClick={this.showConfirmationPopup.bind(this, cellData)} />}
            </span>;
        }
    };

    showConfirmationPopup = (cellData) => {
        this.setState({
            showConfirmation: true,
            cellData
        })
    }

    cancelDelete = () => {
        this.setState({
            showConfirmation: false
        })
    }

    proceedDelete = () => {
        this.setState({
            showConfirmation: false
        });
        this.deleteAction(this.state.cellData);
    }
    render() {
        return (
            <div>

                <DataTable data={this.props.templateAllList}>
                    <DataTable.HeaderBar>
                        <DataTable.HeaderBar.Item content={this.props.templateAllList.length + " Records Fetched"} />
                    </DataTable.HeaderBar>

                    <DataTable.Column className="find-table-header" field="template_name" header="Template Name" sortable={true}
                    />
                    <DataTable.Column field="version" header="Version" sortable={true} />
                    <DataTable.Column className="find-table-header" field="template_desc" header="Description" sortable={true} />
                    <DataTable.Column className="find-table-header" field="status" header="Status" sortable={true} />
                    <DataTable.Column className="find-table-header" field="action" header="Actions" renderer={this.actionsRenderer} align="right" />

                    <DataTable.Pagination />
                </DataTable>
                {this.state.showConfirmation ?
                    <Confirmation showConfirmation={this.state.showConfirmation} cancelDelete={this.cancelDelete} proceedDelete={this.proceedDelete} />
                    : null
                }

            </div>
        );
    }
}
export default connect(null, mapDispatchToProps)(GetAllTemplates);
