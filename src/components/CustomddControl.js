import React, { useEffect, useState, usePrevious } from 'react';
import { Select, Input, Icon } from '@scuf/common';
import ConversionService from './UomCalculation/UomConversion';

const UOMControlOptions = [{ value: 'gm', text: 'GRAMS' }, { value: 'kg', text: 'KILOGRAMS' },
{ value: 'MILLIGRAMS', text: 'MILLIGRAMS' }]

const conversionService = new ConversionService();

const CustomddControl = (props) => {
    const [uomValue, setUomValue] = useState(UOMControlOptions[0].value);
    // const prevUomValue = usePrevious(uomValue);
    const setUom = (data) => {
        const conversionFactor = conversionService.getConvertionFactor(uomValue, data);
        setUomValue(data);
        console.log('conversionFactor ', conversionFactor);
    }

    useEffect(() => {
        // console.log('here at uom level' , conversionService.getConvertionFactor('gm' , 'mg'));
    }, []);
    return (
        <div className="row" key={props.y}>
            <div className="col-sm-6 d-inline-block align-top pr-1">
                <Input type="text" fluid={true} placeholder={props.fieldItem.source_group}
                    indicator={(props.fieldItem.mandatory) ? "required" : ""}
                    label={props.fieldItem.field_label}
                    iconPosition="right" icon={<Icon name="search" root="common" size="small" />} />
            </div>
            <div className="col-sm-6 d-inline-block align-top mt-4 pt-1 pl-0">
                <Select fluid={true} onChange={(data) => setUom(data)} value={uomValue} placeholder="Select" size="small" options={UOMControlOptions} />
            </div>
        </div>
    );
}
export default CustomddControl;