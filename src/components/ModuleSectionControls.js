import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { showGlobalLoader, hideGlobalLoader  } from './../actions/Template';
import ModulePageControls from './ModulePageControls';
import { Accordion, Tab } from '@scuf/common';
import DisplayGrid from './DisplayGrid';

function mapStateToProps(state) {
    console.log("state =>", state);
    const templateState = state.templateReducer;
    return {
        templateModule: templateState.template,
        userName  : templateState.template.userName
    }
   
}
// to call the global loader
const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    showGlobalLoader: ()=>dispatch(showGlobalLoader()),
    hideGlobalLoader: ()=>dispatch(hideGlobalLoader())
});

const ModuleSectionControls = (props) => {
    return (
        <>
           {(props.templateModule.render_as === 'Tab') ?
                <Tab defaultActiveIndex={0} onTabChange={(activeIndex)=>(console.log(activeIndex))} > 
                    {props.templateModule.sections.map((secItem, s) =>
                        <Tab.Pane title={secItem.section_name}>
                            <React.Fragment key={s}>
                                <ModulePageControls secItem={secItem} keyVal={s}
                                    crud={props.crud} moduleUrl= {props.moduleUrl}
                                />
                            </React.Fragment>
                        </Tab.Pane>
                    )}
                </Tab>
            :
                <div>
                    {props.templateModule.sections.map((secItem, s) =>
                        <div key={s}>
                            <Accordion className={(secItem.section_name !== 'Template Grid') ? "preview-accoridian-pop module-accordian-pad" : ''}>
                                <Accordion.Content title={
                                    (secItem.section_name !== 'Template Grid') ?
                                        <div className="main-section" style={buildTemplateStyle.mainSectionbar}  >
                                            <div style={buildTemplateStyle.mainSectionheader}>{secItem.section_name}</div>
                                        </div>
                                        : ''

                                } arrowPosition="right">
                                    <div className="row">
                                        {console.log('inside..!', secItem)}
                                        {(secItem.section_name !== 'Template Grid') ?
                                            //  secItem.fields.map((fieldItem, y) => {
                                                <React.Fragment key={s}>
                                                    <ModulePageControls secItem={secItem} keyVal={s}
                                                        crud={props.crud} moduleUrl= {props.moduleUrl}
                                                    />
                                                </React.Fragment>
                                            //})
                                            :
                                            <div>
                                                <DisplayGrid crud={props.crud} moduleUrl= {props.moduleUrl}></DisplayGrid>
                                            </div>
                                        }
                                    </div>
                                </Accordion.Content>
                            </Accordion>
                        </div>
                    )}        
                </div>
            }
        </>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(ModuleSectionControls);

const buildTemplateStyle = {
    mainSectionbar: {
        display: 'flex',
        justifyContent: 'space-around',
    },
    mainSectionheader: {
        color: '#5f7d95',
        fontWeight: 'bold',
        flex: 1
       
    }
}