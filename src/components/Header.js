import React from 'react';
import { Header } from '@scuf/common';

const HeaderArea = (props) => {
  //console.log(props);
  return (
    <div>
      <Header logoUrl="/images/honeywell-logo.jpg" title="OneDAT" menu={true} onMenuToggle={props.onMenuToggle}>
       {/*  <Header.UserProfile firstName="Raj" lastName="Elavarasan" role="Demolitions">
          <Header.UserProfile.Item>Details</Header.UserProfile.Item>
        </Header.UserProfile> */}
      </Header>
    </div>
  );
};

export default HeaderArea;