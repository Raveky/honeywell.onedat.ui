import React from 'react';
import { Card, Button, Icon, Search, Loader } from '@scuf/common';
import ControlOptions from './ControlOptions';
import { connect } from 'react-redux';
import {
    getSectionName, addSection, deleteSection, toggleControls,
     resetTemplateBuild, updateTableFlyoutVisibility, handleSearchSelect,
     handleSearchChange, addResult, updateActiveSideBar, addTemplateColData
} from '../actions/Template';
import Sections from './Sections';
import Fields from './Fields';
import TableNameFlyOut from './Flyouts/TableNameFlyOut';
import TemplateGrid from './TemplateGrid';
import TemplateService from '../utils/template.service';
import { withRouter } from 'react-router-dom';
import {myRoutePath} from './Routes/Routes';
import ConfigurationModal from './ConfigurationModal';

function mapStateToProps(state) {
 //   console.log(state);
    const templateState = state.templateReducer;
    return {
        sections: templateState.sections,
        templateArray: templateState.template,
        results: templateState.results,
        value: templateState,
        sampleData: templateState
    }
}


const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    getSectionName: (value) => dispatch(getSectionName(value)),
    addSection: () => {
        dispatch(addSection())
        dispatch(addTemplateColData())
    },
    deleteSection: (v,e) => {
        e.stopPropagation();
        dispatch(deleteSection(v))
    },
    toggleControls: (index,e) =>{
        console.log(index, e);
        e.stopPropagation();
        dispatch(toggleControls(index))
    },
    resetTemplate: () => dispatch(resetTemplateBuild()),
    updateTableFlyoutVisibility: (flag)=>{
        dispatch(updateTableFlyoutVisibility(flag))
    },
    handleSearchSelect: (result) => dispatch(handleSearchSelect(result)),
    handleSearchChange: (value) => dispatch(handleSearchChange(value)),
    addResult: (value) => dispatch(addResult(value)),
    updateActiveSideBar:(value)=>dispatch(updateActiveSideBar(value))
});
class BuildTemplateForm extends React.Component {

    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props);
        this.templateService = new TemplateService();
        console.log("Props =>", this.props);
        this.state = {
            hasError: false,
            loading: false,
            showConfigurationModal: false
        }
    }
    
    configure() {
        let hasError = false;
        console.log( );
       if(this.props.templateArray.sections.length > 0 || this.props.templateArray.fields.length > 0){
        this.props.templateArray.sections.forEach(secItem => {
            secItem.fields.forEach((item, index) => {
                console.log(item, !item.field_label )
                if (!item.field_label) {
                    hasError = true
                }
            })
        })
        this.props.templateArray.sections.forEach(secItems => {
            secItems.sub_sections.forEach(sub_item => {sub_item.fields.forEach(field =>{
                if (!field.field_label) {
                    hasError = true
                
                }
              
        })})})
        this.props.templateArray.fields.forEach(fieldIt => {
                //console.log(item, !item.field_label )
                if (!fieldIt.field_label) {
                    hasError = true
                
                }
            })
       
        if (!hasError) {
            this.setState(
                {
                    showConfigurationModal: true, hasError: false
                })

        }else {
            this.setState(
                {
                    showConfigurationModal: false, hasError: true
                })
        }
    
    }
}

closeConfigurationModal = () => {
    this.setState({
        showConfigurationModal: false,
        flag: true
    },
        () => this.props.dispatch({
            type: 'TEMPLATE_ERROR',
            payload: this.state.flag
        }))
}
    submitData = () => {
        this.setState({showConfigurationModal: false, loading: true});
        console.log(this.props.templateArray);
        // console.log(JSON.stringify(this.props.templateArray));
         this.templateService.createTemplate(this.props.templateArray).then((res) =>{
            if(res) {
               // this.updateTemplate(res);
                let getName = myRoutePath.find((v,i)=>v.url === '/');
		        this.props.updateActiveSideBar(getName.path)
		        this.props.history.push('/');
            } else {
                console.log('error');
            }
        })
        .finally(() => this.setState({loading: false}))
    }

    showPreview() {
        if (this.props.templateArray.sections.length > 0 || this.props.templateArray.fields.length > 0) {
            this.props.history.push('/previewTemplate');
        }
    }
   

    showTempLevelControls() {
        this.props.dispatch({
            type: 'SHOW_HIDE_TEMP_CONTROLS',
            payload: !this.props.templateArray.showHideFieldControls
        });
    }

    addTempLevelControls(fType) {
        this.props.dispatch({
            type: 'ADD_TEMP_CONTROLS',
            payload: fType
        });
    }
    addTableFunc = (tableName) =>{
        console.log(this.props);
        this.props.templateArray.addTableFunc("Table", tableName)
    }

    render() {
        return (
            <>
            <Loader loading={this.state.loading} text=" " overlayColor="#a0a0a0" overlayOpacity="0.5">
                <TableNameFlyOut addTableFunc={this.addTableFunc} isTableFlyoutOpen={this.props.templateArray.isTableFlyoutOpen} updateTableFlyoutVisibility={this.props.updateTableFlyoutVisibility} />
                <div className="build-template-cover" style={buildTemplateStyle.titleCover}>
                    <div style={buildTemplateStyle.templateArea}>
                        <p style={buildTemplateStyle.smallTitle}>Template Name</p>
                        <p>{this.props.templateArray.template_name}</p>
                    </div>
                    <div style={buildTemplateStyle.versionArea}>
                        <p style={buildTemplateStyle.smallTitle}>Version</p>
                     <p> {this.props.templateArray.template_version} </p> 
                    
                    </div> 
                    <div style={buildTemplateStyle.descriptionArea}>
                        <p style={buildTemplateStyle.smallTitle}>Description</p>
                        <p>{this.props.templateArray.template_description}</p>
                    </div>
                </div>

                <Card interactive={true}>
                    <Card.Content style={buildTemplateStyle.cardCover}>
                        <div className="create-tempalte-add-controls-style">
                            <div className="">
                                <Search fluid={true}
                                    results={this.props.value.results}
                                    value={this.props.value.autoCompleteBoxValue}
                                    onResultSelect={(value) => this.props.handleSearchSelect(value)}
                                    onSearchChange={(value) => this.props.handleSearchChange(value)}
                                />
                                {(this.props.value.results.length === 0) ? <div className="">
                                    <Button type="primary" size="small" icon={<Icon root="global" name="slider-controls-plus" size="medium" />}
                                        iconPosition="left" content="ADDITEM" onClick={this.props.addResult.bind(this, this.props.value.autoCompleteBoxValue)} />
                                </div> : null}
                            </div>

                            <div className="addButtonStyle">
                                <Button type="primary" size="small" icon={<Icon root="global" name="slider-controls-plus" size="medium" />}
                                    iconPosition="left" content="ADD" onClick={this.props.addSection.bind(this)} />
                            </div>
                            <div className="">
                                <Button size="small"
                                    content="ADD CONTROLS" onClick={this.showTempLevelControls.bind(this)} />
                                {(this.props.templateArray.showHideFieldControls) ? <ControlOptions addSubSections={this.addTempLevelControls.bind(this)}
                                    setStyle={{ top: '10px', right: '50px' }} /> : ''}
                            </div>
                        </div>
                    </Card.Content>
                </Card>
                {
                    this.props.templateArray.fields.map((item, index) => {
                        return <div key={index}>
                          <Fields key={index} fieldIndex={index} fieldItem= {item}
                            parentArray={this.props.templateArray} buildTemplateStyle={buildTemplateStyle} type={'template'} hasError={this.state.hasError}> </Fields>
                        </div>
                    })
                }

                {

                    this.props.templateArray.sections.map((secItem, secIndex) => {
                        return <div key={secIndex}>
                            {(secItem.section_name === 'Template Grid') ? 
                                <div>
                                    <TemplateGrid sectionName = {secItem.section_name} mainIndex={null} secIndex ={secIndex}></TemplateGrid>
                                </div>
                            :
                                <Sections key={secIndex} secIndex={secIndex} secItem= {secItem}
                                parentArray={this.props.templateArray} buildTemplateStyle={buildTemplateStyle} mainIndex={null} hasError={this.state.hasError}></Sections>
                            }
                        </div>
                    })
                }

                <div style={buildTemplateStyle.footerButtonStyle}>
                    <Button type="secondary" size="small" content="RESET" onClick={this.props.resetTemplate.bind(this)} />
                    <Button type="secondary" size="small" content="PREVIEW" onClick={this.showPreview.bind(this)} />
                    <Button type="primary" size="small" content="SAVE" onClick={this.configure.bind(this)}/>
                </div>
                {this.state.showConfigurationModal ? 
                    <ConfigurationModal 
                        showConfigurationModal={this.state.showConfigurationModal} 
                        submitData={this.submitData.bind(this)} 
                        closeConfigurationModal={this.closeConfigurationModal.bind(this)} 
                    /> : null}
            </Loader>
            </>
        );
    }


};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BuildTemplateForm));

const stateOptions = [{ value: 'General Information', text: 'General Information', sub: false }, 
{ value: 'Mixing', text: 'Mixing', sub: true }, { value: 'Powder Mix', text: 'Powder Mix', sub: true }, 
{ value: 'Water Addition', text: 'Water Addition', sub: false }, 
{ value: 'Mixing Instructions', text: 'Mixing Instructions', sub: false },
{ value: 'Template Grid', text: 'Template Grid'}];

const buildTemplateStyle = {
    titleCover: {
        display: 'flex',
        fontWeight: 'bold',
        gap: 12,
    },
    footerButtonStyle: {
        justifyContent: 'flex-start',
        margin: '10px 0px'
    },
    templateArea: {
        padding: '0 10px',
        borderRight: '1px solid #80808066'
    },
    versionArea: {
        padding: '0 10px',
        borderRight: '1px solid #80808066'
    },
    descriptionArea: {
        padding: '0 10px',
    },
    smallTitle: {
        fontSize: '11px',
        fontWeight: 'normal'
    },
    cardCover: {
        padding: '10px 10px'
    },
    selectBoxStyle: {
        width: '40%',
        float: 'left'
    },
    rightToButton: {
        float: 'right'
    },
    mainSectionbar: {
        //background: '#d1dee894',
        //padding: '10px',
        display: 'flex',
        justifyContent: 'space-around'
    },
    mainSectionheader: {
        color: '#5f7d95',
        fontWeight: 'bold',
        flex: 1
    },
    mainSectionControl: {
        borderRight: '1px solid #80808066',
        padding: '3px 10px',
        color: 'black',
        fontWeight: 'bold',
        cursor: 'pointer'
    },
    makeBorderZero: {
        border: 0
    },
    cardContent: {
        display: 'flex',
        justifyContent: 'flex-start',
        border: '0px solid',
        position: 'relative'
    },
    selectStyle: {
        width: '24%',
        margin: '0 0.5%',
    },
    avselectStyle: {
        width: '10%',
        margin: '0 0.5%',
    },

    checkBoxStyle: {
        top: '20px'
    },
    checkBoxTitle: {
        fontWeight: 'bold'
    }
}
