import React from 'react';
import ConversionModel from './UomConversion';


export default class ConversionService extends React.Component {

    treeStructure = [];

    fromNodes = [];

    nodeIndex = -1;
    finalCf = 1;

    arr = [
        {
            'from': 'gm',
            'to': 'kg',
            'cf': 0.0001
        }, {
            'from': 'kg',
            'to': 'gm',
            'cf': 1000
        }, {
            'from': 'gm',
            'to': 'mg',
            'cf': 1000
        }];


    // uom calculation
    checkObjectPresent(arr, from, to) {
        let isPresent;
        isPresent = false;
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].from === from && arr[i].to === to) {
                isPresent = true;
                break;
            }
        }
        return isPresent;
    }
    rearrangeJson(data) {

        let obj = new ConversionModel();

        for (let i = 0; i < data.length; i++) {

            if (data[i].baseUomCode !== null && data[i].uomConversionFactor !== null && data[i].uomCode !== null) {

                obj = new ConversionModel();

                obj['from'] = data[i]['uomCode'].toString().toLowerCase();

                obj['to'] = data[i]['baseUomCode'].toString().toLowerCase();

                obj['cf'] = data[i]['uomConversionFactor'];
                if (!this.checkObjectPresent(this.arr, obj['from'], obj['to'])) {
                    this.arr.push(obj);
                }

                obj = new ConversionModel();

                obj['from'] = data[i]['baseUomCode'].toString().toLowerCase();

                obj['to'] = data[i]['uomCode'].toString().toLowerCase();

                obj['cf'] = (1 / data[i]['uomConversionFactor']);

                if (!this.checkObjectPresent(this.arr, obj['from'], obj['to'])) {
                    this.arr.push(obj);
                }

            }

        }
        this.arr = [
            {
                'from': 'gm',
                'to': 'kg',
                'cf': 0.0001
            }, {
                'from': 'kg',
                'to': 'gm',
                'cf': 1000
            },
            {
                'from': 'gm',
                'to': 'mg',
                'cf': 1000
            }];
        console.log('rearranged', this.arr);

        return this.arr;

    }

    generateTree(frm, to) {
        if (frm && to) {

            const bestCase = this.arr.filter(x => x.from === frm && x.to === to);
            // if (bestCase.length > 0) { // bestCase.length > 0
            //     this.treeStructure.push({id: this.nodeIndex, parentNode: this.nodeIndex - 1, nodeName: bestCase[0].from + '-' + bestCase[0].to, cf: bestCase[0].cf, isFinal: true });
            // } else {

            this.nodeIndex = this.nodeIndex + 1;

            if (this.fromNodes.indexOf(frm) === -1) {

                this.fromNodes.push(frm);

            }

            console.log('parameter ', frm, to);

            const requiredSetOfData = this.arr.filter(x => x.from === frm);

            console.log('result ', requiredSetOfData);

            if (requiredSetOfData.length > 0) {

                for (let i = 0; i < requiredSetOfData.length; i++) {

                    console.log(requiredSetOfData[i].from, requiredSetOfData[i].to);

                    if (requiredSetOfData[i].to !== to) {

                        console.log('check after data ', requiredSetOfData[i].to, to);

                        if (this.fromNodes.indexOf(requiredSetOfData[i].to) === -1) {

                            this.treeStructure.push({ id: this.nodeIndex, parentNode: (this.nodeIndex - 1), nodeName: requiredSetOfData[i].to, cf: requiredSetOfData[i].cf });

                            this.generateTree(requiredSetOfData[i].to, to);

                        }

                    } else {

                        if (this.nodeIndex !== 0) {
                            this.treeStructure.push({ id: this.nodeIndex, parentNode: (this.nodeIndex - 1), nodeName: frm, cf: requiredSetOfData[i].cf });
                        }
                        this.treeStructure.push({ id: this.nodeIndex + 1, parentNode: this.nodeIndex, nodeName: to, cf: requiredSetOfData[i].cf, isFinal: true });

                        console.log('reached the destination');

                    }

                }

                //}

                console.log('fdsfdsgdsfgs', requiredSetOfData);
            }

        }

    }


    getParentNode(parentNodeId, finalCf) {

        const parentObj = this.treeStructure.filter(x => x.id === parentNodeId);

        console.log('parentOBJ', parentNodeId, parentObj, finalCf);

        if (parentObj.length > 0) {

            this.finalCf = this.finalCf * parentObj[0].cf;

            if (parentNodeId !== -1) {

                this.getParentNode(parentObj[0].parentNode, this.finalCf);

            }

        }

        return this.finalCf;

    }

    getConvertionFactor(frm, to) {
        if (frm && to) {
            frm = frm.toString().toLowerCase();
            to = to.toString().toLowerCase();
            // this.initialFrom = frm;
            // console.log('initialFrom', this.initialFrom);
        }
        const bestCase = this.arr.filter(x => x.from === frm && x.to === to);
        if (bestCase.length > 0) {
            this.treeStructure.push({ id: this.nodeIndex, parentNode: -1, nodeName: bestCase[0].from + '-' + bestCase[0].to, cf: bestCase[0].cf, isFinal: true });
        } else {
            this.generateTree(frm, to);
        }
        console.log('this.treeStructure', JSON.stringify(this.treeStructure));
        const leafNode = this.treeStructure.filter(x => x.isFinal === true);

        if (leafNode.length > 0) {

            console.log(leafNode[0].parentNode);

            this.finalCf = 1;

            this.finalCf = this.getParentNode(leafNode[0].parentNode, this.finalCf);

            console.log('finalCf ', this.finalCf);

            this.treeStructure = [];

            this.fromNodes = [];

            this.nodeIndex = -1;

            return this.finalCf;

        } else {

            return 1;

        }

    }

}



