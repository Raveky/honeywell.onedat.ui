// import React, { useState } from "react";
// import { connect } from "react-redux";
// import {
//   Card,
//   Select,
//   Button,
//   Icon,
//   DatePicker,
//   Input,
//   TextArea,
//   Accordion,
//   Checkbox,
//   Radio,
// } from "@scuf/common";
// import CustomddControl from "./CustomddControl";
// import DisplayGrid from "./DisplayGrid";
// import { useHistory } from "react-router-dom";
// import ActionDetails from "./../constants/enum";
// import {
//   configureFlyoutSearchModal,
//   fetchFamilyName,
//   configurePopup,
// } from "../actions/Template";
// import ConfigureFlyoutSearchModal from "./Flyouts/ConfigureFlyoutSearchModal";
// import ConfigurePopup from "./Flyouts/ConfigurePopup";
// import TemplateService from "./../utils/template.service";

// const templateService = new TemplateService();
// const mapDispatchToProps = (dispatch) => ({
//   configureFlyoutSearchModal: (fObject, value) => {
//     dispatch(configureFlyoutSearchModal(fObject, value));
//   },
//   fetchFamilyName: (value) => {
//     dispatch(fetchFamilyName(value));
//   },
//   configurePopup: (fObject, value) => {
//     dispatch(configurePopup(fObject, value));
//   },
// });
// function mapStateToProps(state) {
//   const templateState = state.templateReducer;
//   return {
//     templateArray: templateState.template,
//     familyName: templateState.template.familyName,
//   };
// }
// const avaialbleControlOptions = [
//   { value: "Option1", text: "Option1" },
//   { value: "Option2", text: "Option2" },
// ];

// const statusList = [
//   { value: "Active", text: "Active" },
//   { value: "InActive", text: "InActive" },
// ];

// const PreviewTemplate = (props) => {
//   let history = useHistory();
//   const [value, setValue] = useState("");
//   const [tValue, setTValue] = useState("");
//   const goBack = () => {
//     history.goBack();
//   };
//   console.log(props.templateArray.sections);

//   const openFetchFlyoutSearchModal = (val) => {
//     props.configureFlyoutSearchModal(val, true);
//     props.fetchFamilyName();
//   };
//   const openPopup = (val) => {
//     props.configurePopup(val, true);
//   };

//   const controlChange = (param) => {
//     makeServiceCall(param, "change");
//   };

//   const makeServiceCall = (param, eventType) => {
//     console.log(param);
//     let obj = param.find((v, i) => {
//       return v.event == eventType;
//     });
//     if (obj) {
//       console.log("button click", obj, obj.event);
//       let getUrl = ActionDetails.find((v, i) => {
//         return obj.url == v.name;
//       });
//       console.log(getUrl);
//       serviceGETMethod(getUrl);
//     }
//   };

//   const serviceGETMethod = function (getUrl) {
//     if (getUrl) {
//       props.showGlobalLoader();
//       templateService
//         .getMethod(getUrl.url)
//         .then((res) => {
//           console.log(res);
//         })
//         .finally(() => props.hideGlobalLoader());
//     }
//   };
//   return (
//     <div>
//       {/* <Button type="secondary" size="small" content="Preview Template" onClick={goBack} /> */}
//       <div className="preview-back-button" onClick={goBack}>
//         <Icon
//           className="preview-back-arr-ico"
//           root="global"
//           name="caret-left"
//           exactSize={16}
//           color="#1274B7"
//         />
//         Preview Template
//       </div>
//       <div
//         className="build-template-cover"
//         style={buildTemplateStyle.titleCover}
//       >
//         <div style={buildTemplateStyle.templateArea}>
//           <p style={buildTemplateStyle.smallTitle}>Template Name</p>
//           <p>{props.templateArray.template_name}</p>
//         </div>
//         <div style={buildTemplateStyle.versionArea}>
//           <p style={buildTemplateStyle.smallTitle}>Version</p>
//           <p>{props.templateArray.template_version}</p>
//         </div>
//         <div style={buildTemplateStyle.descriptionArea}>
//           <p style={buildTemplateStyle.smallTitle}>Description</p>
//           <p>{props.templateArray.template_description}</p>
//         </div>
//       </div>

//       {/* {props.templateArray.fields.map((fieldItem, y) => ( */}
//         {props.templateArray.fields ? 
//         <div>
//           <Accordion>
//             <Accordion.Content>
//               <div className="row preview-accordian-content">
//                 {
//                 props.templateArray.fields.map((fieldItem, y) => {
//                     return (
//                         <React.Fragment key={y}>
//                     {fieldItem.field_type === "TextBox" ? (
//                       <div className="col-sm-4" key={y}>
//                         <Input
//                           fluid={true}
//                           type="text"
//                           placeholder="Hint Text"
//                           indicator={fieldItem.mandatory ? "required" : ""}
//                           label={fieldItem.field_label}
//                         />
//                       </div>
//                     ) : (
//                       ""
//                     )}
//                     {fieldItem.field_type === "Lookup" ||
//                     fieldItem.field_type === "UOM" ? (
//                       <CustomddControl
//                         fieldItem={fieldItem}
//                         y={y}
//                       ></CustomddControl>
//                     ) : (
//                       ""
//                     )}
//                     {fieldItem.field_type === "Comments" ? (
//                       <div
//                         className="col-sm-12 preview-text-area-cover"
//                         key={y}
//                       >
//                         <TextArea
//                           fluid={true}
//                           label={fieldItem.field_label}
//                           indicator={fieldItem.mandatory ? "required" : ""}
//                           placeholder="Input multiline data here..."
//                         />
//                       </div>
//                     ) : (
//                       ""
//                     )}
//                     {fieldItem.field_type === "DateTimePicker" ||
//                     fieldItem.field_type === "DatePicker" ? (
//                       <div className="col-sm-4" key={y}>
//                         <DatePicker
//                           fluid={true}
//                           label={fieldItem.field_label}
//                           placeholder="MM/DD/YYYY"
//                           type={
//                             fieldItem.field_type === "DateTimePicker"
//                               ? "datetime"
//                               : "date"
//                           }
//                           indicator={fieldItem.mandatory ? "required" : ""}
//                         ></DatePicker>
//                       </div>
//                     ) : (
//                       ""
//                     )}
//                     {fieldItem.field_type === "Toggle" ? (
//                       <div
//                         className="col-sm-4 d-inline-block"
//                         key={props.keyVal}
//                       >
//                         <Checkbox
//                           fluid={true}
//                           style={buildTemplateStyle.checkBoxStyle}
//                           toggle={true}
//                           label={fieldItem.field_label}
//                           onChange={() => setTValue(fieldItem.field_label)}
//                         />
//                       </div>
//                     ) : null}

//                     {fieldItem.field_type === "Drop Down" ? (
//                       <div className="col-sm-4" key={y}>
//                         <Select
//                           fluid={true}
//                           label={fieldItem.field_label}
//                           placeholder="Select"
//                           size="small"
//                           indicator={fieldItem.mandatory ? "required" : ""}
//                           options={
//                             fieldItem.source_group === "Status"
//                               ? statusList
//                               : avaialbleControlOptions
//                           }
//                         />
//                       </div>
//                     ) : (
//                       ""
//                     )}

//                     {fieldItem.field_type === "Button" ? (
//                       <div className="col-sm-12" key={y}>
//                         <Button
//                           type="primary"
//                           content={fieldItem.field_label}
//                         />
//                       </div>
//                     ) : (
//                       ""
//                     )}
//                     {fieldItem.field_type === "Label" ? (
//                       <p className="col-sm-12" key={y}>
//                         {fieldItem.field_label}
//                       </p>
//                     ) : (
//                       ""
//                     )}
//                     {fieldItem.field_type === "Action Buttons" ? (
//                       <div className="mr-1">
//                         <Button
//                           type={
//                             fieldItem.button_property ? "primary" : "secondary"
//                           }
//                           content={fieldItem.field_label}
//                         ></Button>
//                       </div>
//                     ) : (
//                       ""
//                     )}

//                     {fieldItem.field_type === "MultipleDropDown" ? (
//                       <div className="mr-1">
//                         <Select
//                           label={fieldItem.field_label}
//                           placeholder="Select Multiple States"
//                           options={avaialbleControlOptions}
//                           multiple={true}
//                         />
//                       </div>
//                     ) : (
//                       ""
//                     )}
//                     {fieldItem.field_type === "Radio Button" ? (
//                       <>
//                         <span>{fieldItem.field_label}</span>
//                         {fieldItem.fieldNameArr.map((fieldvalue, index) => (
//                           <Radio
//                             name="RadioGroup"
//                             checked={value === fieldvalue}
//                             label={fieldvalue}
//                             onChange={() => {
//                               setValue(fieldvalue);
//                               console.log(value);
//                             }}
//                           />
//                         ))}
//                       </>
//                     ) : (
//                       " "
//                     )}

//                     {fieldItem.field_type === "Triple Dropdowns" ? (
//                       <div className="col-sm-4" key={props.keyVal}>
//                         <div className="col-sm-4 module-triple-select">
//                           <Select
//                             fluid={true}
//                             label={fieldItem.fieldNameArr[0]}
//                             placeholder="Select"
//                             size="small"
//                             indicator={fieldItem.mandatory ? "required" : ""}
//                             options={
//                               fieldItem.source_group === "Status"
//                                 ? statusList
//                                 : avaialbleControlOptions
//                             }
//                             onChange={() => {
//                               controlChange(fieldItem.actions);
//                             }}
//                           />
//                         </div>
//                         <div className="col-sm-4 module-triple-select">
//                           <Select
//                             fluid={true}
//                             label={fieldItem.fieldNameArr[1]}
//                             placeholder="Select"
//                             size="small"
//                             indicator={fieldItem.mandatory ? "required" : ""}
//                             options={
//                               fieldItem.source_group === "Status"
//                                 ? statusList
//                                 : avaialbleControlOptions
//                             }
//                             onChange={() => {
//                               controlChange(fieldItem.actions);
//                             }}
//                           />
//                         </div>
//                         <div className="col-sm-4 module-triple-select">
//                           <Select
//                             fluid={true}
//                             label={fieldItem.fieldNameArr[2]}
//                             placeholder="Select"
//                             size="small"
//                             indicator={fieldItem.mandatory ? "required" : ""}
//                             options={
//                               fieldItem.source_group === "Status"
//                                 ? statusList
//                                 : avaialbleControlOptions
//                             }
//                             onChange={() => {
//                               controlChange(fieldItem.actions);
//                             }}
//                           />
//                         </div>
//                       </div>
//                     ) : (
//                       ""
//                     )}

//                     {fieldItem.field_type === "link" ? (
//                       <p className="col-sm-12" key={y}>
//                         <a href={fieldItem.actions[0].resource} target="_blank">
//                           {fieldItem.field_label}
//                         </a>
//                       </p>
//                     ) : (
//                       ""
//                     )}

//                     {fieldItem.field_type === "Flyout" ? (
//                       <>
//                         <div className="col-sm-4" key={y}>
//                           <Input
//                             fluid={true}
//                             type="text"
//                             placeholder={fieldItem.field_label}
//                             value={props.familyName}
//                             indicator={fieldItem.mandatory ? "required" : ""}
//                             label={fieldItem.field_label}
//                             onClick={() =>
//                               openFetchFlyoutSearchModal(fieldItem)
//                             }
//                             iconPosition="right"
//                             icon={
//                               <Icon name="search" root="common" size="small" />
//                             }
//                           />
//                         </div>
//                         {fieldItem.showFlyoutSearchModal ? (
//                           <ConfigureFlyoutSearchModal
//                             fieldItem={fieldItem}
//                             fieldType={fieldItem.field_type}
//                             open={fieldItem.showFlyoutSearchModal}
//                             closeEvent={() =>
//                               props.configureFlyoutSearchModal(fieldItem, false)
//                             }
//                           />
//                         ) : (
//                           ""
//                         )}
//                       </>
//                     ) : (
//                       ""
//                     )}
//                     {fieldItem.field_type === "Popup" ? (
//                       <>
//                         <div className="col-sm-4" key={y}>
//                           <Input
//                             fluid={true}
//                             type="text"
//                             placeholder={fieldItem.field_label}
//                             indicator={fieldItem.mandatory ? "required" : ""}
//                             label={fieldItem.field_label}
//                             iconPosition="right"
//                             icon={
//                               <Icon
//                                 name="communication"
//                                 root="common"
//                                 size="small"
//                                 onClick={() => openPopup(fieldItem)}
//                               />
//                             }
//                           />
//                         </div>
//                         {fieldItem.showPopup ? (
//                           <ConfigurePopup
//                             fieldItem={fieldItem}
//                             fieldType={fieldItem.field_type}
//                             open={fieldItem.showPopup}
//                             closeEvent={() =>
//                               props.configurePopup(fieldItem, false)
//                             }
//                           />
//                         ) : (
//                           ""
//                         )}
//                       </>
//                     ) : (
//                       ""
//                     )}
//                   </React.Fragment>
//                     )
//                 })
                  
//                 }
//               </div>
//             </Accordion.Content>
//           </Accordion>
//         </div>
//         :
//         ""
//         }

//       {props.templateArray.sections.map((i, v) => (
//         <div key={v}>
//           <Accordion
//             className={
//               i.section_name !== "Template Grid" ? "preview-accoridian-pop" : ""
//             }
//           >
//             <Accordion.Content
//               title={i.section_name !== "Template Grid" ? i.section_name : ""}
//               arrowPosition={i.section_name !== "Template Grid" ? "right" : ""}
//             >
//               <div className="row preview-accordian-content">
//                 {i.section_name !== "Template Grid" ? (
//                   i.fields.map((fieldItem, y) => {
//                     return (
//                       <React.Fragment key={y}>
//                         {fieldItem.field_type === "TextBox" ? (
//                           <div className="col-sm-4" key={y}>
//                             <Input
//                               fluid={true}
//                               type="text"
//                               placeholder="Hint Text"
//                               indicator={fieldItem.mandatory ? "required" : ""}
//                               label={fieldItem.field_label}
//                             />
//                           </div>
//                         ) : (
//                           ""
//                         )}
//                         {fieldItem.field_type === "Lookup" ||
//                         fieldItem.field_type === "UOM" ? (
//                           <CustomddControl
//                             fieldItem={fieldItem}
//                             y={y}
//                           ></CustomddControl>
//                         ) : (
//                           ""
//                         )}
//                         {fieldItem.field_type === "Comments" ? (
//                           <div
//                             className="col-sm-12 preview-text-area-cover"
//                             key={y}
//                           >
//                             <TextArea
//                               fluid={true}
//                               label={fieldItem.field_label}
//                               indicator={fieldItem.mandatory ? "required" : ""}
//                               placeholder="Input multiline data here..."
//                             />
//                           </div>
//                         ) : (
//                           ""
//                         )}
//                         {fieldItem.field_type === "DateTimePicker" ||
//                         fieldItem.field_type === "DatePicker" ? (
//                           <div className="col-sm-4" key={y}>
//                             <DatePicker
//                               fluid={true}
//                               label={fieldItem.field_label}
//                               placeholder="MM/DD/YYYY"
//                               type={
//                                 fieldItem.field_type === "DateTimePicker"
//                                   ? "datetime"
//                                   : "date"
//                               }
//                               indicator={fieldItem.mandatory ? "required" : ""}
//                             ></DatePicker>
//                           </div>
//                         ) : (
//                           ""
//                         )}

//                         {fieldItem.field_type === "Toggle" ? (
//                           <div
//                             className="col-sm-4 d-inline-block"
//                             key={props.keyVal}
//                           >
//                             <Checkbox
//                               fluid={true}
//                               style={buildTemplateStyle.checkBoxStyle}
//                               toggle={true}
//                               label={fieldItem.field_label}
//                               onChange={() => setTValue(fieldItem.field_label)}
//                             />
//                           </div>
//                         ) : null}

//                         {fieldItem.field_type === "Radio Button" ? (
//                           <>
//                             <span>{fieldItem.field_label}</span>
//                             {fieldItem.fieldNameArr.map((fieldvalue, index) => (
//                               <Radio
//                                 name="RadioGroup"
//                                 checked={value === fieldvalue}
//                                 label={fieldvalue}
//                                 onChange={() => {
//                                   setValue(fieldvalue);
//                                   console.log(value);
//                                 }}
//                               />
//                             ))}
//                           </>
//                         ) : (
//                           " "
//                         )}
//                         {fieldItem.field_type === "Drop Down" ? (
//                           <div className="col-sm-4" key={y}>
//                             <Select
//                               fluid={true}
//                               label={fieldItem.field_label}
//                               placeholder="Select"
//                               size="small"
//                               indicator={fieldItem.mandatory ? "required" : ""}
//                               options={
//                                 fieldItem.source_group === "Status"
//                                   ? statusList
//                                   : avaialbleControlOptions
//                               }
//                             />
//                           </div>
//                         ) : (
//                           ""
//                         )}

//                         {fieldItem.field_type === "Button" ? (
//                           <div className="col-sm-12" key={y}>
//                             <Button
//                               type="primary"
//                               content={fieldItem.field_label}
//                             />
//                           </div>
//                         ) : (
//                           ""
//                         )}
//                         {fieldItem.field_type === "Label" ? (
//                           <p className="col-sm-12" key={y}>
//                             {fieldItem.field_label}
//                           </p>
//                         ) : (
//                           ""
//                         )}
//                         {fieldItem.field_type === "Action Buttons" ? (
//                           <div className="mr-1">
//                             <Button
//                               type={
//                                 fieldItem.button_property
//                                   ? "primary"
//                                   : "secondary"
//                               }
//                               content={fieldItem.field_label}
//                             ></Button>
//                           </div>
//                         ) : (
//                           ""
//                         )}

//                         {fieldItem.field_type === "MultipleDropDown" ? (
//                           <div className="mr-1">
//                             <Select
//                               label={fieldItem.field_label}
//                               placeholder="Select Multiple States"
//                               options={avaialbleControlOptions}
//                               multiple={true}
//                             />
//                           </div>
//                         ) : (
//                           ""
//                         )}

//                         {fieldItem.field_type === "Triple Dropdowns" ? (
//                           <div className="col-sm-4" key={props.keyVal}>
//                             <div className="col-sm-4 module-triple-select">
//                               <Select
//                                 fluid={true}
//                                 label={fieldItem.fieldNameArr[0]}
//                                 placeholder="Select"
//                                 size="small"
//                                 indicator={
//                                   fieldItem.mandatory ? "required" : ""
//                                 }
//                                 options={
//                                   fieldItem.source_group === "Status"
//                                     ? statusList
//                                     : avaialbleControlOptions
//                                 }
//                                 onChange={() => {
//                                   controlChange(fieldItem.actions);
//                                 }}
//                               />
//                             </div>
//                             <div className="col-sm-4 module-triple-select">
//                               <Select
//                                 fluid={true}
//                                 label={fieldItem.fieldNameArr[1]}
//                                 placeholder="Select"
//                                 size="small"
//                                 indicator={
//                                   fieldItem.mandatory ? "required" : ""
//                                 }
//                                 options={
//                                   fieldItem.source_group === "Status"
//                                     ? statusList
//                                     : avaialbleControlOptions
//                                 }
//                                 onChange={() => {
//                                   controlChange(fieldItem.actions);
//                                 }}
//                               />
//                             </div>
//                             <div className="col-sm-4 module-triple-select">
//                               <Select
//                                 fluid={true}
//                                 label={fieldItem.fieldNameArr[2]}
//                                 placeholder="Select"
//                                 size="small"
//                                 indicator={
//                                   fieldItem.mandatory ? "required" : ""
//                                 }
//                                 options={
//                                   fieldItem.source_group === "Status"
//                                     ? statusList
//                                     : avaialbleControlOptions
//                                 }
//                                 onChange={() => {
//                                   controlChange(fieldItem.actions);
//                                 }}
//                               />
//                             </div>
//                           </div>
//                         ) : (
//                           ""
//                         )}

//                         {fieldItem.field_type === "Flyout" ? (
//                           <>
//                             <div className="col-sm-4" key={y}>
//                               <Input
//                                 fluid={true}
//                                 type="text"
//                                 placeholder={fieldItem.field_label}
//                                 value={props.familyName}
//                                 indicator={
//                                   fieldItem.mandatory ? "required" : ""
//                                 }
//                                 label={fieldItem.field_label}
//                                 onClick={() =>
//                                   openFetchFlyoutSearchModal(fieldItem)
//                                 }
//                                 iconPosition="right"
//                                 icon={
//                                   <Icon
//                                     name="search"
//                                     root="common"
//                                     size="small"
//                                   />
//                                 }
//                               />
//                             </div>
//                             {fieldItem.showFlyoutSearchModal ? (
//                               <ConfigureFlyoutSearchModal
//                                 fieldItem={fieldItem}
//                                 fieldType={fieldItem.field_type}
//                                 open={fieldItem.showFlyoutSearchModal}
//                                 closeEvent={() =>
//                                   props.configureFlyoutSearchModal(
//                                     fieldItem,
//                                     false
//                                   )
//                                 }
//                               />
//                             ) : (
//                               ""
//                             )}
//                           </>
//                         ) : (
//                           ""
//                         )}

//                         {fieldItem.field_type === "link" ? (
//                           <p className="col-sm-12" key={y}>
//                             <a
//                               href={fieldItem.actions[0].resource}
//                               target="_blank"
//                             >
//                               {fieldItem.field_label}
//                             </a>
//                           </p>
//                         ) : (
//                           ""
//                         )}

//                         {fieldItem.field_type === "Popup" ? (
//                           <>
//                             <div className="col-sm-4" key={y}>
//                               <Input
//                                 fluid={true}
//                                 type="text"
//                                 placeholder={fieldItem.field_label}
//                                 indicator={
//                                   fieldItem.mandatory ? "required" : ""
//                                 }
//                                 label={fieldItem.field_label}
//                                 iconPosition="right"
//                                 icon={
//                                   <Icon
//                                     name="communication"
//                                     root="common"
//                                     size="small"
//                                     onClick={() => openPopup(fieldItem)}
//                                   />
//                                 }
//                               />
//                             </div>
//                             {fieldItem.showPopup ? (
//                               <ConfigurePopup
//                                 fieldItem={fieldItem}
//                                 fieldType={fieldItem.field_type}
//                                 open={fieldItem.showPopup}
//                                 closeEvent={() =>
//                                   props.configurePopup(fieldItem, false)
//                                 }
//                               />
//                             ) : (
//                               ""
//                             )}
//                           </>
//                         ) : (
//                           ""
//                         )}
//                       </React.Fragment>
//                     );
//                   })
//                 ) : (
//                   <div>
//                     <DisplayGrid tempArray={props.templateArray}></DisplayGrid>
//                   </div>
//                 )}
//               </div>
//             </Accordion.Content>
//           </Accordion>
//         </div>
//       ))}
//     </div>
//   );
// };

// export default connect(mapStateToProps, mapDispatchToProps)(PreviewTemplate);

import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import ModulePage from './ModulePage';
import { connect } from "react-redux";
import {Icon} from "@scuf/common";

function mapStateToProps(state) {
      const templateState = state.templateReducer;
      return {
        templateArray: templateState.template,
        familyName: templateState.template.familyName,
      };
    }

const PreviewTemplate = (props) => {
    const id = props.id;
    let history = useHistory();
    const goBack = () => {
        // history.goBack();
        history.push(`/buildTemplate/${id}`)
    };
    return(
        <>
        <div className="preview-back-button" onClick={goBack}>
            <Icon
            className="preview-back-arr-ico"
            root="global"
            name="caret-left"
            exactSize={16}
            color="#1274B7"
            />
            Preview Template
        </div>
        {/* <div
            className="build-template-cover"
            style={buildTemplateStyle.titleCover}
        >
            <div style={buildTemplateStyle.templateArea}>
            <p style={buildTemplateStyle.smallTitle}>Template Name</p>
            <p>{props.templateArray.template_name}</p>
            </div>
            <div style={buildTemplateStyle.versionArea}>
            <p style={buildTemplateStyle.smallTitle}>Version</p>
            <p>{props.templateArray.template_version}</p>
            </div>
            <div style={buildTemplateStyle.descriptionArea}>
            <p style={buildTemplateStyle.smallTitle}>Description</p>
            <p>{props.templateArray.template_description}</p>
            </div>
        </div> */}
        <ModulePage id={id} crud="no_crud" />
        </>
    )
}

export default PreviewTemplate;
const buildTemplateStyle = {
    titleCover: {
      display: "flex",
      fontWeight: "bold",
      gap: 12,
    },
    footerButtonStyle: {
      justifyContent: "flex-start",
      margin: "10px 0px",
    },
    templateArea: {
      padding: "0 10px",
      borderRight: "1px solid #80808066",
    },
    versionArea: {
      padding: "0 10px",
      borderRight: "1px solid #80808066",
    },
    descriptionArea: {
      padding: "0 10px",
    },
    smallTitle: {
      fontSize: "11px",
      fontWeight: "normal",
    },
    cardCover: {
      padding: "10px 10px",
    },
    selectBoxStyle: {
      width: "40%",
      float: "left",
    },
    rightToButton: {
      float: "right",
    },
    mainSectionbar: {
      background: "#d1dee894",
      padding: "10px",
      display: "flex",
      justifyContent: "space-around",
    },
    mainSectionheader: {
      color: "#5f7d95",
      fontWeight: "bold",
      flex: 1,
    },
    mainSectionControl: {
      borderRight: "1px solid #80808066",
      padding: "3px 10px",
      color: "black",
      fontWeight: "bold",
      cursor: "pointer",
    },
    makeBorderZero: {
      border: 0,
    },
    cardContent: {
      display: "flex",
      justifyContent: "flex-start",
    },
    selectStyle: {
      //  width: '24%',
      margin: "0 0.5%",
    },
  
    checkBoxStyle: {
      top: "20px",
    },
    checkBoxTitle: {
      fontWeight: "bold",
    },
  };