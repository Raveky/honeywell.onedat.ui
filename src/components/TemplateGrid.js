import React from 'react';
import { Accordion, Icon, Input, Select } from '@scuf/common';
import { connect } from 'react-redux';
import { deleteSection, addTemplateColData, updateTemplateGridData, deleteColumn, updateColumnName, updateAddedColumnDetails, updateColumnType,
    updateColumnSourceGroup, addColumn, addColumnLevelControl, attachField } from '../actions/Template';
import TemplateGridSettingFlyOut from './Flyouts/TemplateGridSettingFlyOut';
import Confirmation from './custom-popups/DeleteConfirmation';
import ColumnControlOptions from './ColumnControlOptions';
import GridPopupConfigure from './custom-popups/GridPopupConfigure';


function mapStateToProps(state) {
    const templateState = state.templateReducer;
    return {
        templateArray: templateState.template
    }
}

const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    deleteSection: (index, mainIndex,  e) => {
        e.stopPropagation();
        dispatch(deleteSection(index, mainIndex))
        dispatch(addTemplateColData())
    },
    updateTemplateGridData: (param)=>{
        dispatch(updateTemplateGridData(param))
    },
    deleteColumn: (fObject, index) => {
        dispatch(deleteColumn(fObject, index))
    },
    updateColumnName: (fObject, columnIndex, value) => {
        dispatch(updateColumnName(fObject, columnIndex, value))
    },
    updateAddedColumnDetails: (fObject, columnIndex, value) => {
        dispatch(updateAddedColumnDetails(fObject, columnIndex, value))
    },
    updateColumnType: (fObject, columnIndex, value) => {
        dispatch(updateColumnType(fObject, columnIndex, value))
    },
    updateColumnSourceGroup: (fObject, columnIndex, value) => {
        dispatch(updateColumnSourceGroup(fObject, columnIndex, value))
    },
    addColumn: (fObject) => {
        dispatch(addColumn(fObject));
    },
    addColumnLevelControl: (fObject, columnIndex) => { 
        dispatch(addColumnLevelControl(fObject, columnIndex)) 
    },
    attachField: (fObject, columnIndex, value) => { 
        dispatch(attachField(fObject, columnIndex, value)) 
    },
});
let fItem = '';
let tabIndex = '';
class TemplateGrid extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenTemplateGridFlyout: false,
            showConfirmation: false,
            index: null,
            mainIndex: null,
            event: null,
            showPopupGrid: false
        }
    }

    closeTemplateGridFlyout =() => {
        console.log('template grid flyout');
        this.setState({
            isOpenTemplateGridFlyout: false
        })
    }  

    openTemplateGridFlyout = (event) => {
        event.stopPropagation();
        console.log('template grid flyout');
        this.setState({
            isOpenTemplateGridFlyout: true
        })
    }

    showConfirmationPopup = (index, mainIndex, e) => {
        this.setState({
            showConfirmation: true,
            index,
            mainIndex,
            event: e
        })
    }

    cancelDelete = () => {
        this.setState({
            showConfirmation: false
        })
    }

    proceedDelete = () => {
        this.setState({
            showConfirmation: false
        })
        this.props.deleteSection(this.state.index, this.state.mainIndex, this.state.event);
    }

    showConfigurationPage = (fObj, tabColInd) => {
        fItem = fObj;
        tabIndex = tabColInd;
        this.setState({
            showPopupGrid: true
        })
    }

    cancelPopup = () => {
        this.setState({
            showPopupGrid: false
        })
    }

    render() {
        return(
            <React.Fragment>
                <TemplateGridSettingFlyOut closeTemplateGridFlyout={this.closeTemplateGridFlyout} isOpen={this.state.isOpenTemplateGridFlyout} templateArray={this.props.templateArray} updateTemplateGridData={this.props.updateTemplateGridData} />
                <Accordion className="preview-accoridian-pop">
                    <Accordion.Content title={
                        <div className="main-section" style={buildTemplateStyle.mainSectionbar}  >
                            <div style={buildTemplateStyle.mainSectionheader}>{this.props.sectionName}</div>
                            
                            <Icon style={buildTemplateStyle.mainSectionControl} root="common" name="settings" size="small" onClick={this.openTemplateGridFlyout} />
                            
                            <Icon style={{...buildTemplateStyle.mainSectionControl, ...buildTemplateStyle.makeBorderZero }}
                                root="common" name="delete" size="small" 
                                 onClick={(e)=>this.showConfirmationPopup(this.props.secIndex, this.props.mainIndex, e)} />
                        </div>
                        } arrowPosition="right">
                        {
                            this.props.templateArray.sections.map((sObj, sIndex) => 
                            (sObj.section_name === 'Template Grid')?sObj.fields.map((fObj, fIndex) => {
                                return <>
                                <div className="template-table-flex">
                                    <div className="tempalte-table-title">
                                        {fObj.table[0].table_name}
                                    </div>
                                    <div className="table-column-cover">
                                        {fObj.table[0].rows.map((tabColVal, tabColInd) => {
                                            return (
                                                <div className="template-each-tab-col" key={tabColInd}>
                                                    <div className="template-each-tab-col-upper-division">
                                                        <div className="template-grid-col" >
                                                        {(() => {
                                                            switch (fObj.table[0].rows[tabColInd].column_type) {
                                                                case 'Button':
                                                                    return (                                                                        
                                                                        <>
                                                                        <Icon className="button-detail-settings" 
                                                                            name={fObj.table[0].rows[tabColInd].attachedField==="Popup" ? 
                                                                                "display" 
                                                                                : fObj.table[0].rows[tabColInd].attachedField==="Flyout" ? 
                                                                                "flight-attendant"
                                                                                : "add"} 
                                                                            root={fObj.table[0].rows[tabColInd].attachedField==="Popup" ? 
                                                                                "common" 
                                                                                : fObj.table[0].rows[tabColInd].attachedField==="Flyout" ? 
                                                                                "aero"
                                                                                : "building"} 
                                                                            size="small" 
                                                                            onClick={this.props.addColumnLevelControl.bind(this, fObj, tabColInd)}
                                                                        />
                                                                        {(fObj.table[0].rows[tabColInd].showControlList) ?
                                                                            <ColumnControlOptions className="field-control-list"
                                                                                attachField={(key)=>this.showConfigurationPage(fObj, tabColInd, key)}
                                                                                setStyle={{ top: '15px'}} />
                                                                                : ''}
                                                                        </>
                                                                    );
                                                                default:
                                                            }
                                                        })()}
                                                            {fObj.table[0].rows[tabColInd].disabled ? '' :<Icon className="button-detail-settings" name="delete" root="common" size="small" onClick={this.props.deleteColumn.bind(this, fObj, tabColInd)} />}
                                                        </div>
                                                        <div className="template-each-tab-col-name">
                                                            <Input fluid={true} disabled={fObj.table[0].rows[tabColInd].disabled} value={fObj.table[0].rows[tabColInd].column_name} type="text" placeholder="Column Name" 
                                                            onChange={this.props.updateAddedColumnDetails.bind(this, fObj, tabColInd)} />
                                                        </div>
                                                    </div>
                                                    <div className="template-each-tab-col-type">
                                                        <Select fluid={true} disabled={fObj.table[0].rows[tabColInd].disabled} placeholder="Column Type" size="small" value={fObj.table[0].rows[tabColInd].column_type} options={avaialbleControlOptions} onChange={this.props.updateColumnType.bind(this, fObj, tabColInd)} />
                                                        {(() => {
                                                            switch (fObj.table[0].rows[tabColInd].column_type) {
                                                                case 'UOM':
                                                                case 'Lookup':
                                                                case 'Drop Down':
                                                                    return (
                                                                        <Input fluid={true} disabled={fObj.table[0].rows[tabColInd].disabled} value={fObj.table[0].rows[tabColInd].source_group} onChange={this.props.updateColumnSourceGroup.bind(this, fObj, tabColInd)} type="text" placeholder="Hint Text" search={true} />
                                                                    );
                                                                // break;
                                                                default:
                                                            }
                                                        })()}
                                                    </div>
                                                </div>)
                                        })}
                                        <div className="template-add-col-cover" onClick={this.props.addColumn.bind(this, fObj)}>
                                            <span>+</span><br /> Add Column
                                        </div>
                                    </div>
                                </div>
                            </>
                                
                            }):""
                            )}
                    </Accordion.Content>
                </Accordion>
                {this.state.showConfirmation ?
                    <Confirmation showConfirmation={this.state.showConfirmation} cancelDelete={this.cancelDelete} proceedDelete={this.proceedDelete} />
                : null
                }
                {this.state.showPopupGrid ?
                    <GridPopupConfigure showPopupGrid={this.state.showPopupGrid}
                    cancelPopup={this.cancelPopup} fObj={fItem}  tabColInd = {tabIndex}/>
                : null
                }
            </React.Fragment>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TemplateGrid);

const buildTemplateStyle = {
    titleCover: {
        display: 'flex',
        fontWeight: 'bold',
        gap: 12,
    },
    footerButtonStyle: {
        justifyContent: 'flex-start',
        margin: '10px 0px'
    },
    templateArea: {
        padding: '0 10px',
        borderRight: '1px solid #80808066'
    },
    versionArea: {
        padding: '0 10px',
        borderRight: '1px solid #80808066'
    },
    descriptionArea: {
        padding: '0 10px',
    },
    smallTitle: {
        fontSize: '11px',
        fontWeight: 'normal'
    },
    cardCover: {
        padding: '10px 10px'
    },
    selectBoxStyle: {
        width: '40%',
        float: 'left'
    },
    rightToButton: {
        float: 'right'
    },
    mainSectionbar: {
        //background: '#d1dee894',
        //padding: '10px',
        display: 'flex',
        justifyContent: 'space-around'
    },
    mainSectionheader: {
        color: '#5f7d95',
        fontWeight: 'bold',
        flex: 1
    },
    mainSectionControl: {
        borderRight: '1px solid #80808066',
        padding: '3px 10px',
        color: 'black',
        fontWeight: 'bold',
        cursor: 'pointer'
    },
    makeBorderZero: {
        border: 0
    },
    cardContent: {
        display: 'flex',
        justifyContent: 'flex-start',
        border: '0px solid',
        position: 'relative'
    },
    selectStyle: {
        width: '24%',
        margin: '0 0.5%',
    },
    avselectStyle: {
        width: '10%',
        margin: '0 0.5%',
    },

    checkBoxStyle: {
        top: '20px'
    },
    checkBoxTitle: {
        fontWeight: 'bold'
    }
}

const avaialbleControlOptions = [{ value: 'TextBox', text: 'Textbox' }, { value: 'DateTimePicker', text: 'Date & Time' }, { value: 'Lookup', text: 'Lookup' }, { value: 'Comments', text: 'Comment Box' }, { value: 'Table', text: 'Table' },
{ value: 'UOM', text: 'UOM' }, { value: 'DatePicker', text: 'Date' }, { value: 'Drop Down', text: 'Drop Down' }, { value: 'Action Buttons', text: 'Action Buttons' },
{ value: 'Radio Button', text: 'Radio Button' }, { value: 'Toggle', text: 'Toggle' }, { value: 'Label', text: 'Label' }, { value: 'Button', text: 'Button' }, { value: 'Triple Dropdowns', text: 'Triple Dropdowns' }, { value: 'Flyout', text: 'Flyout' }, { value: 'Popup', text: 'Popup' }]