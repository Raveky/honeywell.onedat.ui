import React from 'react';
import { Icon, Popup } from '@scuf/common';

const ColumnControlOptions = (props) => {
    const controlsArr = [
        {
            name: 'flight-attendant',
            root: 'aero',
            spanName: 'Flyout',
            key: 'Flyout'
        },
        {
            name: 'display',
            root: 'common',
            spanName: 'Popup',
            key: 'Popup'
        }
    ]

    const controlClick = (e, key, type) => {
        e.stopPropagation();
        console.log(props);
        console.log("key and type: ", key, type);
        props.attachField(key, type);
    }

    const locStyle = {
        marginTop: props.setStyle.top,
        right: props.setStyle.right
    }

    return (

        <>
            <div style={{ ...controlStyle.controlPageCover, ...locStyle }}>
                <div style={controlStyle.triangleBox}></div>
                {
                    controlsArr.map((i, v) => {
                        return (<div style={controlStyle.individualControlCover} key={v} onClick={(e) => {
                            controlClick(e, i.key, i.type)
                        }}>
                            <Icon style={controlStyle.iconStyle} name={i.name} size="small" root={i.root} />
                            <span style={controlStyle.nameAlign}>{i.spanName} </span>
                        </div>)
                    })
                }
            </div>
        </>
    )
}

export default ColumnControlOptions;
const controlStyle = {
    controlPageCover: {
        display: 'flex',
        position: 'absolute',
        background: 'white',
        right: '23px',
        width: '250px',
        flexWrap: 'wrap',
        padding: '10px',
        marginTop: '31px',
        boxShadow: 'grey 0px 0px 8px 1px',
        zIndex: '999',
        fontWeight: 'normal',
        color: 'black'
    },
    iconStyle: {
        marginTop: '2px'
    },
    individualControlCover: {
        width: '50%',
        cursor: 'pointer'
    },
    nameAlign: {
        padding: '0px 5px',
        fontSize: '10px'
    },
    triangleBox: {
        position: 'absolute',
        width: 0,
        height: 0,
        borderLeft: '10px solid transparent',
        borderRight: '10px solid transparent',
        borderBottom: '10px solid white',
        top: '-10px',
        right: '150px'
    }
}

