import React from 'react';
import { Select, Icon, Input, Checkbox, Search, Loader } from '@scuf/common';
import ControlOptions from '../containers/ControlOptions';
import { connect } from 'react-redux';
import ButtonDetailFlyOut from './Flyouts/ButtonDetailFlyOut';
import ConfigureFieldsFlyOut from './Flyouts/ConfigureFieldsFlyOut';

import {
    clearFieldName, updateFieldName, updateFieldType, updateSourceGroup, updateMandatory, openHideButtonDetails,
    addButtonDetails, cloneControl, addFieldLevelControl, deleteControl, addFieldControls, hideIcons, displayIcons,
    updateTableFlyoutVisibility, assignAddTableFunctionality, deleteColumn, updateColumnName, updateColumnType,
    updateColumnSourceGroup, addColumn, updateBtnProperty, addConfigureFields, createTripleDropdown,
    addTemplateColData, configureFields, createRadioButton, showGlobalLoader, hideGlobalLoader, updateFlyoutGridData
} from '../actions/Template';
import TemplateService from '../utils/template.service';
import Confirmation from './custom-popups/DeleteConfirmation';
import TableField from './TableField';
import { ApiUrl } from '../constants';
import TemplateGridSettingFlyOut from './Flyouts/TemplateGridSettingFlyOut';

const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    updateSourceGroup: (fObject, value) => dispatch(updateSourceGroup(fObject, value)),
    updateFieldName: (fObject, value) => {
        console.log(fObject, value);
        dispatch(updateFieldName(fObject, value))
        dispatch(addTemplateColData())
    },
    clearFieldName: (fObject, value) => {
        dispatch(clearFieldName(fObject, value))
    },
    updateFieldType: (fObject, value) => dispatch(updateFieldType(fObject, value)),
    updateMandatory: (fObject, value) => dispatch(updateMandatory(fObject, value)),
    openHideButtonDetails: (fObject, value) => { dispatch(openHideButtonDetails(fObject, value)) },
    addButtonDetails: (fObject, value) => { dispatch(addButtonDetails(fObject, value)) },
    cloneControl: (fObject, fArray, value) => {
        dispatch(cloneControl(fObject, fArray, value))
        dispatch(addTemplateColData())
    },
    addFieldLevelControl: (fObject, value) => { dispatch(addFieldLevelControl(fObject, value)) },
    deleteControl: (fObject, value) => {
        dispatch(deleteControl(fObject, value))
        dispatch(addTemplateColData())
    },
    addFieldControls: (fObject, fArray, value, ftype, colType) => {
        dispatch(addFieldControls(fObject, fArray, value, ftype, colType))
        dispatch(addTemplateColData())
    },
    displayIcons: (fObject, value) => { dispatch(displayIcons(fObject, value)) },
    hideIcons: (fObject, value) => { dispatch(hideIcons(fObject, value)) },
    updateBtnProperty: (fObject, value) => { dispatch(updateBtnProperty(fObject, value)) },
    deleteColumn: (fObject, index) => {
        dispatch(deleteColumn(fObject, index))
    },
    updateColumnName: (fObject, columnIndex, value) => {
        dispatch(updateColumnName(fObject, columnIndex, value))
    },
    updateColumnType: (fObject, columnIndex, value) => {
        dispatch(updateColumnType(fObject, columnIndex, value))
    },
    updateColumnSourceGroup: (fObject, columnIndex, value) => {
        dispatch(updateColumnSourceGroup(fObject, columnIndex, value))
    },
    updateTableFlyoutVisibility: (flag) => {
        dispatch(updateTableFlyoutVisibility(flag))
    },
    assignAddTableFunctionality: (func) => {
        dispatch(assignAddTableFunctionality(func))
    },
    addColumn: (fObject) => {
        dispatch(addColumn(fObject));
    },
    addConfigureFields: (fObject, value) => {
        dispatch(addConfigureFields(fObject, value))
    },
    createTripleDropdown: (fObject, value) => {
        console.log(fObject, value);
        dispatch(createTripleDropdown(fObject, value))

    },
    createRadioButton: (fObject, value) => {
        console.log(fObject);
        console.log(value)
        dispatch(createRadioButton(fObject, value))

    },
    configureFields: (fObject, value) => {
        dispatch(configureFields(fObject, value))
    },
    updateFlyoutGridData: (fObject, value)=>{
        dispatch(updateFlyoutGridData(fObject, value))
    },
    showGlobalLoader: () => dispatch(showGlobalLoader()),
    hideGlobalLoader: () => dispatch(hideGlobalLoader()),
});

function mapStateToProps(state) {
    const templateState = state.templateReducer;
    return {
        sections: templateState.template.sections,
        templateArray: templateState.template,
        searchList: templateState.searchList,
        firstRender: templateState.firstRender
    }
}

class Fields extends React.Component {
    constructor(props) {
        super(props);
        this.templateService = new TemplateService();
        this.state = {
            isTableFlyoutOpen: false,
            addTableFunc: null,
            results: [],
            showConfirmation: false,
            fields: null,
            fieldIndex: null,
            fieldItem: null,
            tabColIndex: null,
            tabCol: false,
            isOpenTemplateGridFlyout: false,
            currFieldItem: {}
        }
    }

    componentDidMount() {
        this.props.showGlobalLoader();
        const tableNames = this.props.templateArray.tableName.split(".");
        let displayData = {};
        displayData.OperationType = "GET";
        displayData.Messages = [];
        let mesgObj = {
            table_name: tableNames[1]
        };
        displayData.Messages.push(mesgObj);
        let url = ApiUrl.getTableColNames
        this.templateService.CommonApiCall(url, displayData).then((res) => {
            if (res.data.dataResponse[0].data.length > 0) {
                let data = [];
                res.data.dataResponse[0].data.map((item, index) => {
                    if (item.title != null) {
                        data.push(item);
                    }
                    return data;
                }); //dispatch the update_searchlist from here 
                let dataVal = [...data]
                if(this.props.firstRender === true){
                console.log('method dispatched here')
                this.props.dispatch({
                    type: 'UPDATE_SEARCH_LIST',     //Action Dispatched
                    payload: dataVal
                })
                }
            } else {
                console.log('error');
            }
        })
            .finally(() => this.props.hideGlobalLoader());
    }

    handleSearchChange(value) {
        console.log("handle search change =>", value);
        let convertedvalue = value.toLowerCase()
        console.log(convertedvalue)
        let results;
        //let test = [...this.props.searchList];
        console.log(this.props.searchList)
        results = this.props.searchList.filter(item => {

            return item.title.toLowerCase().includes(convertedvalue)
        })

        this.setState({
            results
        })}


    showConfirmationPopup = (item, index, isTabCol) => {
        isTabCol ?
            this.setState({
                showConfirmation: true,
                fieldItem: item,
                tabColIndex: index,
                tabCol: true
            })
            :
            this.setState({
                showConfirmation: true,
                fields: item,
                fieldIndex: index,
                tabCol: false
            })
    }

    cancelDelete = () => {
        this.setState({
            showConfirmation: false,
            fields: null,
            fieldIndex: null,
            fieldItem: null,
            tabColIndex: null,
            tabCol: false
        })
    }

    proceedDelete = () => {
        this.setState({
            showConfirmation: false
        })
        this.state.tabCol ? this.props.deleteColumn(this.state.fieldItem, this.state.tabColIndex)
            : this.props.deleteControl(this.state.fields, this.state.fieldIndex)
    }

    isError = (item) => {
        console.log(item.field_label)
        console.log(this.props)
        if (!item.field_label && this.props.hasError) {
            return "Please enter the field value!"
        } else {
            return "";
        }
    }

    resultSelectBeforeDispatch = (fObject, value) => {
        console.log(value, fObject);
        let tempList = [];
        //Action Dispatched
        this.props.updateFieldName(fObject, value)
        tempList = [...this.props.searchList]
        console.log(this.props.searchList)
        tempList.forEach((item, index) => {
            console.log(item.title, value.title)
            if (item.title === value.title) {
                tempList.splice(index, 1)
                console.log(tempList)
                this.props.dispatch({
                    type: 'UPDATE_SEARCH',     //Action Dispatched
                    payload: tempList
                })
            }
            else {
                console.log('in the else part')
            }
        })

    }

    clearFieldNameBeforeDispatch = (fObject, value) => {
        let currentLabel = fObject.field_label;
        let filteredValue = this.props.searchList.find((searchValue, searchIndex) => currentLabel === searchValue.title)
        if (!filteredValue) {
            let newSearchList = [...this.props.searchList,{ columnname: null, title: currentLabel }]
            this.props.dispatch({
                type: 'UPDATE_SEARCH',     //Action Dispatched
                payload: newSearchList
            })
        }
        console.log(this.state, fObject, value)
        this.props.clearFieldName(fObject, value)

    }

    updateTemplateGridData = (data) => {
        console.log("data", data);
        this.props.updateFlyoutGridData(this.state.currFieldItem, data);
    }

    closeTemplateGridFlyout = () =>{
        this.setState({
            isOpenTemplateGridFlyout: false
        })
    }

    openFlyoutManager = (fieldItem,b) => {
        console.log(fieldItem, b);
        if(fieldItem.field_type === 'Flyout') {
            this.setState({
                isOpenTemplateGridFlyout: true,
                currFieldItem: fieldItem
            })
        } else {
            this.props.openHideButtonDetails(fieldItem, true)
        }
        
    }

    render() {
        return (
            <div>
                <TemplateGridSettingFlyOut currFieldItem={this.state.currFieldItem} closeTemplateGridFlyout={this.closeTemplateGridFlyout} isOpen={this.state.isOpenTemplateGridFlyout} templateArray={this.props.templateArray} updateTemplateGridData={this.updateTemplateGridData} />
                <div key={this.props.fieldIndex} className={this.props.fieldItem.showHideIcons ? "card-border" : "card-without-border"}
                    style={this.props.buildTemplateStyle.cardContent}
                    onMouseLeave={this.props.hideIcons.bind(this, this.props.fieldItem, false)}
                    onMouseEnter={this.props.displayIcons.bind(this, this.props.fieldItem, true)}>

                    {
                        (() => {
                            switch (this.props.fieldItem.field_type) {
                                case 'Table':
                                    return (
                                        <>

                                            <TableField key={this.props.key} fieldItem={this.props.fieldItem} fieldIndex={this.props.fieldIndex} parentArray={this.props.parentArray} buildTemplateStyle={this.props.buildTemplateStyle}>

                                            </TableField>
                                        </>
                                    );

                                // break;
                                default:
                                    return (<div className="w-100 d-inline-block" >
                                        {(this.props.fieldItem.field_type !== 'Triple Dropdowns') ? <div className="col-sm-3 d-inline-block align-top global-search-type">
                                            <div className="fields-field-name"><b>Field Name</b></div>
                                            <Search fluid={true}
                                                results={this.state.results}
                                                value={this.props.fieldItem.field_label}
                                                onResultSelect={this.resultSelectBeforeDispatch.bind(this, this.props.fieldItem)}
                                                onSearchChange={(value) => this.handleSearchChange(value)}
                                                onResultClear={this.clearFieldNameBeforeDispatch.bind(this, this.props.fieldItem)}
                                                error={this.isError(this.props.fieldItem)}
                                                indicator="required"
                                                data-tip data-for="fieldNameTip"
                                            />
                                            {/* <button >
                                                Register
                                             </button> */}
                                            {/* {
                                                this.props.fieldItem.field_label != '' ?
                                                    <ReactTooltip id="fieldNameTip" place="top" effect="solid">
                                                        {this.props.fieldItem.field_label}
                                                    </ReactTooltip> : null
                                            } */}

                                        </div> : ""}
                                        <div className="col-sm-3 d-inline-block align-top">
                                            <Select fluid={true} label="Field Type" placeholder="Select Field Type" size="small"
                                                value={this.props.fieldItem.field_type} options={avaialbleControlOptions}
                                                onChange={this.props.updateFieldType.bind(this, this.props.fieldItem)} />
                                        </div>

                                        {
                                            (this.props.fieldItem.field_type === 'Lookup' || this.props.fieldItem.field_type === 'UOM' || this.props.fieldItem.field_type === 'Drop Down' || this.props.fieldItem.field_type === 'MultipleDropDown' || this.props.fieldItem.field_type === 'Flyout') ?
                                                <div className="col-sm-3 d-inline-block align-top">
                                                    <Input fluid={true} value={this.props.fieldItem.source_group} type="text" placeholder="Hint Text"
                                                        onChange={this.props.updateSourceGroup.bind(this, this.props.fieldItem)} label="Source Group" search={true} />
                                                </div>
                                                : ""
                                        }

                                        {

                                            (this.props.fieldItem.field_type !== 'Label' && this.props.fieldItem.field_type !== 'Button' && this.props.fieldItem.field_type !== 'Action Buttons') ?
                                                <div className="col-sm-1 d-inline-block align-top">
                                                    <div style={this.props.buildTemplateStyle.checkBoxTitle}>Mandatory</div>
                                                    <Checkbox style={this.props.buildTemplateStyle.checkBoxStyle} toggle={true}
                                                        checked={this.props.fieldItem.mandatory} onChange={this.props.updateMandatory.bind(this, this.props.fieldItem)} />
                                                </div> : null
                                        }

                                        {
                                            (this.props.fieldItem.field_type === 'Triple Dropdowns' ?
                                                <>
                                                    <div className="col-sm-1 d-inline-block marginConfigureFields">
                                                        <Icon name="double-caret-left" root="common" size="large" color="black"
                                                            onClick={this.props.configureFields.bind(this, this.props.fieldItem, true)} />
                                                    </div>
                                                    {(this.props.fieldItem.showConfigureFieldPopup) ?
                                                        <ConfigureFieldsFlyOut fieldType='Triple Dropdowns' open={this.props.fieldItem.showConfigureFieldPopup} closeEvent={this.props.configureFields.bind(this, this.props.fieldItem, false)}
                                                            addConfigureFields={this.props.addConfigureFields.bind(this, this.props.fieldItem)} createTripleDropdown={this.props.createTripleDropdown.bind(this, this.props.fieldItem)}
                                                            fieldLabArr={this.props.fieldItem.fieldNameArr} sourceGrpArr={this.props.fieldItem.sourceGroupArr} />
                                                        : ""}
                                                </>
                                                : null
                                            )
                                        }
                                        {
                                            (this.props.fieldItem.field_type === 'Radio Button' ?
                                                <>
                                                    <div className="col-sm-1 d-inline-block marginConfigureFields">
                                                        <Icon name="double-caret-left" root="common" size="large" color="black"
                                                            onClick={this.props.configureFields.bind(this, this.props.fieldItem, true)} />
                                                    </div>
                                                    {(this.props.fieldItem.showConfigureFieldPopup) ?
                                                        <ConfigureFieldsFlyOut fieldType='Radio Button' open={this.props.fieldItem.showConfigureFieldPopup} closeEvent={this.props.configureFields.bind(this, this.props.fieldItem, false)}
                                                            addConfigureFields={this.props.addConfigureFields.bind(this, this.props.fieldItem)} createRadioButton={this.props.createRadioButton.bind(this, this.props.fieldItem)}
                                                            fieldLabArr={this.props.fieldItem.fieldNameArr} sourceGrpArr={this.props.fieldItem.sourceGroupArr} />
                                                        : ""}
                                                </>
                                                : null
                                            )
                                        }
                                    </div>)
                                //  break;
                            }
                        })()
                    }

                    {
                        <ButtonDetailFlyOut open={this.props.fieldItem.showButtonPopup} closeEvent={this.props.openHideButtonDetails.bind(this, this.props.fieldItem, false)}
                            fieldValue={this.props.fieldItem} addButtonDetails={this.props.addButtonDetails.bind(this, this.props.fieldItem)} />}
                    {(this.props.fieldItem.showHideIcons && this.props.fieldItem.field_type !== 'Table') ?
                        <div className="temp-section-control">
                            <div className="section-controls-cover">
                                <Icon root="global" name="settings" size="small"
                                    onClick={this.openFlyoutManager.bind(this, this.props.fieldItem)}
                                />


                                <Icon name="duplicate" root="common" size="small" onClick={this.props.cloneControl.bind(this, this.props.fieldItem, this.props.parentArray.fields)} />
                                <Icon name="add" root="building" size="small" onClick={this.props.addFieldLevelControl.bind(this, this.props.fieldItem)} />
                                {(this.props.fieldItem.showControlList) ?
                                    <ControlOptions className="field-control-list"
                                        updateTableFlyoutVisibility={this.props.updateTableFlyoutVisibility.bind(this)}
                                        assignAddTableFunctionality={this.props.assignAddTableFunctionality.bind(this)}
                                        addSubSections={this.props.addFieldControls.bind(this, this.props.fieldItem, this.props.parentArray.fields, this.props.fieldIndex)}
                                        setStyle={{ top: '287px', right: '-26px' }} /> : ''}
                                <Icon name="delete" root="common" size="small"
                                    /*onClick={this.props.deleteControl.bind(this, this.props.parentArray.fields, this.props.fieldIndex)}*/
                                    onClick={this.showConfirmationPopup.bind(this, this.props.parentArray.fields, this.props.fieldIndex, false)} />

                            </div>
                        </div>
                        : null}

                </div>
                {this.state.showConfirmation ?
                    <Confirmation showConfirmation={this.state.showConfirmation} cancelDelete={this.cancelDelete} proceedDelete={this.proceedDelete} />
                    : null
                }
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Fields);
const avaialbleControlOptions = [{ value: 'TextBox', text: 'Textbox' }, { value: 'DateTimePicker', text: 'Date & Time' }, { value: 'Lookup', text: 'Lookup' }, { value: 'Comments', text: 'Comment Box' }, { value: 'Table', text: 'Table' },
{ value: 'UOM', text: 'UOM' }, { value: 'DatePicker', text: 'Date' }, { value: 'Drop Down', text: 'Drop Down' }, { value: 'Action Buttons', text: 'Action Buttons' },
{ value: 'Radio Button', text: 'Radio Button' }, { value: 'Toggle', text: 'Toggle' }, { value: 'Label', text: 'Label' }, { value: 'Button', text: 'Button' }, { value: 'Triple Dropdowns', text: 'Triple Dropdowns' }, { value: 'Flyout', text: 'Flyout' }, { value: 'Popup', text: 'Popup' }, { value: 'MultipleDropDown', text: 'MultipleDropDown' }, { value: 'link', text: 'Link' }]   
