import React from 'react';
import { DataTable  } from '@scuf/datatable';
import { Icon, Loader, Button, Select } from '@scuf/common';
import '@scuf/datatable/honeywell-compact/theme.css'
import { connect } from 'react-redux';
import TemplateService from '../utils/template.service';
import Confirmation from './custom-popups/DeleteConfirmation';
import ConfigurePopup from './Flyouts/ConfigurePopup';
import ConfigureFlyoutSearchModal from './Flyouts/ConfigureFlyoutSearchModal';
import PayloadGenerator from '../utils/PayloadGenerator';
import { ApiUrl } from '../constants';
import { showGlobalLoader, hideGlobalLoader  } from './../actions/Template';

function mapStateToProps(state) {
    console.log("insideDisplayGrid State ===>", state);
    const templateState = state.templateReducer;
    return {
        dataList: templateState.module_grid_data,
        renderArray: templateState.template,
        dataColumnArray: templateState.templateGrid,
        // defaultColumns: templateState.templateGrid.fields[0].table[0].default_columns
    }
}

const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    showGlobalLoader: ()=>dispatch(showGlobalLoader()),
    hideGlobalLoader: ()=>dispatch(hideGlobalLoader()),
});

const payloadGenerator = new PayloadGenerator();
class DisplayGrid extends React.Component {
    deleteData = '';
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props);
        this.templateService = new TemplateService();
        this.state = {
            dataGridArray: this.props.renderArray,
            showConfirmation: false,
            cellData: null,
            showPopup: false,
            selectedRowData: [],
            showFlyoutSearchModal: false,
            // defaultPageCount: 50,
            // defaultPageCount: this.props.dataColumnArray.fields[0].table[0].page_size;
            // itemsPerPage: this.props.dataColumnArray.fields[0].table[0].pagination;
            // itemsPerPage:[{ value: 5, text: 5 }, { value: 10, text: 10 }, { value: 50, text: 50 }]
       }
    }
    
    findAttachedField = (field, cellData) => {
        console.log("fieldinsidefindAttachedField: ", field);
        console.log("cellDatainsidefindAttachedField: ", cellData);
        if (field === "Popup") {
            this.setState({
                showPopup: true,
                selectedRowData: cellData.rowData
            })
        } else if (field === "Flyout") {
            this.setState({
                showFlyoutSearchModal: true,
                selectedRowData: cellData.rowData
            })
        }
    }

    statusRenderer = (cellData) => {
        console.log("cellData in Status Renderer: ", cellData);
        const data = cellData.value;
        const field = cellData.field;
        if (field === 'active_ind') {
            const color = data === 'Y' ? 'green' : 'red';
            return <span><DataTable.Status color={color} />{data === 'Y' ? "Active" : "InActive"}</span>;
        } else if (field === 'Popup') {
            return <span><Button size='small' type='primary' content='View Runs' onClick={this.findAttachedField.bind(this, field, cellData)} /></span>;
        } else if (field === 'Flyout') {
            return <span><Button size='small' type='primary' content='View Runs' onClick={this.findAttachedField.bind(this, field, cellData)} /></span>;
        } else {
            return <span>{data}</span>;
        }
    }


    actionsRenderer = (cellData) => {
        const actions = cellData.field;
        return <span className="find-table-actions">
            {actions.map((action) => {
                return (
                    <Icon root={action === "Add" ? "building" : "common"} name={action.toLowerCase()} size="small" id={action.toLowerCase()}
                        onClick={this.props.crud ? (
                            action === "Add" ? console.log(cellData)
                            : action === "Edit" ? console.log(cellData)
                                : this.showConfirmationPopup.bind(this, cellData)
                        ): console.log("no crud operation allowed")} />
                )
            })}

        </span>;
    };

    callDataList() {
        this.props.showGlobalLoader();
        const operationType = "GET";
        const displayData = payloadGenerator.generatePayload(operationType, this.props.renderArray.template_name, this.props.renderArray.tableName, null, "displayData")
        
         
        this.templateService.CommonApiCall(this.props.moduleUrl,displayData).then((res) => {
            if (res.data.dataResponse.length > 0 ) {
                console.log(res);
                this.deleteData = '';
                const ModifiedArray=res.data.dataResponse[0].data;
                this.props.dispatch({
                    type: 'SET_DISPLAY_GRID_DATA',
                    payload: ModifiedArray.reverse()
                });
            } else {
                console.log('error');
            }
        })
            .finally(() => this.props.hideGlobalLoader());
    }

    getRowData(data, event) {
        if (this.deleteData === '') {
            this.props.dispatch({
                type: 'GRID_DISPLAY_DATA',
                payload: data
            });
        }
    }

    deleteAction = (data, event) => {
        this.deleteData = 'delete';
        const operationType = "DELETE";
        const deleteRowData = payloadGenerator.generatePayload(operationType, this.props.renderArray.template_name, this.props.renderArray.tableName, data, "deleteRowData")
        
        this.templateService.CommonApiCall(this.props.moduleUrl,deleteRowData).then((res) => {
            if (res) {
                console.log('deleted');
                this.callDataList();
                this.props.dispatch({
                    type: 'RESET_RENDER_DATA'
                })
            }
        });
    };

    showConfirmationPopup = (cellData) => {
        this.setState({
            showConfirmation: true,
            cellData
        })
    }

    cancelDelete = () => {
        this.setState({
            showConfirmation: false
        })
    }

    proceedDelete = () => {
        this.setState({
            showConfirmation: false
        });
        this.deleteAction(this.state.cellData);
    }
    // handlePageChange = (a,b) =>{
    //     console.log("a",a);
    //     console.log("b",b);
    // }

    render() {
        //  const Item = DataTable.ActionBar.Item;
        return (
            <div>
                <DataTable
                    data={this.props.dataList}
                    reorderableColumns={true}
                    resizableColumns={true}
                    search={true}
                    // rows={this.state.defaultPageCount}
                    onRowClick={(data1) => {
                        if(this.props.crud){
                            console.log("data1: ",data1);
                            this.getRowData(data1)
                        } else {
                            console.log("no crud operation allowed");
                        }
                        
                    }}
                >

                    {this.props.dataColumnArray.fields[0].table[0].rows.map((rowItem, rowIndex)=> {
                        return (
                            this.props.dataColumnArray.fields[0].table[0].default_columns.includes(rowItem.column_header) ? 
                            <DataTable.Column field={rowItem.column_col_name} header={rowItem.column_header} 
                                sortable={rowItem.is_sortable} renderer={this.statusRenderer}/>
                            : ""
                        )}
                    )}
                    <DataTable.Column className="find-table-header" field={this.props.dataColumnArray.fields[0].table[0].actions} header="Actions" renderer={this.actionsRenderer} 
                        align="right" />

                    
                        <DataTable.Pagination /*id="pagination" onPageChange={this.handlePageChange(1,this.state.defaultPageCount)} itemsPerPage={this.state.defaultPageCount}*/ />
                </DataTable>
                {/* <Select label="Items Per Page" 
                    placeholder={this.state.defaultPageCount} 
                    options={this.state.itemsPerPage}
                    onChange = {(value)=> {
                    let event = new Event('onPageChange');
                    document.getElementById('pagination').dispatchEvent(event);
                    this.callDataList();
                    this.setState({defaultPageCount:value});
                    }}
                /> */}
                {this.state.showConfirmation ?
                    <Confirmation showConfirmation={this.state.showConfirmation} cancelDelete={this.cancelDelete} proceedDelete={this.proceedDelete} />
                : null
                }
                {(this.state.showPopup) ?
                    <ConfigurePopup selectedRowData={this.state.selectedRowData} open={this.state.showPopup} closeEvent={()=>this.setState({showPopup: false})}  /> : ""}
                {(this.state.showFlyoutSearchModal) ?
                    <ConfigureFlyoutSearchModal selectedRowData={this.state.selectedRowData} open={this.state.showFlyoutSearchModal} closeEvent={()=>this.setState({showFlyoutSearchModal: false})}  /> : ""}
            </div>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(DisplayGrid);