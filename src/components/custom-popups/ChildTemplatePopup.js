import React, { useState , useEffect} from 'react';
import { useHistory } from "react-router-dom";
import TemplateService from '../../utils/template.service';
import { DataTable  } from '@scuf/datatable';
import { Loader , Modal , Button} from '@scuf/common';
import '@scuf/datatable/honeywell-compact/theme.css'



const templateService = new TemplateService();

const ChildTemplatePopup = (props) => {
    const [loading, setLoading] = useState(false);
    const [templateArray, setTemplateArray] = useState([]);
    const [selectedTemplateArray, setSelectedTemplateArray] = useState([]);
    useEffect(() => {
        setLoading(true);
        templateService.getAllTemplates().then((res) => {
            if(res) {
               console.log('template names ' , res.data);
               setTemplateArray(res.data)
            }
       })
       .finally(()=>setLoading(false));
      
    },[])

    const closeModal = () => {
        props.closeEvent()
    }

    const saveChildDetails = () => {
        props.saveChildDetails(selectedTemplateArray);
    }

    return(
        <>
            <Modal size="large" className="popup-modal" closeIcon={true} onClose={closeModal} open={props.open} closeOnDimmerClick={false}>
                <Modal.Header>
                    <div>List of Templates</div><hr />
                </Modal.Header>
                <Modal.Content className="popup-content">
                    <DataTable
                        data={templateArray}
                        search={true}
                        scrollable={true}
                        onRowClick={(data) => {
                            console.log(JSON.stringify(data));
                            setSelectedTemplateArray(data);
                        }}
                    >
                        <DataTable.Column className="find-table-header" field="template_name" 
                            header="Template Name" sortable={true}/>
                        <DataTable.Pagination />
                    </DataTable>                   
                </Modal.Content>
                <Modal.Footer>
                    <Button type="secondary" size="medium" content="Cancel" onClick={closeModal} />
                    <Button type="primary" size="medium" content="OK" onClick={saveChildDetails} />
                </Modal.Footer>
            </Modal>
        </>
    );
}
export default ChildTemplatePopup;