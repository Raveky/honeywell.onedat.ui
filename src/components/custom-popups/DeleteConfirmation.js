import React from 'react';
import { Modal, Button } from '@scuf/common';

const Confirmation = (props) => {

    return (
        <Modal className="confirmation-modal" size="small" open={props.showConfirmation} closeOnDimmerClick={false}>
            <Modal.Content>
                Are you sure you want to delete ?
            </Modal.Content>
            <Modal.Footer>
                <Button type="primary" size="medium" content="CANCEL" onClick={props.cancelDelete} />
                <Button type="primary" size="medium" content="PROCEED" onClick={props.proceedDelete} />
            </Modal.Footer>
        </Modal>
    )
}


export default Confirmation;
