import React, {useEffect, useState} from 'react';
import { Modal, Button, Select } from '@scuf/common';
import PayloadGenerator from './../../utils/PayloadGenerator';
import { ApiUrl } from './../../constants/ApiConstants';
import TemplateService from './../../utils/template.service';
import {connect} from 'react-redux';
import { showGlobalLoader, hideGlobalLoader , updatePopupGridData } from '../../actions/Template';


const mapDispatchToProps = (dispatch)=>({
    showGlobalLoader: () => dispatch(showGlobalLoader()),
    hideGlobalLoader: () => dispatch(hideGlobalLoader()),
    updatePopupGridData: (param) => dispatch(updatePopupGridData(param))
})


const payloadGenerator = new PayloadGenerator();
const templateService = new TemplateService();
const paginationCountOptions = [{ value: '5', text: '5' }, { value: '10', text: '10' }, { value: '20', text: '20' }, { value: '30', text: '30' }, { value: '40', text: '40' }, { value: '50', text: '50' }, { value: '100', text: '100' }];
const GridPopupConfigure = (props) => {
    const [tableNames, setTableNames] = useState([]);
    const [selectedTableName, setSelectedTableNames] = useState("");
    const [sortableItems, setSortableItems] = useState([]);
    const [defaultItems, setDefaultItems] = useState([]);
    const [defaultArr, setdefaultArr] = useState([]);
    const [paginationCount, setpaginationCount] = useState([]);
    const [defaultPageCount, setDefaultPageCount] = useState("");

    useEffect(() => {
        props.showGlobalLoader();
        let url = ApiUrl.getTableNames;
        let operationType = "GET"
        const displayData = payloadGenerator.generatePayload(operationType, '', '', null, "getTableNames")
        console.log('dis', displayData);
        templateService.CommonApiCall(url, displayData).then((res) => {
            if (res) {
                let resData = [];
                res.data.dataResponse[0].data.forEach((item, index) => {
                    if(item.title !== null) {
                        resData.push({
                            value: item.tablename,
                            text: item.title
                        })
                    }
                })
                setTableNames(resData);
            }
        })
            .finally(() => {
                props.hideGlobalLoader();
            });
    },[]);

    const getList = (data) => {
        console.log(data);
        setSelectedTableNames(data);
        props.showGlobalLoader();
        const displayData = payloadGenerator.generatePayload("GET", "", data.split(".")[1].toLowerCase(), null, null)
        console.log(displayData);
        let url = ApiUrl.getTableColNames;
        templateService.CommonApiCall(url, displayData).then((res) => {
            console.log(res);
            if (res.data.dataResponse[0].data.length > 0) {
                let tabProps = res.data.dataResponse[0].data;
                let data = [];
                let selectedData = [];
                tabProps.forEach((item, index)=>{
                    if (item.title != null) {
                        data.push({ text: item.title, value: item.columnname });
                        selectedData.push(item.columnname)
                    }
                });
                console.log("Final Data",data);
                setdefaultArr([...data]);
                setDefaultItems([...selectedData]);
                setSortableItems([...selectedData]);
                
            } else {
                console.log('error');
            }
        })
            .finally(() => {
                props.hideGlobalLoader();
            });
    }

    const savePopUpData = () => {
        let column_header = [];
        defaultItems.forEach((dItem, dIndex) => {
            const menudd = defaultItems.indexOf((dItem))
            if( menudd > -1) {
                column_header.push(dItem);
            }
        });
        props.updatePopupGridData({
            page_size: defaultPageCount,
            pagination: paginationCount,
            sort_columns: sortableItems,
            default_columns: defaultItems,
            fItem: props.fObj,
            tabIndex: props.tabColInd,
            column_header: column_header
        });
        props.cancelPopup();
    }

    return (
        <Modal size="large" open={props.showPopupGrid} closeOnDimmerClick={false}>
            <Modal.Content>
            <div className="popupgrid-select-box">
                <Select fluid={true} label="Module Name" placeholder="Select" indicator="required"
                     options={tableNames} search={true} multiple= {false} value={selectedTableName}
                    onChange={(data) => getList(data)} />
                {(selectedTableName !== "") ?
                    <>
                        <Select fluid={true} multiple={true} label="Default Columns" placeholder="Default Columns"
                            size="small" indicator="required"
                            value={defaultItems} options={defaultArr}
                            onChange={(data) => {
                                console.log(data);
                                setDefaultItems(data);
                                setSortableItems(data);
                            }} />
                        <Select fluid={true} multiple={true} label="Sortable Columns" placeholder="Sortable Columns"
                            size="small" indicator="required"
                            value={sortableItems} options={defaultArr}
                            onChange={(data) => {
                                console.log(data);
                                setSortableItems(data)
                            }} />
                        <Select fluid={true} multiple={true} label="Items Per Page" placeholder="Items Per Page" size="small" indicator="required"
                            value={paginationCount} options={paginationCountOptions}
                            onChange={(data) => {
                                setpaginationCount(data)
                            }} />
                        <Select fluid={true} label="Default Page Count" placeholder="Default Page Count" size="small" indicator="required"
                            value={defaultPageCount} options={paginationCountOptions}
                            onChange={(data) => {
                                setDefaultPageCount(data)
                            }} />
                    </>
                :""}
            </div>
            </Modal.Content>
            <Modal.Footer>
                <Button type="primary" size="medium" content="CANCEL" onClick={props.cancelPopup} />
                <Button type="primary" size="medium" content="OK" onClick={savePopUpData} />
            </Modal.Footer>
        </Modal>
    )
}


export default connect("", mapDispatchToProps)(GridPopupConfigure);
