import React from 'react';

const OrientationPopup = (props) => {
    const options = ['Portrait', 'Landscape'];
    const itemClick = (item) => {
        console.log(item);
        props.updatePortraitMode(item);
    }
    return (
        <div className="orientation-pop-cover">
            {options.map((i, v) => {
                return (<div className="orientation-pop-list-item" key={v} onClick={(e) => {
                    e.stopPropagation();
                    itemClick(i);
                }}>{i}</div>)
            })}
        </div>
    )
}

export default OrientationPopup;