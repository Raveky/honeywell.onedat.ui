
import React from 'react';
import { Table } from '@scuf/common';

const TableRowGroup = (props) => {

  return (<Table className="table-row-group">
                <Table.Header>
                    {props.headers.map(item=>{
                        return  <Table.HeaderCell content={item}/>
                    })}
                </Table.Header>
                <Table.Body>
                {props.data.map(each=>{
                    return (
                        <Table.Row>
                            <Table.Cell>
                            {each.name}
                            </Table.Cell>
                            <Table.Row>
                                <Table.Row>
                                    {each.variables.map(item=>{
                                        return (<Table.Row>
                                                    <Table.Cell>
                                                        {item.value}
                                                    </Table.Cell>
                                                </Table.Row>
                                                )
                                    })}
                                </Table.Row>    
                            </Table.Row>   
                        </Table.Row> )    
                })}    
                </Table.Body>
        </Table>    
  );
};

export default TableRowGroup;