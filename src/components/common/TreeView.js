import React from 'react';
import { Tree } from '@scuf/common';
import PropTypes from 'prop-types';

const TreeView = (props) => {

  const Item = Tree.Content.Item;
  return (
    <Tree>
        <Tree.Content>
                <Item title="Jedi">
                    <Item title="Obi-Wan Kenobi" icon="user" iconRoot="common" />
                    <Item title="Luke Skywalker" icon="user" iconRoot="common" />
                </Item>
                <Item title="Robots">
                    <Item title="C-3PO" icon="settings" iconRoot="common"/>
                    <Item title="R2-D2" icon="settings" iconRoot="common"/>
                </Item>
        </Tree.Content>
    </Tree>
  );
};

export default TreeView;
