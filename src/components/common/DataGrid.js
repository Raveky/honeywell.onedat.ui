
import React from 'react';
import { DataTable } from '@scuf/datatable';
import { Icon } from '@scuf/common';

const DataGrid = (props) => {

  const statusRenderer=(cellData, data)=>{
    if(cellData.icon){
      return <Icon name={cellData.icon} size="small" color="blue"  root="common" />;
    }
    return data.value
  }

  return (
        <DataTable
            data={props.data}
            reorderableColumns={true}
            resizableColumns={true}
            //rows={10}
            scrollable={true}
        >
            <DataTable.Pagination
                totalItems={props.data.length}
                itemsPerPage={10}
            />
            {props.headers.map((item) => {
                return <DataTable.Column  initialWidth={150} field={item.field} header={item.header} sortable={item.sortable} renderer={(data)=>statusRenderer(item,data)}/>
            })}
        </DataTable>
  );
};

export default DataGrid;