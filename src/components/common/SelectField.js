import React, { useState } from 'react';
import { Select } from '@scuf/common';
import PropTypes from 'prop-types';

const SelectField = (props) => {
  
  const handleChange = (option) =>{
    props.onChange(option)
  }

  return (
    <Select className="common-select" placeholder={props.placeholder} label={props.label} indicator={props.indicator} options={props.options} onChange={(val)=>handleChange(val)}/>
  );
};

SelectField.propTypes = {
    label: PropTypes.string,
    indicator: PropTypes.string,
    placeholder: PropTypes.string,
    options: PropTypes.array,
    onChange: PropTypes.func
};

export default SelectField;