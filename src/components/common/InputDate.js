import React from 'react';
import { DatePicker } from '@scuf/common';
import PropTypes from 'prop-types';

const InputDate = (props) => {
    return (
      <DatePicker
        key={props.key}
        label={props.label}
        type={props.type}
        displayFormat={props.displayFormat}
        placeholder={props.displayFormat}
        minuteStep={props.minuteStep}
        indicator={props.indicator}
    />
    );

}

InputDate.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  indicator: PropTypes.string,
  displayFormat:PropTypes.string,
  minuteStep:PropTypes.number
};

export default InputDate;