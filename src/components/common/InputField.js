import React from 'react';
import { Input } from '@scuf/common';
import PropTypes from 'prop-types';

const InputField = (props) => {

  const handleChange = (val) =>{
    console.log(val)
  }

  return (
        <Input label={props.label} indicator={props.indicator} placeholder={props.placeholder} onChange={(val)=>handleChange(val)}/>
  );
};

InputField.propTypes = {
  label: PropTypes.string,
  indicator: PropTypes.string,
  placeholder: PropTypes.string
};

export default InputField;
