import React from 'react';
import { Input } from '@scuf/common';

const Input = (props) => {
  //console.log(props);
  return (
        <Input label="Name" indicator="required" placeholder="Name" />
  );
};

export default Input;