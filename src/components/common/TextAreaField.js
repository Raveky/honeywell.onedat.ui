import React from 'react';
import { TextArea } from '@scuf/common';
import PropTypes from 'prop-types';

const TextAreaField = (props) => {
  return (
    <TextArea placeholder={props.placeholder} label={props.label} />
  );
};

TextArea.propTypes = {
    label: PropTypes.string,
};

export default TextAreaField;