import React from 'react';
import { Button, Modal } from '@scuf/common';

const DataModal = (props) => {
        return (
            <Modal className="data-modal" closeIcon={true} onClose={() => props.onClose()} open={props.open} closeOnDimmerClick={false}>
                <Modal.Header>
                  {props.title}
                </Modal.Header>
                <Modal.Content>
                 {props.children}
                </Modal.Content>
                <Modal.Footer className="data-modal-footer">
                    <Button type="primary" size="medium" content="Export" onClick={() => props.onClose()}/>
                    <Button type="secondary" size="medium" content="Close" onClick={() => props.onClose()}/>
                  
                </Modal.Footer>
            </Modal>
        );
}

export default DataModal;