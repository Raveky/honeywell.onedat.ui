import React, {useState} from 'react';
import { Card, Button, Input  } from '@scuf/common';

const LogInPage = (props) =>{
    const [userName, setUserName] = useState("");
    const [tempateErr, setTempateErr] = useState("");
    const login = ()=>{
        if(!userName) {
            setTempateErr("Please Enter the User Name!");
        } else {
            setTempateErr("");
            localStorage.setItem("username",userName);
            localStorage.setItem("showPopup","true");
            //props.history.push('/home');
            props.updateState();
        }
    }
    return(
        <div className="login-page-cover">
            <div>
                <Input fluid={true} value={userName} type="text" placeholder="User Name" onChange={(data) => {setUserName(data) }} label="User Name" indicator="required" error={tempateErr} />
            </div>            
            <Button type="primary" size="small" content="Login" onClick={login} />
        </div>
    )
}

export default LogInPage;