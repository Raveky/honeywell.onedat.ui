import React, {useState} from 'react';
import { Modal, Button, Select } from '@scuf/common';
import { connect } from 'react-redux';
import {addTechnology} from './../actions/Template';

const mapDispatchToProps = (dispatch)=>({
    dispatch:dispatch,
    addTechnology: (value)=>dispatch(addTechnology(value)) 
 });
 
 const TechnologySelectionModel = (props) => {
    const [technolgyList, setTechnologyList] = useState(null);
   const {techErr} =props
    return (
        <Modal /* id="honeywell-custom-flyout" */ open={props.isTechnologyModelShow} closeOnDimmerClick={false}>

            <Modal.Content>
                <div className="technology-select-box">
                    <Select fluid={true} label="Technologies" multiple={false} 
                    placeholder="Select Technologies" size="small" value={technolgyList} 
                    indicator="required" error={techErr} options={props.technologyDropdownArr} onChange={
                        (value) => {
                            setTechnologyList(value)
                        }
                    } />
                </div>
            </Modal.Content>
            <Modal.Footer>
                <Button type="primary" size="medium" content="Submit" onClick={() => props.closeModel(technolgyList)}  />
            </Modal.Footer>
        </Modal>)
}

//const avaialbleControlOptions = [{ value: 'HYDAT', text: 'HYDAT' }, { value: 'NAPTHA', text: 'NAPTHA' }, { value: 'MINIDAT', text: 'MINIDAT' }, { value: 'FCC', text: 'FCC' }, { value: 'ADSDAT', text: 'ADSDAT' }, { value: 'FEEDSTOCK', text: 'FEEDSTOCK' }, { value: 'HYCE', text: 'HYCE' }];




export default connect(null, mapDispatchToProps)(TechnologySelectionModel);