import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Accordion, Loader } from '@scuf/common';
import TemplateService from '../utils/template.service';
import { useHistory } from "react-router-dom";
import PayloadGenerator from '../utils/PayloadGenerator';
import ActionDetails from './../constants/enum'
import { ApiUrl } from '../constants';
import { showGlobalLoader, hideGlobalLoader  } from './../actions/Template';
import ModuleSectionControls from './ModuleSectionControls';

function mapStateToProps(state) {
    console.log("state =>", state);
    const templateState = state.templateReducer;
    return {
        templateModule: templateState.template,
        templateSections: templateState.template.sections,
        userName  : templateState.template.userName
    }
   
}
// to call the global loader
const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    showGlobalLoader: ()=>dispatch(showGlobalLoader()),
    hideGlobalLoader: ()=>dispatch(hideGlobalLoader())
});

const templateService = new TemplateService();
const payloadGenerator = new PayloadGenerator();

const ModulePage = (props) => {
    const [allowNavigation , setNavigation] = useState(false);
    const [temData, setTempData] = useState(null);
    const [moduleURL, setModuleUrl] = useState(null);
    const id = props.id;
    const moduleName = props.name;
    // console.log('this.props.match.params' , props.match.params);
    const crud = props.crud && props.crud==="no_crud" ? false : true;
    let history = useHistory();
    let moduleUrl = '';
    let today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    console.log(date)
    console.log(props.userName)
    useEffect(() => {
        props.showGlobalLoader();
        let displayData = {};
        displayData.OperationType = "GET";
        displayData.Messages = [];
        let mesgObj = {
            template_name: "",
            table_name: "sc_template.template_master",
            fields: [],
            filter: []
        };
        mesgObj.filter.push({
            "field_name": "template_id_sq",
            "field_type": "int",
            "field_value": id.toString()
        })
        displayData.Messages.push(mesgObj);
        let url=ApiUrl.templateAPi
        templateService.CommonApiCall(url,displayData).then((res) => {
            if (res) {
                if (res.data.dataResponse.length > 0) {
                    let tempData = JSON.parse(res.data.dataResponse[0].data[0].template_data)//.replace(/\\n/g, '').replace(/\\r/g, ''))
                    console.log(res);
                    setTempData(tempData);
                    if(crud) {
                        callDisplayGridData(tempData);
                    } else{
                        props.dispatch({
                            type: 'SET_MODULE_DATA',
                            payload: tempData
                        });
                        setNavigation(true);
                    }
                    console.log(temData);
                } else {
                    history.push('/');
                }
            }
        })
            .finally(() => props.hideGlobalLoader());
        console.log('rendering');
    }, [id]);

    const callDisplayGridData = (tempData) => {
        props.dispatch({
            type: 'SET_MODULE_DATA',
            payload: tempData
        });
        const operationType = "GET";
        const displayData = payloadGenerator.generatePayload(operationType, tempData.template_name, tempData.tableName, null, "displayData")
        ActionDetails.forEach((item, index)=>{
            if(item.name === moduleName) {
                 moduleUrl = item.url;
                 setModuleUrl(moduleUrl);
            }
         })
        templateService.CommonApiCall(moduleUrl, displayData).then((res) => {
            if (res.data.dataResponse.length>0) {
                const ModifiedArray=res.data.dataResponse[0].data;
                props.dispatch({
                    type: 'SET_DISPLAY_GRID_DATA',
                    payload: ModifiedArray.reverse()
                });
                setNavigation(true);
            }
        })
    }


    return (

        <div className="container-fluid">
                {allowNavigation ?
                    <ModuleSectionControls temData={temData} crud={crud} moduleUrl={moduleURL}></ModuleSectionControls>
                    : <div>Please wait....</div>}
            
        </div >
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(ModulePage);

