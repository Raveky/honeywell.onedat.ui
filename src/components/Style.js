const BuildTemplateStyle = {
    titleCover: {
        display: 'flex',
        fontWeight: 'bold',
        gap: 12,
    },
    footerButtonStyle: {
        justifyContent: 'flex-start',
        margin: '10px 0px'
    },
    templateArea: {
        padding: '0 10px',
        borderRight: '1px solid #80808066'
    },
    versionArea: {
        padding: '0 10px',
        borderRight: '1px solid #80808066'
    },
    descriptionArea: {
        padding: '0 10px',
    },
    smallTitle: {
        fontSize: '11px',
        fontWeight: 'normal'
    },
    cardCover: {
        padding: '10px 10px'
    },
    selectBoxStyle: {
        width: '40%',
        float: 'left'
    },
    rightToButton: {
        float: 'right'
    },
    mainSectionbar: {
        //background: '#d1dee894',
        //padding: '10px',
        display: 'flex',
        justifyContent: 'space-around',
        //marginBottom: '10px'
    },
    mainSectionheader: {
        color: '#5f7d95',
        fontWeight: 'bold',
        flex: 1,
    },
    mainSectionControl: {
        borderRight: '1px solid #80808066',
        padding: '3px 10px',
        color: 'black',
        fontWeight: 'bold',
        cursor: 'pointer'
    },
    makeBorderZero: {
        border: 0
    },
    cardContent: {
        display: 'flex',
        justifyContent: 'flex-start',
    },
    selectStyle: {
        //  width: '24%',
        margin: '0 0.5%',
    },

    checkBoxStyle: {
        top: '20px'
    },
    checkBoxTitle: {
        fontWeight: 'bold'
    }
}

export default BuildTemplateStyle;