import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Modal, Button, Icon, Accordion, FormattedList, Checkbox } from '@scuf/common';
import DisplayGrid from './DisplayGrid';
import { updateHoldstate, updateKeyState } from '../actions/Template';

const mapStateToProps = (state) => {
    const templateState = state.templateReducer;
    return {
        templateArray: templateState.template
    }
}

const mapDispatchToProps = (dispatch) => ({
    updateHoldstate: (fObject, value) => dispatch(updateHoldstate(fObject, value)),
    updateKeyState: (fObject, value) => dispatch(updateKeyState(fObject, value))

})

const ConfigurationModal = (props) => {
    console.log("templateArray Inside Configuration Modal: ", props.templateArray);

    return (
        <Modal open={props.showConfigurationModal} closeOnDimmerClick={false}>
            <Modal.Header>
                <div className="configuration-back-button" onClick={props.closeConfigurationModal}>
                    <Icon root="global" name="caret-left" exactSize={26} color="#1274B7" />
                    Configuration
                </div>
            </Modal.Header>
            <Modal.Content className="configuration-modal-content">
                {props.templateArray.fields.map((fieldItem, y) => {
                    console.log("props.templateArray.fields: ", props.templateArray.fields);
                    return (
                        (fieldItem.field_type !== 'Triple Dropdowns' && fieldItem.field_type !== 'Label' && fieldItem.field_type !== 'Action Buttons' && fieldItem.field_type !== 'Button') ?
                            <FormattedList className="row">
                                <FormattedList.Item
                                    key={y}
                                    className="col-12"
                                    title={fieldItem.field_label}
                                    actionIconName={
                                        <div>
                                            <Checkbox toggle={true} checked={fieldItem.holds_state} label="Hold State" onChange={(data) => { props.updateHoldstate(fieldItem, data) }} />
                                            <Checkbox toggle={true} label="Key State" onChange={(data) => { props.updateKeyState(fieldItem, data) }} />
                                        </div>
                                    }
                                />
                            </FormattedList>
                            : null
                    )
                })}
                {props.templateArray.sections.map((i, v) =>
                    <div key={v}>
                        {/* <Accordion className= {(i.section_name !== 'Template Grid')?"preview-accoridian-pop": ''}>

                            <Accordion.Content title={(i.section_name !== 'Template Grid')? i.section_name : ''} 
                                arrowPosition={(i.section_name !== 'Template Grid')? "right" : ''} > */}
                        <div>
                            {(i.section_name !== 'Template Grid') ?
                                i.fields.map((fieldItem, y) => {
                                    return (
                                        (fieldItem.field_type !== 'Triple Dropdowns' && fieldItem.field_type !== 'Label' && fieldItem.field_type !== 'Action Buttons' && fieldItem.field_type !== 'Button') ?
                                            <FormattedList className="row">
                                                <FormattedList.Item
                                                    key={y}
                                                    className="col-12"
                                                    title={fieldItem.field_label}
                                                    actionIconName={
                                                        <div>
                                                            <Checkbox toggle={true} checked={fieldItem.holds_state} label="Hold State" onChange={(data) => { props.updateHoldstate(fieldItem, data) }} />
                                                            <Checkbox toggle={true} label="Key State" onChange={(data) => { props.updateKeyState(fieldItem, data) }} />
                                                        </div>
                                                    }
                                                />
                                            </FormattedList>
                                            : null
                                    )
                                })
                                : ''
                            }
                        </div>
                        {/* </Accordion.Content>
                        </Accordion> */}
                    </div>
                )}
            </Modal.Content>
            <Modal.Footer className="configuration-footer">
                <Button type="primary" size="medium" content="OK" onClick={props.submitData} />
            </Modal.Footer>
        </Modal>
    )
}


export default connect(mapStateToProps, mapDispatchToProps)(ConfigurationModal);
