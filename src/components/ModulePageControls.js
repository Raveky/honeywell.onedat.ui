import React, { useState } from 'react';
import { Input, TextArea, DatePicker, Checkbox, Select, Button, Icon, Radio } from '@scuf/common';
import buildTemplateStyle from './Style';
import { updateFieldValue, configureFlyoutSearchModal, fetchFamilyName, configurePopup, showGlobalLoader, hideGlobalLoader } from '../actions/Template';
import { connect } from 'react-redux';
import ConfigureFlyoutSearchModal from './Flyouts/ConfigureFlyoutSearchModal';
import ConfigurePopup from './Flyouts/ConfigurePopup';
import ActionDetails from './../constants/enum';
import TemplateService from './../utils/template.service';
import { statusList, SulfdingReductionIndicator  } from '../constants/DropDownData';
import PayloadGenerator from '../utils/PayloadGenerator';

const templateService = new TemplateService();
const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    updateFieldValue: (fObject, value) => {
        dispatch(updateFieldValue(fObject, value))
    },
    configureFlyoutSearchModal: (fObject, value) => {
        dispatch(configureFlyoutSearchModal(fObject, value))
    },
    fetchFamilyName: (value) => {
        dispatch(fetchFamilyName(value))
    },
    configurePopup: (fObject, value) => {
        dispatch(configurePopup(fObject, value))
    },
    showGlobalLoader: () => dispatch(showGlobalLoader()),
    hideGlobalLoader: () => dispatch(hideGlobalLoader())
})

const mapStateToProps = (state) => {
    const templateState = state.templateReducer;
    return {
        fieldNameArr: templateState.fieldNameArr,
        familyName: templateState.template.familyName,
        templateModule: templateState.template
    }
}
const payloadGenerator = new PayloadGenerator();

const ModulePageControls = (props) => {
    const [minLengthError, setMinLengthError] = useState(false);
    const avaialbleControlOptions = [{ value: 'Option1', text: 'Option1' }, { value: 'Option2', text: 'Option2' }]
    const [value, setValue] = useState("");
    const [tValue, setTValue] = useState("")
    const openFetchFlyoutSearchModal = (val) => {
        props.configureFlyoutSearchModal(val, true);
        props.fetchFamilyName();
    }
    const openPopup = (val) => {
        props.configurePopup(val, true);
    }

    const updateFieldValue = (fObject, value, minlength) =>{
        console.log(this, fObject, minlength, value)
        props.updateFieldValue(fObject, value)
        controlChange(fObject.actions);
        if(fObject.field_value && fObject.field_value.length >= minlength){
            setMinLengthError(false);
        }else{
            setMinLengthError(true);
        }
    }

    const buttonClick = (param, label) => {
        makeServiceCall(param, 'click' , label);
    }

    const controlChange = (param) => {
        makeServiceCall(param, 'change');
    }

    const controlBlur = (param) => {
        makeServiceCall(param, 'blur');
    }



    const makeServiceCall = (param, eventType, label) => {
        console.log(param)
        let obj = param.find((v, i) => {
            return v.event === eventType;
        });
        if (obj) {
            console.log("button click", obj, obj.event);
            let getUrl = ActionDetails.find((v, i) => {
                return obj.url === v.name
            })
            console.log(getUrl);
            buttonActions(label,getUrl.url);
            // serviceGETMethod(getUrl);
        } else {
            buttonActions(label);
        }
    }

    const serviceGETMethod = function (getUrl) {
        if (getUrl) {
            props.showGlobalLoader();
            templateService.getMethod(getUrl.url).then((res) => {
                console.log(res);
            }).finally(() => props.hideGlobalLoader())
        }
    }

    const labelManipulation = (fieldItem, y) => {
        let field_label = fieldItem.field_label;
        let index = field_label.indexOf('*');
        if (index > -1) {
            let splittedArr = field_label.split('*');
            return (
                <p className="col-sm-12" key={y}>
                    {
                        splittedArr.map((v, i) => <>
                            <span>{v} </span>
                            {
                                (i !== splittedArr.length - 1) ? <span className="red-asterisk-mandatory">* </span> : ""
                            }
                        </>)
                    }

                </p>
            )
        } else {
            return (
                <p className="col-sm-12" key={y}>
                    {fieldItem.field_label}
                </p>
            )
        }

    }

    const saveModule = (method, url) => {
        const operationType = method === 'edit' ? "PUT" : "POST";
        const moduleData = payloadGenerator.generatePayload(operationType, props.templateModule.template_name, props.templateModule.tableName, props.templateModule, "moduleData");
        console.log("moduledata from payload generator: ",moduleData);
        props.showGlobalLoader();
        templateService.CommonApiCall(url, moduleData).then((res) => {
            if (res) {
                // alert('Saved successfully');
                callDisplayGridData(props.templateModule.template_name, props.templateModule.tableName, url);
                if(method === 'edit') {
                    props.dispatch({
                        type: 'RESET_BUTTON_NAME'
                    })
                }
                resetData();
            } else {

            }
        }).finally(() => props.hideGlobalLoader());
    }

    const resetData = () => {
        props.dispatch({
            type: 'RESET_RENDER_DATA'
        })
    }

    const callDisplayGridData = (templateName, tableName, url) => {
        const operationType = "GET";
        const displayData = payloadGenerator.generatePayload(operationType, templateName, tableName, null, "displayData")
        templateService.CommonApiCall(url, displayData).then((res) => {
            if (res) {
                props.dispatch({
                    type: 'SET_DISPLAY_GRID_DATA',
                    payload: res.data.dataResponse[0].data
                });
            }
        })
    }

    const findData = () => {
        const operationType = "GET";
        const findData = payloadGenerator.generatePayload(operationType, props.templateModule.template_name, props.templateModule.tableName, props.templateModule, "findData")
        
        templateService.CommonApiCall(props.moduleUrl,findData).then((res) => {
            if (res.data.dataResponse.length > 0) {
                const ModifiedArray=res.data.dataResponse[0].data;
                props.dispatch({
                    type: 'SET_DISPLAY_GRID_DATA',
                    payload: ModifiedArray.reverse()
                });
            }
        })
    }

    const buttonActions = (type, url) => {
        if(props.crud){
            switch (type) {
                case 'SAVE':
                    saveModule('save', url);
                    break;
                case 'RESET':
                    resetData();
                    break;
                case 'FIND':
                    findData();
                    break;
                case 'UPDATE':
                    saveModule('edit', url);
                    break;
                default:
            }
        }else{
            console.log("no crud operation allowed");
        }
    }

    return (
        <>
            {props.secItem.fields.map((fieldItem, y) => {
                return <React.Fragment key={y}>
                {fieldItem.field_type === 'TextBox' ?
                    <div className="col-sm-4 d-inline-block" key={props.keyVal}>
                        <Input fluid={true} type="text" placeholder="Text" value={fieldItem.field_value}
                            indicator={(fieldItem.mandatory) ? "required" : ""} label={fieldItem.field_label}
                            onChange={(value)=>updateFieldValue( fieldItem, value, fieldItem.field_min_length)}
                            onBlur = {()=>{
                                controlBlur(fieldItem.actions); 
                            }}
                            minlength = {fieldItem.field_min_length}
                            maxlength = {fieldItem.field_max_length}
                            error = {minLengthError ? `Minimum length of ${fieldItem.field_min_length} chars is required` : ""}
                        />
                    </div>
                    : null}

                {fieldItem.field_type === 'Lookup' || fieldItem.field_type === 'UOM' ?
                    <div className="col-sm-4 d-inline-block" key={props.keyVal}>
                        <Input fluid={true} type="text" placeholder={fieldItem.source_group}
                            indicator={(fieldItem.mandatory) ? "required" : ""} label={fieldItem.field_label}
                            iconPosition="right" icon={<Icon name="search" root="common" size="small" />} />
                    </div>
                    : null}

                {fieldItem.field_type === 'Comments' ?
                    <div className="col-sm-12" key={props.keyVal}>
                        <TextArea fluid={true} label={fieldItem.field_label}
                            indicator={(fieldItem.mandatory) ? "required" : ""} placeholder='Input multiline data here...'
                            onChange={() => {
                                controlChange(fieldItem.actions);
                            }} />
                    </div>
                    : null}

                {fieldItem.field_type === 'DateTimePicker' || fieldItem.field_type === 'DatePicker' ?
                    <div className={(fieldItem.field_type === 'DateTimePicker') ? "col-sm-4 d-inline-block" : "col-sm-4 d-inline-block"} key={props.keyVal}>
                        <DatePicker fluid={true} label={fieldItem.field_label} placeholder="MM/DD/YYYY"
                            type={(fieldItem.field_type === 'DateTimePicker') ? "datetime" : "date"} indicator={(fieldItem.mandatory) ? "required" : ""}
                            onChange={() => {
                                controlChange(fieldItem.actions)
                            }}
                        ></DatePicker>
                    </div> : null}

                {
                    fieldItem.field_type === 'Toggle' ?
                        <div className="col-sm-4 d-inline-block" key={props.keyVal}>
                            <Checkbox fluid={true} style={buildTemplateStyle.checkBoxStyle} toggle={true}
                                label={fieldItem.field_label} onChange={() => setTValue(fieldItem.field_label)}
                            />
                        </div>
                        : null
                }

                {
                    fieldItem.field_type === 'Drop Down' ?
                        <div className="col-sm-4 " key={props.keyVal}>
                            <Select fluid={true} label={fieldItem.field_label} placeholder="Select" size="small" value={fieldItem.field_value}
                                indicator={(fieldItem.mandatory) ? "required" : ""}
                                onChange={(value) => { fieldItem.field_value = value }}
                                options={(fieldItem.source_group === 'Status' ? statusList : avaialbleControlOptions)}
                                onChange={(value)=>updateFieldValue( fieldItem, value)}
                                onBlur={() => {
                                    controlBlur(fieldItem.actions)
                                }} />
                        </div>
                        : ""
                }
                {
                    fieldItem.field_type === 'Triple Dropdowns' ?

                        <div className="col-sm-4" key={props.keyVal}>
                            <div className="col-sm-4 module-triple-select">
                                <Select fluid={true} label={fieldItem.fieldNameArr[0]} placeholder="Select" size="small"
                                    indicator={(fieldItem.mandatory) ? "required" : ""} options={(fieldItem.source_group === 'Status' ? statusList : avaialbleControlOptions)}
                                    onChange={() => {
                                        controlChange(fieldItem.actions)
                                    }} />
                            </div>
                            <div className="col-sm-4 module-triple-select">
                                <Select fluid={true} label={fieldItem.fieldNameArr[1]} placeholder="Select" size="small"
                                    indicator={(fieldItem.mandatory) ? "required" : ""} options={(fieldItem.source_group === 'Status' ? statusList : avaialbleControlOptions)}
                                    onChange={() => {
                                        controlChange(fieldItem.actions)
                                    }} />
                            </div>
                            <div className="col-sm-4 module-triple-select">
                                <Select fluid={true} label={fieldItem.fieldNameArr[2]} placeholder="Select" size="small"
                                    indicator={(fieldItem.mandatory) ? "required" : ""} options={(fieldItem.source_group === 'Status' ? statusList : avaialbleControlOptions)}
                                    onChange={() => {
                                        controlChange(fieldItem.actions)
                                    }} />
                            </div>
                        </div>
                        : ""
                }

                {
                    fieldItem.field_type === 'Button' ?
                        <div className="col-sm-12" key={props.keyVal}>
                            <Button type="primary" content={fieldItem.field_label} onClick={() => {
                                buttonActions(fieldItem.field_label)
                                buttonClick(fieldItem.actions)

                            }} />
                        </div>
                        : null
                }
                {
                    (fieldItem.field_type === 'Label') ?
                        labelManipulation(fieldItem, props.keyVal)
                        : null
                }
                {
                    fieldItem.field_type === 'Action Buttons' ?
                        <div className="mr-1">
                            <Button disabled={minLengthError} type={(fieldItem.button_property) ? 'primary' : 'secondary'} content={fieldItem.field_label}
                                onClick={() => {
                                    buttonClick(fieldItem.actions,fieldItem.field_label)
                                }} key={props.keyVal}></Button>
                        </div>
                        : ""
                }
                {
                    (fieldItem.field_type === 'Radio Button') ?
                        <>

                            <div>
                                <span>{fieldItem.field_label}</span>

                                {
                                    fieldItem.fieldNameArr.map((fieldvalue, index) => (
                                        <Radio
                                            name="RadioGroup"
                                            checked={value === fieldvalue}
                                            label={fieldvalue}
                                            onChange={() => {
                                                setValue(fieldvalue)
                                                console.log(value)
                                            }}

                                        />))} </div></> : ' '
                }


                {
                    fieldItem.field_type === 'MultipleDropDown' ?
                        <div className="mr-1">
                            <Select label={fieldItem.field_label} placeholder="Select Multiple States" options={avaialbleControlOptions} multiple={true} />
                        </div>
                        : ""
                }
                {fieldItem.field_type === 'Flyout' ?
                    <>
                        <div className="col-sm-4 d-inline-block" key={props.keyVal}>
                            <Input fluid={true} type="text" placeholder={fieldItem.field_label}
                                value={props.familyName}
                                indicator={(fieldItem.mandatory) ? "required" : ""}
                                label={fieldItem.field_label}
                                onClick={() => openFetchFlyoutSearchModal(fieldItem)}
                                iconPosition="right"
                                icon={<Icon name="search" root="common" size="small" />}
                            />
                        </div>
                        {(fieldItem.showFlyoutSearchModal) ?
                            <ConfigureFlyoutSearchModal fieldItem={fieldItem} fieldType={fieldItem.field_type} open={fieldItem.showFlyoutSearchModal} closeEvent={() => props.configureFlyoutSearchModal(fieldItem, false)} /> : ""}
                    </>
                    : null
                }
                {fieldItem.field_type === 'Popup' ?
                    <>
                        <div className="col-sm-4 d-inline-block" key={props.keyVal}>
                            <Input fluid={true} type="text" placeholder={fieldItem.field_label}
                                indicator={(fieldItem.mandatory) ? "required" : ""}
                                label={fieldItem.field_label}
                                iconPosition="right"
                                icon={<Icon name="communication" root="common" size="small" onClick={() => openPopup(fieldItem)} />}
                            />
                        </div>
                        {(fieldItem.showPopup) ?
                            <ConfigurePopup fieldItem={fieldItem} fieldType={fieldItem.field_type} open={fieldItem.showPopup} closeEvent={() => props.configurePopup(fieldItem, false)} /> : ""}
                    </>
                    : null
                }

                {

                    fieldItem.field_type === 'link' ?
                        <p className="col-sm-12" key={props.keyVal}>
                            <a href={fieldItem.actions[0].resource} target="_blank">{fieldItem.field_label}</a></p>
                        : null
                }
                </React.Fragment>
            })
        }
    </>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(ModulePageControls);