import React, { useState } from 'react';
import { Modal, Button, Input } from '@scuf/common';
import { DataTable } from '@scuf/datatable';
import { connect } from 'react-redux';
import { updateFamilyName } from '../../actions/Template';

const mapDispatchToProps = (dispatch) => ({
    updateFamilyName: (value) => {
        dispatch(updateFamilyName(value))
    }
})
function mapStateToProps(state) {
    const templateState = state.templateReducer;
    return {
        flyoutSearchData: templateState.flyoutSearchData,
        templateState: templateState
    }
}
const ConfigureFlyoutSearchModal = (props) => {
    const flyoutSearchData = props.flyoutSearchData;
    const [searchValue, setSearchValue] = useState("");
    const [eventErr, setEventErr] = useState("");
    const [data, setData] = useState(flyoutSearchData)


    const closeModal = () => {
        props.closeEvent()
    }

    const clearSearch = () => {
        setSearchValue("");
        setData(flyoutSearchData);
    }

    const search = () => {
        console.log(flyoutSearchData)
        const filteredData = flyoutSearchData.filter(function (obj) {
            return obj.catalyst_scale_nm.includes(searchValue);
        })
        setData(filteredData);
    }

    const fieldItemTableDetails = props.fieldItem.table[0];
    console.log(fieldItemTableDetails)

    return (
        <>
            <Modal id="flyout-search-modal" closeIcon={true} onClose={closeModal} open={props.open} closeOnDimmerClick={false}>
                <Modal.Header>
                    <div>{props.selectedRowData ?
                        props.selectedRowData.catalyst_scale_nm + '-' + props.selectedRowData.catalyst_scale_desc + '-Associated Run(s)'
                        : props.fieldItem.field_label}</div><hr />
                </Modal.Header>
                <Modal.Content>
                    {props.selectedRowData ? null :
                        <>
                            <Input className="col-sm-6" fluid={true} value={searchValue} type="text" placeholder={props.fieldItem.field_label}
                                onChange={data => { setSearchValue(data) }} error={eventErr} />
                            <Button className="col-auto" type="secondary" size="small" content="Clear Search" onClick={clearSearch} />
                            <Button className="col-auto" type="primary" size="small" content="Search" onClick={search} />
                        </>
                    }
                </Modal.Content>
                <Modal.Footer>
                    <DataTable
                        data={data}
                        rows={parseInt(fieldItemTableDetails.page_size)}
                        scrollable={true}
                        onRowClick={(e) => {
                            console.log(e);
                            if (!props.selectedRowData) {
                                props.updateFamilyName(e.data.catalyst_scale_nm);
                                closeModal();
                            }
                        }}
                    >
                         
                        
                        {
                            
                            fieldItemTableDetails.default_columns.map((item, val)=>{
                                return(                                  
                                        
                                    <DataTable.Column key={val} field={item} header={item} sortable={
                                        fieldItemTableDetails.sort_columns.includes(item)
                                    } align="center" />
                                    
                                )
                            })
                        }
                        
                        <DataTable.Pagination
                            totalItems={data.length}
                            //itemsPerPage={props.fieldItem.table[0].page_size}
                            showDisplayDetails
                            itemsPerPageOptions={[3, 5, 10]}
                        />
                    </DataTable>
                </Modal.Footer>

            </Modal>
        </>
    )
}


export default connect(mapStateToProps, mapDispatchToProps)(ConfigureFlyoutSearchModal);