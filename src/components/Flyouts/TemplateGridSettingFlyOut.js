import React, { useState, useEffect } from 'react';
import { Modal, Button, Input, Select } from '@scuf/common';
import PayloadGenerator from './../../utils/PayloadGenerator';
import { ApiUrl } from './../../constants/ApiConstants';
import TemplateService from './../../utils/template.service';
import { connect } from 'react-redux';
import { showGlobalLoader, hideGlobalLoader } from '../../actions/Template';

//sortable, pagesize, pagination counts
const payloadGenerator = new PayloadGenerator();
const templateService = new TemplateService();
const mapDispatchToProps = (dispatch) => ({
    showGlobalLoader: () => dispatch(showGlobalLoader()),
    hideGlobalLoader: () => dispatch(hideGlobalLoader()),
})
const TemplateGridSettingFlyOut = (props) => {
    const [tableNames, setTableNames] = useState([]);
    const [selectedTableName, setSelectedTableNames] = useState("");
    const [defaultArr, setdefaultArr] = useState([]);
    const [paginationCount, setpaginationCount] = useState([]);
    const [defaultPageCount, setDefaultPageCount] = useState("");
    const [sortableItems, setSortableItems] = useState([]);
    const [defaultItems, setDefaultItems] = useState([]);
    const [actionItems, setActionItems] = useState([]);
    const [currFieldItem, setCurrFieldItem] =  useState("");

    useEffect(()=>{
        if(props.hasOwnProperty('currFieldItem')) {
            if(props.currFieldItem.hasOwnProperty("field_type")) {
                setCurrFieldItem(props.currFieldItem.field_type);
            }
            
        }
    },[props.currFieldItem])
    

    useEffect(() => {       

        props.showGlobalLoader();
        let url = ApiUrl.getTableNames;
        let operationType = "GET"
        const displayData = payloadGenerator.generatePayload(operationType, '', '', null, "getTableNames")
        console.log('dis', displayData);
        templateService.CommonApiCall(url, displayData).then((res) => {
            if (res) {
                let resData = [];
                res.data.dataResponse[0].data.forEach((item, index) => {
                    if (item.title !== null) {
                        resData.push({
                            value: item.tablename,
                            text: item.title
                        })
                    }
                })
                setTableNames(resData);
            }
        })
            .finally(() => {
                props.hideGlobalLoader();
            });
    }, []);

    useEffect(() => {
        console.log("template grid props =>", props);
        props.templateArray.sections.forEach((val, ind) => {
            console.log(val.section_name);
            if (val.section_name === "Template Grid") {
                val.fields.forEach((fieldItem, tableIndex) => {
                    console.log(fieldItem);
                    let colValues = [];
                    let defaultValues = [];
                    fieldItem.table[0].rows.forEach((rowItem, rowIndex) => {
                        colValues.push({ text: rowItem.column_header, value: rowItem.column_header });
                        defaultValues.push(rowItem.column_header)
                    });
                    setdefaultArr([...colValues]);
                    setDefaultItems([...defaultValues]);
                    setSortableItems([...defaultValues]);
                })

            }
        })
    }, []);

    /* useEffect(() => {
        console.log("second use effect", props.currFieldItem);
        if (props.isOpen && props.hasOwnProperty('currFieldItem')) {
            if (props.currFieldItem.hasOwnProperty('field_type')) {
                console.log(props.currFieldItem.source_group)
                console.log(props.currFieldItem.field_type === "Flyout" && props.currFieldItem.source_group);
                if (props.currFieldItem.field_type === "Flyout" && props.currFieldItem.source_group) {
                    console.log('make service call');
                    const displayData = payloadGenerator.generatePayload("GET", "", "catalyst_scale", null, null);
                    console.log(displayData);
                    let url = ApiUrl.getTableColNames;
                    props.showGlobalLoader();
                    templateService.CommonApiCall(url, displayData).then((res) => {
                        console.log(res);
                        if (res.data.dataResponse[0].data.length > 0) {
                            let tabProps = res.data.dataResponse[0].data;
                            let data = [];
                            let selectedData = [];
                            tabProps.forEach((item, index) => {
                                if (item.title != null) {
                                    data.push({ text: item.title, value: item.columnname });
                                    selectedData.push(item.columnname)
                                }
                            });
                            console.log("Final Data", data);
                            setdefaultArr([...data]);
                            setDefaultItems([...selectedData]);
                            setSortableItems([...selectedData]);

                        } else {
                            console.log('error');
                        }
                    })
                        .finally(() => props.hideGlobalLoader());
                }
            }
        }
    }, [props]) */
    //[props.currFieldItem, props.currFieldItem.source_group]

    console.log(defaultArr);


    const closeModel = () => {
        //console.log(paginationCount, sortableItems, defaultPageCount)
        props.closeTemplateGridFlyout();
    }

    const submitActionDetails = () => {
        props.updateTemplateGridData({
            page_size: defaultPageCount,
            pagination: paginationCount,
            sort_columns: sortableItems,
            actions: actionItems,
            default_columns: defaultItems
        });
        closeModel();
    }

    const getList = (data) => {
        console.log(data);
        setSelectedTableNames(data);
        props.showGlobalLoader();
        const displayData = payloadGenerator.generatePayload("GET", "", data.split(".")[1].toLowerCase(), null, null)
        console.log(displayData);
        let url = ApiUrl.getTableColNames;
        templateService.CommonApiCall(url, displayData).then((res) => {
            console.log(res);
            if (res.data.dataResponse[0].data.length > 0) {
                let tabProps = res.data.dataResponse[0].data;
                let data = [];
                let selectedData = [];
                tabProps.forEach((item, index) => {
                    if (item.title != null) {
                        data.push({ text: item.title, value: item.columnname });
                        selectedData.push(item.columnname)
                    }
                });
                console.log("Final Data", data);
                setdefaultArr([...data]);
                setDefaultItems([...selectedData]);
                setSortableItems([...selectedData]);

            } else {
                console.log('error');
            }
        })
            .finally(() => {
                props.hideGlobalLoader();
            });
    }

    return (
        <>
            <Modal id="honeywell-custom-flyout" closeIcon={true} onClose={closeModel} open={props.isOpen} closeOnDimmerClick={false}>
                <Modal.Header>
                    <div>Template Grid Details</div><hr />
                </Modal.Header>
                <Modal.Content>
                    <div className="action-flyout-form-cover">
                        {console.log("currFieldItem =>",currFieldItem)}
                        {
                            (currFieldItem === "Flyout") ?
                                <Select fluid={true} label="Module Name" placeholder="Select" indicator="required"
                                    options={tableNames} search={true} multiple={false} value={selectedTableName}
                                    onChange={(data) => getList(data)} />
                                : null
                        }

                        {((selectedTableName !== "" && currFieldItem === "Flyout") || currFieldItem !== "Flyout") ?
                            <>
                                <Select fluid={true} multiple={true} label="Default Columns" placeholder="Default Columns"
                                    size="small" indicator="required"
                                    value={defaultItems} options={defaultArr}
                                    onChange={(data) => {
                                        console.log(data);
                                        setDefaultItems(data);
                                        setSortableItems(data);
                                    }} />
                                <Select fluid={true} multiple={true} label="Sortable Columns" placeholder="Sortable Columns"
                                    size="small" indicator="required"
                                    value={sortableItems} options={defaultArr}
                                    onChange={(data) => {
                                        console.log(data);
                                        setSortableItems(data)
                                    }} />
                                <Select fluid={true} multiple={true} label="Items Per Page" placeholder="Items Per Page" size="small" indicator="required"
                                    value={paginationCount} options={paginationCountOptions}
                                    onChange={(data) => {
                                        setpaginationCount(data)
                                    }} />
                                <Select fluid={true} label="Default Page Count" placeholder="Default Page Count" size="small" indicator="required"
                                    value={defaultPageCount} options={paginationCountOptions}
                                    onChange={(data) => {
                                        setDefaultPageCount(data)
                                    }} />
                                <Select fluid={true} multiple={true} label="Actions" placeholder="Actions" size="small" indicator="required"
                                    value={actionItems} options={actionOptions}
                                    onChange={(data) => {
                                        setActionItems(data)
                                    }} />
                            </>
                            : null}

                    </div><hr />
                </Modal.Content>
                <Modal.Footer>
                    <Button type="secondary" size="medium" content="Cancel" onClick={closeModel} />
                    <Button type="primary" size="medium" content="Submit" onClick={submitActionDetails} />
                </Modal.Footer>
            </Modal>
        </>
    )
}

const paginationCountOptions = [{ value: '5', text: '5' }, { value: '10', text: '10' }, { value: '20', text: '20' }, { value: '30', text: '30' }, { value: '40', text: '40' }, { value: '50', text: '50' }, { value: '100', text: '100' }];
const actionOptions = [{ value: 'Add', text: 'Add' }, { value: 'Edit', text: 'Edit' }, { value: 'Delete', text: 'Delete' }];


export default connect("", mapDispatchToProps)(TemplateGridSettingFlyOut);