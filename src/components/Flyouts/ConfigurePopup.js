import React, { useEffect, useState } from 'react';
import { Modal, Button, Input } from '@scuf/common';
import { DataTable } from '@scuf/datatable';
import { connect } from 'react-redux';

function mapStateToProps(state) {
    const templateState = state.templateReducer;
    return {
        popupGridData: templateState.popupGridData
    }
}
const ConfigurePopup = (props) => {
    const popupGridData = props.popupGridData;
    const [data, setData] = useState([])

    useEffect(() => {
        setData(props.selectedRowData.fieldConfiguration);
    },[]);
    const closeModal = () => {
        props.closeEvent()
    }

    return (
        <>
            <Modal size="large" className="popup-modal" closeIcon={true} onClose={closeModal} open={props.open} closeOnDimmerClick={false}>
                <Modal.Header>
                    <div>{props.selectedRowData ? 'Associated Run(s)'
                        : props.fieldItem.field_label}</div><hr />
                </Modal.Header>
                <Modal.Content className="popup-content">
                <DataTable
                    data={this.props.dataList}
                    reorderableColumns={true}
                    resizableColumns={true}
                    search={true}
                >

                    {props.selectedRowData.fieldConfiguration.table[0].rows.map((rowItem, rowIndex)=> {
                        return (
                            props.selectedRowData.fieldConfiguration.table[0].default_columns.includes(rowItem.column_header) ? 
                            <DataTable.Column field={rowItem.column_col_name} header={rowItem.column_header} 
                                sortable={rowItem.is_sortable} renderer={this.statusRenderer}/>
                            : ""
                        )}
                    )} 
                    <DataTable.Pagination />
                </DataTable>
                    {/* <DataTable
                        data={data}
                        search={true}
                        scrollable={true}
                    >
                        <DataTable.Column field="project" header="Project" sortable={true} />
                        <DataTable.Column field="doeStudy" header="DOE Study" sortable={true} />
                        <DataTable.Column field="doeRun" header="DOE Run#" sortable={true} />
                        <DataTable.Column field="plant" header="Plant" sortable={true} />
                        <DataTable.Column field="plantrun" header="PLant run" sortable={true} />
                        <DataTable.Column field="runStatus" header="Run Status" sortable={true} />
                       
                    </DataTable> */}
                </Modal.Content>

            </Modal>
        </>
    )
}


export default connect(mapStateToProps, null)(ConfigurePopup);