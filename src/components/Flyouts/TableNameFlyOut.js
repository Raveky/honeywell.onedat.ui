import React, { useState } from 'react';
import { Modal, Button, Input } from '@scuf/common';

const TableNameFlyOut = (props) => {
    const [tableName, setTableName] = useState("");
    const [tableNameErr, setTableNameErr] = useState("");

    const closeModel = () => {
        console.log('close');
        props.updateTableFlyoutVisibility(false);
    }

    const addTable = () => {
        console.log('add');
        props.updateTableFlyoutVisibility(false);
        console.log(props);
        props.addTableFunc(tableName);
    }

    return (
        <Modal id="honeywell-custom-flyout" closeIcon={true} onClose={closeModel} open={props.isTableFlyoutOpen} closeOnDimmerClick={false}>
            <Modal.Header>
                <div>Add Table</div><hr />
            </Modal.Header>
            <Modal.Content>
                <div className="action-flyout-form-cover">
                    <Input fluid={true} value={tableName} type="text" placeholder="Name"
                        onChange={data => { setTableName(data) }} label="Table Name" indicator="required" error={tableNameErr} />
                </div><hr />
            </Modal.Content>
            <Modal.Footer>
                <Button type="secondary" size="medium" content="Cancel" onClick={closeModel} />
                <Button type="primary" size="medium" content="Add" onClick={addTable} />
            </Modal.Footer>
        </Modal>
    )
}

export default TableNameFlyOut;

