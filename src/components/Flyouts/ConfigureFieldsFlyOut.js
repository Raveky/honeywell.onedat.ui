import React, { useState } from 'react';
import { Modal, Button, Checkbox, Input } from '@scuf/common';

import { connect } from 'react-redux';


const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch
});

const ConfigureFieldsFlyOut = (props) => {
    const [holdsState, setHoldsState] = useState(false);
    const [keyField, setKeyField] = useState(false);
    const [fieldName1, setFieldName1] = useState('');
    const [fieldName2, setFieldName2] = useState('');
    const [fieldName3, setFieldName3] = useState('');
    const [fields, setFields] = useState(["", ""])

    const [sourcegroup1, setSourcegroup1] = useState('');
    const [sourcegroup2, setSourcegroup2] = useState('');
    const [sourcegroup3, setSourcegroup3] = useState('');

    const closeModel = () => {
        props.closeEvent()
    }


    const submitActionDetails = () => {
        console.log('proceed...!');
        props.addConfigureFields({
            holds_state: holdsState, key_field: keyField
        });
        console.log(fieldName1, fieldName2, fieldName3, sourcegroup1, sourcegroup2, sourcegroup3);
        console.log(fields);
        if (props.fieldType == 'Triple Dropdowns') {
            props.createTripleDropdown({
                fieldNameArr: [fieldName1, fieldName2, fieldName3],
                sourceGroupArr: [sourcegroup1, sourcegroup2, sourcegroup3]
            });
        }

        if (props.fieldType == 'Radio Button') {


            props.createRadioButton({


                fieldNameArr: [...fields]


            });
        }



        closeModel();
    }



    const updateRadioButtonFieldvalue = (data, index) => {
        console.log(data, index)
        let fieldCopy = [...fields];
        fieldCopy[index] = data
        setFields([...fieldCopy])

    }

    const handleChange = () => {

        let tempArr = [...fields, ""]

        setFields([...tempArr])
    }

    return (
        <>
            <Modal id="honeywell-custom-flyout" closeIcon={true} onClose={closeModel} open={props.open} closeOnDimmerClick={false}>
                <Modal.Header>
                    <div>Configuration</div><hr />
                </Modal.Header>
                <Modal.Content>
                    <div className="action-flyout-form-cover">
                        <div>
                            <div>Hold State</div>
                            <Checkbox toggle={true}
                                checked={holdsState} onChange={(data) => { setHoldsState(data) }} />
                        </div>
                        <div>
                            <div>Key State</div>
                            <Checkbox toggle={true}
                                checked={keyField} onChange={(data) => { setKeyField(data) }} />
                        </div>
                        {console.log(props.fieldType)}
                        {(props.fieldType == 'Triple Dropdowns') ?
                            <>
                                <div>
                                    <Input fluid={true} value={fieldName1} type="text" placeholder="Field Name"
                                        onChange={(data) => { setFieldName1(data) }} label="Field Name 1" />
                                </div>
                                <div>
                                    <Input fluid={true} value={sourcegroup1} type="text" placeholder="Source Group"
                                        onChange={(data) => { setSourcegroup1(data) }} label="Source Group 1" search={true} />
                                </div>
                                <div>
                                    <Input fluid={true} value={fieldName2} type="text" placeholder="Field Name"
                                        onChange={(data) => { setFieldName2(data) }} label="Field Name 2" />
                                </div>
                                <div>
                                    <Input fluid={true} value={sourcegroup2} type="text" placeholder="Source Group"
                                        onChange={(data) => { setSourcegroup2(data) }} label="Source Group 2" search={true} />
                                </div>
                                <div>
                                    <Input fluid={true} value={fieldName3} type="text" placeholder="Field Name"
                                        onChange={(data) => { setFieldName3(data) }} label="Field Name 3" />
                                </div>
                                <div>
                                    <Input fluid={true} value={sourcegroup3} type="text" placeholder="Source Group"
                                        onChange={(data) => { setSourcegroup3(data) }} label="Source Group 3" search={true} />
                                </div>
                            </>
                            : ''}
                        {
                            (props.fieldType == 'Radio Button') ?
                                <>
                                    {console.log(fields)}
                                    {fields.map((fieldvalue, index) => (

                                        <div key={index}>
                                            <Input fluid={true} type="text" label="Field Name" placeholder="Field Name" value={fieldvalue} onChange={(data) => updateRadioButtonFieldvalue(data, index)} />
                                        </div>
                                    )
                                    )
                                    }

                                    {(fields.length < 5) ?
                                        <>
                                            <Button type="secondary" size="medium" content="Add" onClick={() => handleChange()} />
                                        </>
                                        : ''
                                    }
                                </>
                                : ' '
                        }

                    </div><hr />
                </Modal.Content>
                <Modal.Footer>

                    <Button type="secondary" size="medium" content="Cancel" onClick={closeModel} />
                    <Button type="primary" size="medium" content="Submit" onClick={submitActionDetails} />
                </Modal.Footer>
            </Modal>
        </>
    )
}



export default connect(null, mapDispatchToProps)(ConfigureFieldsFlyOut)