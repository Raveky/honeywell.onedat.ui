import React, { useState } from 'react';
import { Modal, Button, Input, Select } from '@scuf/common';
import ActionDetails from './../../constants/enum'


const ButtonDetailFlyOut = (props) => {
    const [customEvent, setCustomEvent] = useState("");
    const [actionName, setActionName] = useState("");
    const [actionPayLoad, setActionPayLoad] = useState("");
    const [url, setUrl] = useState("");
    const [urlValue, setUrlValue] = useState("");


    const [eventErr, setEventErr] = useState("");
    const [actionErr, setActionErr] = useState("");
    const [urlErr, setUrlErr] = useState("");
    const [urlValueError, setUrlValueError] = useState("");

    //console.log("props =>", props);
    const closeModel = () => {
        props.closeEvent()
    }

    const submitActionDetails = () => {
        let flag = false;
        if (!customEvent) {
            setEventErr('Please Enter the Event Name!');
            flag = true;
        } else {
            setEventErr('');
        }
        if (!actionName) {
            setActionErr('Please Enter the Action Name!');
            flag = true;
        } else {
            setActionErr('');
        }
        if (props.fieldValue.field_type === 'link') {
            if (!urlValue) {
                setUrlValueError('Please Enter the URL!')
                flag = true;
            } else {
                setUrlValueError('')
            }
        }
        if (!url) {
            setUrlErr('Please Enter the Url!');
            flag = true;
        } else {
            setUrlErr('');
        }
        console.log(flag, props.fieldValue.field_type)
        if (flag === false && props.fieldValue.field_type === 'link') {
            let patternErr = true;
            var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
                '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator

            if (pattern.test(urlValue)) {
                patternErr = false;
            } else {
                setUrlValueError('Please Provide Valid Url')
            } console.log(patternErr)
            if (patternErr === false) {
                props.addButtonDetails({
                    event: customEvent, action_name: actionName, action_payload: actionPayLoad, url, resource: urlValue
                })
                closeModel();
            }
        }
        if (flag === false && props.fieldValue.field_type !== 'link') {
            console.log('proceed...!');
            props.addButtonDetails({
                event: customEvent, action_name: actionName, action_payload: actionPayLoad, url, resource: ''
            })
            closeModel();
        }
    }


    return (
        <>
            <Modal id="honeywell-custom-flyout" closeIcon={true} onClose={closeModel} open={props.open} closeOnDimmerClick={false}>
                <Modal.Header>
                    <div>Action Details</div><hr />
                </Modal.Header>
                <Modal.Content>
                    <div className="action-flyout-form-cover">
                            <Select fluid={true} label="Event Type" placeholder="Event Type" size="small" indicator="required"
                            value={customEvent} options={eventOptions}
                            onChange={(data) => {
                                setCustomEvent(data)
                            }} error={eventErr} />

                        <Input fluid={true} value={actionName} type="text" placeholder="Action Name"
                            onChange={data => { setActionName(data) }} label="Action Name" indicator="required" error={actionErr} />

                        <Input fluid={true} value={actionPayLoad} type="text" placeholder="Action Payload"
                            onChange={data => { setActionPayLoad(data) }} label="Action Payload" />

                        <Select fluid={true} label="Endpoint" placeholder="Select Endpoint" size="small" indicator="required"
                            value={url} options={avaialbleControlOptions}
                            onChange={(data) => {
                                setUrl(data)
                            }} error={urlErr} />

                        {props.fieldValue.field_type === 'link' ?
                            <div>{console.log('inside last field')}
                                <Input fluid={true} value={urlValue} type="text" placeholder="URL" label="Enter URL" indicator="required"
                                    value={urlValue} error={urlValueError} onChange={data => { setUrlValue(data) }}
                                /></div>
                            : console.log('after input')
                        }
                    </div><hr />
                </Modal.Content>
                <Modal.Footer>
                    <Button type="secondary" size="medium" content="Cancel" onClick={closeModel} />
                    <Button type="primary" size="medium" content="Submit" onClick={submitActionDetails} />
                </Modal.Footer>
            </Modal>
        </>
    )
}
const avaialbleControlOptions= [];
ActionDetails.forEach((action)=>{
    avaialbleControlOptions.push({
        value: action.name,
        text: action.name
    })
})
const eventOptions = [{ value: 'click', text: 'Click' }, { value: 'blur', text: 'Blur' }, { value: 'change', text: 'Change' }]
const styles = {
    coverStyle: {
        left: '60%',
        position: ['absolute', '!important'],
        width: '40%',
        margin: [[0], '!important']
    }
}


export default ButtonDetailFlyOut;