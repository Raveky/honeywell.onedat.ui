import React,{useEffect,useState} from 'react';
import { Icon } from '@scuf/common';
import TechnologySelectionModel from './TechnologySelectionModel';
import { connect } from 'react-redux';
import {addTechnology} from './../actions/Template';

function mapStateToProps(state){
    return{
     tempArray: state
    } 
 }

const mapDispatchToProps = (dispatch)=>({
    dispatch:dispatch,
    addTechnology: (value)=>dispatch(addTechnology(value)) 
 });

const Home = (props) => {
    const colorArr = ["#50c9bd", "#f1a686", "#7870a4", "#f6d598", "#596174", "#e88788", "#6acddb", "#dcdcc4", "#9fdfff", "#4ca5c5", "#ffe6ae", "#de75c7"];
    const [isTechnologyModelShow, setIsTechnologyModelShow] = useState(false)  
    const [selectedTechnology, setSelectedTechnology] = useState([]);
const [techErr,setTechErr] =useState("");
    const updateTechnologyModelVisibility = ()=> {
        setIsTechnologyModelShow(false);
        localStorage.setItem("showPopup","false");
    }
    const techArray = [
        {
            title: 'FEEDSTOCKDAT',
            description: 'Sample descriptions about the technology.'
        },
        {
            title: 'NAPHTHDAT',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
            title: 'HYDAT',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
            title: 'HCYE',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
            title: 'ABSORBENTS',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
            title: 'MINIDAT',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
            title: 'HEAVIER',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
            title: 'FCC',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
            title: 'AROMATICS',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
            title: 'HPPA',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        },
        {
            title: 'HPT',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        }
    ];
    const reportsArray = [];
    reportsArray.length = 9;
    reportsArray.fill("Report Name Sample");
    const graphsArray = [];
    graphsArray.length = 9;
    graphsArray.fill("Graph Name Sample");
    const getRandomInt = (max) => {
        return Math.floor(Math.random() * Math.floor(max));
    }
    useEffect(() => {
        console.log(selectedTechnology);
        const checkValue = localStorage.getItem("showPopup");
        if(checkValue === 'true' && selectedTechnology.length === 0) {
            setIsTechnologyModelShow(true);
        } else {
            setIsTechnologyModelShow(false);
        }
      }, [selectedTechnology]);

      const closeModel = (data)=>{ 

        if(data===null)
        {
            setTechErr("Please Enter the Technology!");
        }
        else
        {

            setTechErr("");
            console.log("data =>", data);   
        updateTechnologyModelVisibility();
        //setSelectedTechnology(prevNames => [...prevNames, ...data]);        
        setSelectedTechnology(data);        
        console.log(data)
        console.log(selectedTechnology);
        props.addTechnology(data);
        }
        
        
      }
    return (
        <div>
            <TechnologySelectionModel isTechnologyModelShow={isTechnologyModelShow}  closeModel={closeModel} techErr={techErr} />
            <div className="home-technical-part">
                <p className="home-main-title">Technologies</p>
                {
                    techArray.map((i, v) => {
                        return (
                            <div key={v} className="home-technical-tab-each" style={{ borderLeft: '4px solid ' + colorArr[getRandomInt(colorArr.length)] + '' }}>
                                {(selectedTechnology.includes(i.title))?<Icon className="home-technology-green-check" root="common" name="badge-check" size="small" color="white" />:""}
                                <div className="home-tech-title">{i.title}</div>
                                <div className="home-tech-desc">{i.description}</div>
                            </div>
                        )
                    })
                }
            </div>
            <div className="home-reports-part">
                <div className="home-main-title reports-cover">
                    <div>Reports</div>
                    <div className="home-main-title-view-all">View All
                    <Icon root="building" name="caret-down" size="small" color="#1274be" /></div>
                </div>

                <div className="home-reports-tab-each home-reports-add">
                    <Icon className="home-reports-ico" root="building" name="slider-controls-plus" size="medium" color="#4caae9" />
                    <div className="home-reports-title">Add Report</div>
                </div>
                {/* {
                    reportsArray.map((i, v) => {
                        return (
                            <div key={v} className="home-reports-tab-each" style={{ borderLeft: '4px solid ' + colorArr[getRandomInt(colorArr.length)] + '' }}>
                                <Icon className="home-reports-ico" root="building" name="document" size="medium" />
                                <div className="home-reports-title">{i}</div>
                            </div>
                        )
                    })
                } */}
            </div>
            <div className="home-graphs-part">
                <div className="home-main-title reports-cover">
                    <div>Graphs</div>
                    <div className="home-main-title-view-all">View All
                    <Icon root="building" name="caret-down" size="small" color="#1274be" /></div>
                </div>

                <div className="home-reports-tab-each home-reports-add">
                    <Icon className="home-reports-ico" root="building" name="slider-controls-plus" size="medium" color="#4caae9" />
                    <div className="home-reports-title">Add Graphs</div>
                </div>
                {
                    graphsArray.map((i, v) => {
                        return (
                            <div key={v} className="home-reports-tab-each" style={{ borderLeft: '4px solid ' + colorArr[getRandomInt(colorArr.length)] + '' }}>
                                <Icon className="home-reports-ico" root="building" name="graph-alt" size="medium" />
                                <div className="home-reports-title">{i}</div>
                            </div>
                        )
                    })
                }
            </div>

        </div>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);