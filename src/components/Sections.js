import React, { useState } from 'react';
import Fields from './Fields';
import { Card, Select, Button, Icon, Input, Checkbox, Accordion, Popup } from '@scuf/common';
import ControlOptions from '../containers/ControlOptions';
import { connect } from 'react-redux';
import {
    getSectionName, addSection, deleteSection, addControls, toggleControls,
    resetTemplateBuild, updateTableFlyoutVisibility, assignAddTableFunctionality,
    toggleOrientationControls, updateOrientationType, addTemplateColData
} from '../actions/Template';
import OrientationPopup from './custom-popups/OrientationPopup';
import SubSection from './SubSection';
import Confirmation from './custom-popups/DeleteConfirmation';

function mapStateToProps(state) {
    return {
        sections: state.sections,
        templateArray: state
    }
}

const mapDispatchToProps = (dispatch) => ({
    dispatch: dispatch,
    deleteSection: (index, mainIndex, e) => {
        e.stopPropagation();
        dispatch(deleteSection(index, mainIndex))
        dispatch(addTemplateColData())
    },
    addControls: (secItem, tableName, fType, colType) => {
        dispatch(addControls(secItem, tableName, fType, colType))
        dispatch(addTemplateColData())
    },
    toggleControls: (secItem, e) => {
        e.stopPropagation();
        dispatch(toggleControls(secItem))
    },
    updateTableFlyoutVisibility: (flag) => {
        dispatch(updateTableFlyoutVisibility(flag))
    },
    assignAddTableFunctionality: (func) => {
        dispatch(assignAddTableFunctionality(func));
    },
    toggleOrientationControls: (secObj, e) => {
        //console.log(e);
        e.stopPropagation();
        dispatch(toggleOrientationControls(secObj))
    },
    updatePortraitMode: (secObj, oType) => {
        dispatch(updateOrientationType(secObj, oType))
        dispatch(toggleOrientationControls(secObj))
    },
    resetTemplate: () => dispatch(resetTemplateBuild())
});

const stateOptions = [{ value: 'General Information', text: 'General Information', sub: false },
{ value: 'Mixing', text: 'Mixing', sub: true }, { value: 'Powder Mix', text: 'Powder Mix', sub: true },
{ value: 'Water Addition', text: 'Water Addition', sub: false },
{ value: 'Mixing Instructions', text: 'Mixing Instructions', sub: false }, { value: 'Template Grid', text: 'Template Grid' }];


const Sections = (props) => {
    //console.log(props);
    const addNewItemSelectBoxRef = React.createRef();
    const addSubSec = (value, secItem) => {
        // e.stopPropagation();
        /* props.dispatch({
            type: 'ADD_SUB_SECTION',
            secItem, value
        }); */
    }
    const [showConfirmation, setShowConfirmation] = useState(false);
    const [index, setIndex] = useState(null);
    const [mainIndex, setMainIndex] = useState(null);
    const [event, setEvent] = useState(null);

    const addNewSubSectionItem = (secItem) => {
        //console.log(addNewItemSelectBoxRef.current.state.values);
        let value = addNewItemSelectBoxRef.current.state.values;
        props.dispatch({
            type: 'ADD_SUB_SECTION',
            secItem, value
        });
    }

    const showConfirmationPopup = (index, mainIndex, e) => {
        setShowConfirmation(true);
        setIndex(index);
        setMainIndex(mainIndex);
        setEvent(e);
    }

    const cancelDelete = () => {
        setShowConfirmation(false);
    }

    const proceedDelete = () => {
        setShowConfirmation(false);
        props.deleteSection(index, mainIndex, event);
    }

    return (
        <div>
            <div key={props.secIndex}>
                <Accordion className="preview-accoridian-pop">
                    <Accordion.Content title={
                        <div className="main-section" style={props.buildTemplateStyle.mainSectionbar}  >
                            <div style={props.buildTemplateStyle.mainSectionheader}>{(!props.secItem.isSubSection) ? props.secItem.section_name : props.secItem.sub_section_name}</div>
                            {/* <div className="custom-add-sec-pop"> */}
                            <Popup className="popup-theme-wrap" onClick={(e) => { e.stopPropagation() }}
                                element={
                                    (props.secItem.fields.length === 0 && !props.secItem.isSubSection&& !props.secItem.directFieldSection) ?
                                        <div className="add-new-section"><Icon style={props.buildTemplateStyle.mainSectionControl} root="common"
                                            name="slidercontrols-plus" size="small"></Icon>New Section<Icon style={props.buildTemplateStyle.mainSectionControl} root="global"
                                                name="caret-down" size="small"></Icon>
                                        </div> : ""
                                }
                                on="click" position="bottom center">
                                <div className="new-section-pop">
                                    <div className="">
                                        <Select ref={addNewItemSelectBoxRef} fluid={true} placeholder="Select or Add Section/Unit Operation"
                                            options={stateOptions} onChange={(e) => addSubSec(e, props.secItem)}
                                        />
                                    </div>
                                    <div className="new-section-add-button">
                                        <Button type="primary" size="small" icon={<Icon root="global" name="slider-controls-plus" size="medium" />}
                                            iconPosition="left" content="ADD" onClick={() => {
                                                addNewSubSectionItem(props.secItem)
                                            }} />
                                    </div>
                                </div>

                            </Popup>

                            <Icon style={props.buildTemplateStyle.mainSectionControl} root="common" name="settings" size="small" onClick={props.toggleOrientationControls.bind(this, props.secItem)} />
                            {(props.secItem.isShowOrientationIcons) ? <OrientationPopup updatePortraitMode={props.updatePortraitMode.bind(this, props.secItem)} /> : ""}
                            {(props.secItem.sub_sections.length === 0 && !props.secItem.directFieldSection) ? <Icon style={props.buildTemplateStyle.mainSectionControl} root="common" name="badge-plus" size="small"
                                onClick={(e) => props.toggleControls(props.secItem, e)} /> : null}
                            {(props.secItem.showControlList) ? <ControlOptions addSubSections={props.addControls.bind(this, props.secItem)}
                                setStyle={{ top: '31px', right: '27px' }} updateTableFlyoutVisibility={props.updateTableFlyoutVisibility.bind(this)} assignAddTableFunctionality={props.assignAddTableFunctionality.bind(this)} /> : ''}
                            {/* {(props.secItem.showControlList) ? <ControlOptions addSubSections={(key) => props.addControls(props.secItem,key, '')} 
                                setStyle={{ top: '31px', right: '13px' }} updateTableFlyoutVisibility={props.updateTableFlyoutVisibility.bind(this)} assignAddTableFunctionality = {props.assignAddTableFunctionality.bind(this)}   /> : ''} */}
                            <Icon style={{ ...props.buildTemplateStyle.mainSectionControlAccord, ...props.buildTemplateStyle.makeBorderZero }}
                                root="common" name="delete" size="small" /*onClick={(e) => props.deleteSection(props.secIndex,props.mainIndex, e)}*/ onClick={(e) => showConfirmationPopup(props.secIndex, props.mainIndex, e)} />
                        </div>
                    } arrowPosition="right">

                        {
                            props.secItem.sub_sections.map((subItem, subIndex) => {
                                return (
                                    <>
                                        <SubSection mainIndex={props.secIndex} key={subIndex} secIndex={subIndex} secItem={subItem} parentArray={props.parentArray} buildTemplateStyle={props.buildTemplateStyle} hasError={props.hasError} />
                                    </>
                                )
                            })
                        }
                        {
                            props.secItem.fields.map((fItem, fIndex) => {
                                return <Fields key={fIndex} fieldIndex={fIndex} fieldItem={fItem}
                                    parentArray={props.secItem} buildTemplateStyle={props.buildTemplateStyle} hasError={props.hasError}> </Fields>
                            })
                        }
                    </Accordion.Content>
                </Accordion>
            </div>
            {showConfirmation ?
                <Confirmation showConfirmation cancelDelete={() => cancelDelete()} proceedDelete={() => proceedDelete()} />
                : null
            }
        </div>
    );
}
export default connect(mapStateToProps, mapDispatchToProps)(Sections);