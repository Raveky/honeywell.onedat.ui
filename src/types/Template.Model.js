export class TemplateModel {
    template_name = '';
    template_id = 0;
    template_description = '';
    version  = '';
    authorization = '';
    technology_name = '';
    module_name = '';
    tbl_comment_name = '';
    tableName = '';
    showHideFieldControls = false;
    child_templates = []; /* Use class  Sub_Template */
    sections = []; /* Use class SectionModel */
    fields = []; /* Use class Fields */
    actions = []; /* Use class Actions */
    isTableFlyoutOpen = false;
    addTableFunc = null;
    render_seq_id = 0;
    published = false;
    render_as = 'Section';
}

/* class for Actions(API /javascript functions) at any level (template, section , sub section , fields)  */
export class Actions {
    event = '';
    type = '';
    method = '';
    resource = '';
    payload = '';
}

/* child templates class */
export class Sub_Template {
    sub_template_name = '';
    sub_template_id = '';
}

/* Sections under Template */
export class SectionModel {
    section_name = '';
    sub_sections = []; /* Use class SubSectionModel */
    showControlList= false;
    fields = []; /* Use class Fields */
    show_sub_section=false;
    export_print = '';
    section_id = 0;
    orientation = '';
    render_as = '';
    child_templates = []; /* Use class  Sub_Template */
    isShowOrientationIcons = false;
    isSubSection = false;
	directFieldSection = false;
}

export class SubSectionModel {
    sub_section_id = 0;
    export_print = '';
    sub_section_name = '';
    orientation = '';
    render_as = '';
    fields = []; /* Use class Fields */
    child_templates = []; /* Use class  Sub_Template */
    sub_sections = []; /* Use class SubSectionModel */
    isSubSection = true;
}

/* Class for Fields , this can be at template , sections and sub section level */
export class Fields {
    field_id = 0;
    field_sequence_id = 0;
    field_col_name = '';
    field_label = '';
    field_type = '';
    field_data_type= '';
    field_max_length=0;
    field_min_length=3;
    field_value = '';
    source_group = '';
    mandatory = false;
    lookup_endpoint= '';
    holds_state = false;
    showHideIcons = false;
    showControlList = false;
    key_field = false;
    lookup_endpoint = '';
    default_field_value = '';
    showButtonPopup = false;
    button_property = false;
    table = []; /* Use class TableTemplate */
    actions = []; /* Use class Actions */
	sourceGroupArr = [];
    fieldNameArr = [];
    fieldValueArr = [];
    showFlyoutSearchModal = false;
    showConfigurationModal = false;
    showPopup = false;
}

/* Class for data table under fields */
export class TableTemplate {
    table_id = '';
    table_name = '';
    pagination = '';
    page_size = '';
    default_page_size = '';
    column_headers = [];
    column_value = [];
    default_columns = '';
    sort_columns = [];
    user_column_prefs = []; /* Use class UserColumnPref */
    rows = []; /* Use class TableRow */
    actions = [];
}

export class GridActions {
    delete = true;
    add = false;
    edit = false;
}

/* required for table class */
export class UserColumnPref {
    user_id = 0;
    column_prefs = '';
}

/* required for table class */
export class TableRow {
    column_id = 0;
    column_name = '';
    column_value = '';
    column_type = '';
    column_header = '';
    column_label = '';
    is_sortable = false;
    source_group = '';
    column_col_name = '';
    disabled =  false;
    showControlList = false;
    attachedField = '';
    fieldConfiguration = [];
}