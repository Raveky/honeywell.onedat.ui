import React from 'react';
import './App.css';
import '@scuf/common/honeywell/theme.css';
import ExampleContainer from './containers/SideBar';
import { Provider } from 'react-redux';
import store from './store/Store';
import LogInPage from './containers/LogInPage';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false
    }


  }

  updateState = () => {
    this.setState({
      isLoggedIn: true
    })
  }


  render() {

    return (
      <Provider store={store}>
        <div style={{ height: '100%' }}>
          <ToastContainer />
          {(this.state.isLoggedIn) ? <ExampleContainer /> : <LogInPage updateState={this.updateState} />}
        </div>
      </Provider>
    );
  }
}