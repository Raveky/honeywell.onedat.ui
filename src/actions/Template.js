// import {CREATE_TEMPLATE} from '../constants/Templateconst';

export const createTemplate = (value) => ({
    type: 'CREATE_TEMPLATE',
    value
})

export const findTemplate = () => ({
    type: 'SHOW_HIDE_FIND_TABLE'
});
export const editTemplate = () => ({
    type: 'EDIT_TEMPLATE'
});
export const cloneTemplate = () => ({
    type: 'CLONE_TEMPLATE'
});
export const deleteTemplate = () => ({
    type: 'DELETE_TEMPLATE'
});

export const resetTemplate = () => ({
    type: 'RESET_TEMPLATE'
});

export const getTemplateNames = (value) => ({
    type: 'UPDATE_TEMPLATE_NAME',
    value
});

export const getTemplateVersion = (value) => ({
    type: 'UPDATE_TEMPLATE_VERSION',
    value
});

export const getTemplateDescription = (value) => ({
    type: 'UPDATE_TEMPLATE_DESCRIPTION',
    value
});

export const updateTblName = (value) => ({
    type: 'UPDATE_TBL_NAME',
    value
});

export const getSectionName = (value) => ({
    type: 'SECTION_NAME',
    value
});

export const addSection = () => ({
    type: 'ADD_SECTION'
});
export const addTemplateColData = () => ({
    type: 'ADD_TEMPLATE_GRID_COL_DATA'
});

export const updateTemplateGridData = (value) => ({
    type: 'UPDATE_TEMPLATE_GRID_DATA',
    value
});

export const updatePopupGridData = (value) => ({
    type: 'UPDATE_POPUP_GRID_DATA',
    value
});

export const updateFlyoutGridData = (fObject, value) => ({
    type: 'UPDATE_FLYOUT_GRID_DATA',
    fObject, value
});

export const deleteSection = (index, mainIndex) => ({
    type: 'DELETE_SECTION',
    index, mainIndex
});

export const addControls = (secItem, fType, colType, tableName) => ({
    type: 'ADD_FIELDS',
    secItem, fType, colType, tableName
});

export const toggleControls = (secItem) => ({
    type: 'TOGGLE_ICONS',
    secItem
});

export const updateFieldName = (fObject, value) => ({
    type: 'UPDATE_FIELD_NAME',
    fObject, value
});

export const clearFieldName = (fObject, value) => ({
    type: 'CLEAR_FIELD_NAME',
    fObject, value: ''
});

export const updateFieldType = (fObject, value) => ({
    type: 'UPDATE_FIELD_TYPE',
    fObject, value
});

export const updateSourceGroup = (fObject, value) => ({
    type: 'UPDATE_SOURCE_GROUP',
    fObject, value
});

export const updateMandatory = (fObject, value) => ({
    type: 'UPDATE_MANDATORY',
    fObject, value
});

export const openHideButtonDetails = (fObject, value) => ({
    type: 'OPEN_HIDE_BUTTON_DETAILS',
    fObject, value
});

export const addButtonDetails = (fObject, value) => ({
    type: 'ADD_BUTTON_DETAILS',
    fObject, value
});

export const resetTemplateBuild = () => ({
    type: 'RESET'
});

export const cloneControl = (fObject, fArray) => ({
    type: 'CLONE_CONTROL',
    fObject, fArray
});

export const addFieldLevelControl = (fObject, value) => ({
    type: 'SHOW_CONTROLS',
    fObject, value
});

export const deleteControl = (fArray, index) => ({
    type: 'DELETE_CONTROL',
    fArray, index
});

export const addFieldControls = (fObject, fArray, index, fType, colType) => ({
    type: 'ADD_FIELD_BETWEEN',
    fObject, fArray, index, fType, colType
});

export const displayIcons = (fObject, value) => ({
    type: 'SHOWHIDE_ICONS',
    fObject, value
});

export const hideIcons = (fObject, value) => ({
    type: 'SHOWHIDE_ICONS',
    fObject, value
});

export const updateTableFlyoutVisibility = (flag) => ({
    type: 'UPDATE_TABLE_FLYOUT_VISIBLITY',
    flag
});

export const assignAddTableFunctionality = (func) => ({
    type: 'ADD_TABLE_FUNCTIONALITY',
    func
});

export const deleteColumn = (fObject, columnIndex) => ({
    type: 'DELETE_COLUMN',
    fObject, columnIndex
});

export const updateColumnName = (fObject, columnIndex, value) => ({
    type: 'UPDATE_COLUMN_NAME',
    fObject, columnIndex, value
});

export const updateAddedColumnDetails = (fObject, columnIndex, value) => ({
    type: 'UPDATE_ADDED_COLUMN_DETAILS',
    fObject, columnIndex, value
});

export const addColumnLevelControl = (fObject, columnIndex) => ({
    type: 'SHOW_COLUMN_CONTROLS',
    fObject, columnIndex
});

export const attachField = (fObject, columnIndex, value) => ({
    type: 'ATTACH_FIELD',
    fObject, columnIndex, value
});

export const updateColumnType = (fObject, columnIndex, value) => ({
    type: 'UPDATE_COLUMN_TYPE',
    fObject, columnIndex, value
});
export const updateColumnSourceGroup = (fObject, columnIndex, value) => ({
    type: 'UPDATE_COLUMN_SOURCE_GROUP',
    fObject, columnIndex, value
});

export const addColumn = (fObject) => ({
    type: 'ADD_COLUMN',
    fObject
});

export const toggleOrientationControls = (secObj) => ({
    type: 'TOGGLE_ORIENTATION_ICONS',
    secObj
});

export const updateOrientationType = (secObj, oType) => ({
    type: 'UPDATE_ORIENTATION_TYPE',
    secObj, oType
});

export const updateBtnProperty = (fObject, value) => ({
    type: 'UPDATE_BTN_PRTY',
    fObject, value
});

export const configureFields = (fObject, value) => ({
    type: 'SHOW_HIDE_CONFIGURE_FIELDS',
    fObject, value
});

export const addConfigureFields = (fObject, value) => ({
    type: 'ADD_CONFIGURE_FIELDS',
    fObject, value
});

export const handleSearchSelect = (result) => ({
    type: 'HANDLE_SEARCH_SELECT',
    result
});
export const handleSearchChange = (value) => ({
    type: 'HANDLE_SEARCH_CHANGE',
    value
});
export const addResult = (value) => ({
    type: 'ADD_RESULT',
    value
});
export const addTechnology = (value) => ({
    type: 'ADD_TECHNOLOGY',
    value
});
export const updateFieldValue = (fObject, value) => ({
    type: 'UPDATE_FIELD_VALUE',
    fObject, value
});

export const createTripleDropdown = (fObject, value) => ({
    type: 'CREATE_TRIPLE_DROPDOWN',
    fObject, value
});
export const createRadioButton = (fObject, value) => ({
    type: 'CREATE_RADIOBUTTON', fObject, value

})

export const updateActiveSideBar = (value) => ({
    type: 'UPDATE_ACTIVE_SIDEBAR',
    value
});

export const configureFlyoutSearchModal = (fObject, value) => ({
    type: 'SHOW_HIDE_FLYOUT_SEARCH_MODAL',
    fObject, value
});

export const fetchFamilyName = () => ({
    type: 'FETCH_FAMILY_NAME'
});

export const updateFamilyName = (value) => ({
    type: 'UPDATE_FAMILY_NAME',
    value
});

export const updateHoldstate = (fObject, value) => ({
    type: 'UPDATE_HOLD_STATE',
    fObject, value
});

export const updateKeyState = (fObject, value) => ({
    type: 'UPDATE_KEY_STATE',
    fObject, value
});

export const configurePopup = (fObject, value) => ({
    type: 'OPEN_POPUP',
    fObject, value
});

export const showGlobalLoader = () => ({
    type: 'SHOW_GLOBAL_LOADER'
});

export const hideGlobalLoader = () => ({
    type: 'HIDE_GLOBAL_LOADER'
});

export const updateTemplateStatus = (fObject, value) => ({
    type: 'UPDATE_TEMPLATE_STATUS',
    fObject, value
});
// export const updateRenderUrl = (fObject, value) => ({
//     type: 'UPDATE_RENDER_URL',
//     fObject, value
// });