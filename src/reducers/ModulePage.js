const initState = {
    module: {
        module_name: '',
        module_version: 1.0,
        module_description: ''
    }
}

const moduleReducer = (state = initState, action) => {

    let modItems = { ...state.module };

    switch (action.type) {
        case 'Module_Update':
            console.log(state);
            return { ...state, module:{
                ...state.module,
                module_name: action.value
            }
        };
        default:
            return state;
    }
};

export default moduleReducer; 