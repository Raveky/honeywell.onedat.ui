import { SectionModel, Fields, Actions, SubSectionModel, TableTemplate, UserColumnPref, TableRow } from '../types/Template.Model';

const initState = {
  template: {
    template_name: '',
    version: '1.0',
    template_description: '',
    tbl_comment_name: '',
    tableName: '',
    sectionName: '',
    showHideFieldControls: false,
    sections: [],
    fields: [],
    isTableFlyoutOpen: false,
    addTableFunc: null,
    technology_name: '',
    tbl_name: '',
    familyName: '',
    userName: '',
    published: false,
    render_as: "Section"
  },
  searchList: [
    { columnname: null, title: 'All fields marked with * are mandatory' },
    { columnname: null, title: 'RESET' },
    { columnname: null, title: 'FIND' },
    { columnname: null, title: 'SAVE' },
    { columnname: null, title: 'Popup Grid' }
  ],
  firstRender: true,
  technologyArray: [],
  results: [],
  autoCompleteBoxValue: '',
  sampleData: [
    { title: 'create' },

  ],
  activeSideBar: '',
  showHideFindTable: false,
  find_results: [],
  module_grid_data: [],
  flyoutSearchData: [
    { id: 1, catalyst_scale_nm: "DHC31", catalyst_scale_desc: "AA", active_ind: 'Y' },
    { id: 2, catalyst_scale_nm: "DHC32", catalyst_scale_desc: "AB", active_ind: 'N' },
    { id: 3, catalyst_scale_nm: "DHC33", catalyst_scale_desc: "AC", active_ind: 'Y' },
    { id: 4, catalyst_scale_nm: "DHC34", catalyst_scale_desc: "AD", active_ind: 'Y' },
    { id: 5, catalyst_scale_nm: "DHC35", catalyst_scale_desc: "AE", active_ind: 'Y' },
    { id: 6, catalyst_scale_nm: "DHC36", catalyst_scale_desc: "AF", active_ind: 'N' },
    { id: 7, catalyst_scale_nm: "DHC37", catalyst_scale_desc: "AG", active_ind: 'Y' },
    { id: 8, catalyst_scale_nm: "DHC38", catalyst_scale_desc: "AH", active_ind: 'N' },
    { id: 9, catalyst_scale_nm: "DHC39", catalyst_scale_desc: "AI", active_ind: 'Y' },
    { id: 10, catalyst_scale_nm: "DHC40", catalyst_scale_desc: "AJ", active_ind: 'Y' },
    { id: 11, catalyst_scale_nm: "DHC41", catalyst_scale_desc: "AK", active_ind: 'N' },
    { id: 12, catalyst_scale_nm: "DHC42", catalyst_scale_desc: "AL", active_ind: 'N' },
    { id: 13, catalyst_scale_nm: "DHC43", catalyst_scale_desc: "AM", active_ind: 'N' },
    { id: 14, catalyst_scale_nm: "DHC44", catalyst_scale_desc: "AN", active_ind: 'Y' },
    { id: 15, catalyst_scale_nm: "DHC45", catalyst_scale_desc: "AO", active_ind: 'Y' },
    { id: 16, catalyst_scale_nm: "DHC46", catalyst_scale_desc: "AP", active_ind: 'N' },
    { id: 17, catalyst_scale_nm: "DHC47", catalyst_scale_desc: "AQ", active_ind: 'N' },
    { id: 18, catalyst_scale_nm: "DHC48", catalyst_scale_desc: "AR", active_ind: 'Y' },
    { id: 19, catalyst_scale_nm: "DHC49", catalyst_scale_desc: "AS", active_ind: 'Y' },
    { id: 20, catalyst_scale_nm: "DHC50", catalyst_scale_desc: "AT", active_ind: 'N' },
  ],
  popupGridData: [
    { id: 1, project: "HS-10 Low & High Na Catalyst Testing31", doeStudy: "HS-10 Low & High Na Catalyst Testing31", doeRun: "1", plant: "2012", plantrun: "8", runStatus: "Run Executed" },
    { id: 2, project: "HS-10 Low & High Na Catalyst Testing32", doeStudy: "HS-10 Low & High Na Catalyst Testing31", doeRun: "1", plant: "2012", plantrun: "8", runStatus: "Run Executed" },
    { id: 3, project: "HS-10 Low & High Na Catalyst Testing33", doeStudy: "HS-10 Low & High Na Catalyst Testing31", doeRun: "1", plant: "2012", plantrun: "8", runStatus: "Run Executed" },
    { id: 4, project: "HS-10 Low & High Na Catalyst Testing34", doeStudy: "HS-10 Low & High Na Catalyst Testing31", doeRun: "1", plant: "2012", plantrun: "8", runStatus: "Run Executed" },
    { id: 5, project: "HS-10 Low & High Na Catalyst Testing35", doeStudy: "HS-10 Low & High Na Catalyst Testing31", doeRun: "1", plant: "2012", plantrun: "8", runStatus: "Run Executed" }
  ],
  templateGrid: [],
  isShowGlobalLoader: false,
  module_definition: []
}

const findTemplateAction = (action = '', currentState, selectedRow) => {
  if (action === 'EDIT') {
    console.log("edit template");

  } else if (action === 'CLONE') {
    console.log("clone template");
  } else if (action === 'DELETE') {
    console.log("delete template");
  }
  console.log("selectedRow: ", selectedRow);
  return {};
}

const templateReducer = (state = initState, action) => {
  let temItems = { ...state.template };

  switch (action.type) {
    case 'UPDATE_USERNAME':
      console.log("inside Templatereducer: ", state);
      return {
        ...state, template: {
          ...state.template,
          userName: action.value
        }
      };

    case 'UPDATE_TEMPLATE_NAME':
      console.log("inside Templatereducer: ", state);
      return {
        ...state, template: {
          ...state.template,
          template_name: action.value
        }
      };

    case 'UPDATE_TEMPLATE_STATUS':
      console.log("fObject: ", action.fObject);
      console.log("updating Template status to: ", action.value);
      action.fObject.published = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };   
    case 'UPDATE_TEMPLATE_VERSION':
      return {
        ...state, template: {
          ...state.template,
          version: action.value
        }
      };
    case 'UPDATE_TEMPLATE_DESCRIPTION':
      return {
        ...state, template: {
          ...state.template,
          template_description: action.value
        }
      };
    case 'UPDATE_TBL_NAME':
      console.log("state", state)
      return {
        ...state, template: {
          ...state.template,
          tbl_comment_name: action.value.title,
          tableName: action.value.tablename
        }
      };
    case 'RESET_TEMPLATE':
      return {
        ...state, template: {
          ...state.template,
            template_name: '',
            template_description: '',
            tbl_comment_name: '',
            tableName: ''
        }
      };
    case 'SECTION_NAME':
      return {
        ...state, template: {
          ...state.template,
          sectionName: action.value
        }
      };
    case 'SHOW_HIDE_TEMP_CONTROLS':
      return {
        ...state, template: {
          ...state.template,
          showHideFieldControls: action.payload
        }
      };
    case 'ADD_SECTION':
      const sectionModel = new SectionModel();
      sectionModel.section_name = state.autoCompleteBoxValue;

      let items = [...temItems.sections];
      console.log("state.autoCompleteBoxValue =>", state.autoCompleteBoxValue, action);


      if (state.autoCompleteBoxValue === 'Template Grid') {
        sectionModel.fields.push({
          table: []
        });
        sectionModel.fields[0].table.push(new TableTemplate());
        sectionModel.fields[0].table[0].table_name = 'Template Grid';
        sectionModel.fields[0].table[0].page_size = '10';
        sectionModel.fields[0].table[0].default_page_size = '10';
        sectionModel.fields[0].table[0].column_value = [];
        sectionModel.fields[0].table[0].column_headers = [];
        sectionModel.fields[0].table[0].rows = [];
        if (items.length > 0) {
          items.forEach((secItem, index) => {
            if (secItem.section_name !== 'Template Grid') {
              secItem.fields.forEach((fItem, fIndex) => {
                if (fItem.field_col_name !== null) {
                  sectionModel.fields[0].table[0].rows.push({
                    column_name: fItem.field_label,
                    column_type: fItem.field_type,
                    column_header: fItem.field_label,
                    column_col_name: fItem.field_col_name,
                    source_group: fItem.source_group,
                    is_sortable: false,
                    disabled: true
                  });
                }
              })
            }
          });
        }
        console.log("template grid", items);
      }
      items.push(sectionModel);
      console.log('add section=>', state);
      return {
        ...state, template: {
          ...state.template,
          sections: items
        }
      };
    case 'ADD_TEMPLATE_GRID_COL_DATA':
      let tempGridObj = [...temItems.sections].findIndex((val, ind) => {
        return val.section_name === 'Template Grid'
      });
      let tempSections = [...temItems.sections];
      if (tempGridObj > -1) {
        tempSections[tempGridObj].fields[0].table[0].column_value = [];
        tempSections[tempGridObj].fields[0].table[0].column_headers = [];
        tempSections.forEach((secValue, secInd) => {
          if (secValue.section_name === 'Template Grid') return;
          secValue.fields.forEach((fieldVal, fieldInd) => {
            if (!fieldVal.isChecked) return;
            console.log(fieldVal);
            //sectionModel.fields[0].table[0].rows.push(new TableRow()); 
            tempSections[tempGridObj].fields[0].table[0].column_value.push(fieldVal.field_label);
            tempSections[tempGridObj].fields[0].table[0].column_headers.push(fieldVal.field_col_name);
          });
        });
      }
      console.log("state from GRID COL Data =>", state);
      return {
        ...state, template: {
          ...state.template,
          sections: tempSections
        }
      };

    case 'UPDATE_TEMPLATE_GRID_DATA':
      let getTempGridObj = [...temItems.sections].findIndex((val, ind) => {
        return val.section_name === 'Template Grid'
      });
      let templateSections = [...temItems.sections];
      if (getTempGridObj > -1) {
        console.log(action);
        templateSections[getTempGridObj].fields[0].table[0].page_size = action.value.page_size;
        templateSections[getTempGridObj].fields[0].table[0].pagination = action.value.pagination;
        templateSections[getTempGridObj].fields[0].table[0].sort_columns = action.value.sort_columns;
        templateSections[getTempGridObj].fields[0].table[0].actions = action.value.actions;
        templateSections[getTempGridObj].fields[0].table[0].default_columns = action.value.default_columns;
        templateSections[getTempGridObj].fields[0].table[0].rows.forEach((rowItem, rowIndex) => {
          templateSections[getTempGridObj].fields[0].table[0].sort_columns.forEach((sItem, sIndex) => {
            if (rowItem.column_name === sItem) {
              rowItem.is_sortable = true;
            }
          });
        })
      }
      return {
        ...state, templateGrid: {
          ...state.templateGrid,
          sections: templateSections
        }
      };
    
    case 'UPDATE_POPUP_GRID_DATA':
      action.value.fItem.table[0].rows[action.value.tabIndex].fieldConfiguration = [];
      action.value.fItem.table[0].rows[action.value.tabIndex].fieldConfiguration.push(new TableTemplate());
      action.value.fItem.table[0].rows[action.value.tabIndex].fieldConfiguration[0].page_size = action.value.page_size;
      action.value.fItem.table[0].rows[action.value.tabIndex].fieldConfiguration[0].pagination = action.value.pagination;
      action.value.fItem.table[0].rows[action.value.tabIndex].fieldConfiguration[0].sort_columns = action.value.sort_columns;
      action.value.fItem.table[0].rows[action.value.tabIndex].fieldConfiguration[0].default_columns = action.value.default_columns;
      return {
        ...state, template: {
          ...state.template
        }
      };

    case 'UPDATE_FLYOUT_GRID_DATA':
      let flyoutGridObj = {
        pagination: action.value.pagination,
        page_size: action.value.page_size,
        sort_columns: action.value.sort_columns,
        default_columns: action.value.default_columns
      }
      action.fObject.table[0] = flyoutGridObj;  
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'ADD_SUB_SECTION':
      const subSecModel = new SubSectionModel();
      subSecModel.sub_section_name = action.value
      action.secItem.sub_sections.push(subSecModel);
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'DELETE_SECTION':
      if (action.mainIndex === null) {
        temItems.sections.splice(action.index, 1);
      } else {
        temItems.sections[action.mainIndex].sub_sections.splice(action.index, 1);
      }
      return {
        ...state, template: {
          ...state.template,
          sections: temItems.sections
        }
      };
    case 'ADD_FIELDS':
      action.secItem.showControlList = !action.secItem.showControlList;
      const fieldModel = new Fields();
      fieldModel.field_type = action.fType;
      // fieldModel.field_data_type = action.colType;
      if (action.fType === 'Table') {
        fieldModel.table.push(new TableTemplate());
        fieldModel.table[0].table_name = action.tableName;
        fieldModel.table[0].user_column_prefs.push(new UserColumnPref());
        fieldModel.table[0].rows.push(new TableRow(), new TableRow(), new TableRow());
      }
      fieldModel.field_sequence_id = action.secItem.fields.length;
      action.secItem.fields.push(fieldModel);
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'ADD_TEMP_CONTROLS':
      console.log("Direct Field Reducer!");
      const fieldTempModel = new Fields();
      fieldTempModel.field_type = action.payload;
      fieldTempModel.field_sequence_id = temItems.fields.length;
      let getDirectFieldSection = temItems.sections.findIndex(sectionName => sectionName.directFieldSection === true)
      if(getDirectFieldSection === -1) {
        let tempSec = new SectionModel();
        tempSec.directFieldSection = true;
        temItems.sections.unshift(tempSec);
        temItems.sections[0].fields.push(fieldTempModel);
      } else {
        temItems.sections[getDirectFieldSection].fields.push(fieldTempModel);
      }
      //temItems.fields.push(fieldTempModel); //directFieldSection
      return {
        ...state, template: {
          ...state.template,
          showHideFieldControls: !temItems.showHideFieldControls
        }
      };
    case 'TOGGLE_ICONS':
      action.secItem.showControlList = !action.secItem.showControlList;
      return {
        ...state, template: {
          ...state.template
        }
      };

      case 'UPDATE_SEARCH_LIST':
        return { ...state, searchList: [...state.searchList, ...action.payload ], firstRender: false};
 
      case 'UPDATE_SEARCH':
        return {...state, searchList : [...action.payload]}
        
    case 'UPDATE_FIELD_NAME':
      action.fObject.field_label = action.value.title;
      action.fObject.field_col_name = action.value.columnname;
      action.fObject.field_data_type = action.value.datatype ? action.value.datatype : "";
      action.fObject.field_max_length = action.value.maxlength ? action.value.maxlength : "";
      console.log('fObject action ', action.value)
      console.log('fObject action 1', action.fObject)
      if (action.value.title === ("SAVE" || "SEARCH") && action.fObject.field_type === "Action Buttons") action.fObject.button_property = true
      else action.fObject.button_property = false
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'UPDATE_FIELD_TYPE':
      action.fObject.field_type = action.value;
      console.log("fIbject =>", action.fObject);
      action.fObject.actions = [];
      action.fObject.fieldNameArr = [];
      action.fObject.fieldValueArr = [];
      action.fObject.sourceGroupArr = [];
      action.fObject.table = [];
      action.fObject.source_group = '';
      if (action.fObject.field_label === ("SAVE" || "SEARCH") && action.value === "Action Buttons") action.fObject.button_property = true
      else action.fObject.button_property = false
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'UPDATE_SOURCE_GROUP':
      action.fObject.source_group = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'UPDATE_MANDATORY':
      action.fObject.mandatory = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'UPDATE_BTN_PRTY':
      action.fObject.button_property = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'OPEN_HIDE_BUTTON_DETAILS':
      action.fObject.showButtonPopup = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'ADD_BUTTON_DETAILS':
      action.fObject.actions.push(new Actions());
      action.fObject.actions[action.fObject.actions.length - 1] = { ...action.value };
      return { ...state };
    case 'RESET':
      temItems.sections = [];
      temItems.fields = [];


      return {
        ...state, template: {
          ...state.template, ...state.sampData,
          sections: temItems.sections,
          fields: temItems.fields,

        }, autoCompleteBoxValue: ''
      };
    case 'DELETE_CONTROL':
      /* action.fArray = action.fArray.filter((obj,i) =>{
        return i !== action.index;
      }); */
      action.fArray.splice(action.index, 1);
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'SHOWHIDE_ICONS':
      action.fObject.showHideIcons = action.value;
      action.fObject.showControlList = false;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'SHOW_CONTROLS':
      action.fObject.showControlList = !action.fObject.showControlList
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'ADD_FIELD_BETWEEN':
      const controlItem = new Fields();
      controlItem.field_type = action.fType;
      // controlItem.field_data_type = action.colType;
      controlItem.field_sequence_id = action.fArray.length;
      action.fArray.splice(action.index + 1, 0, controlItem);
      action.fObject.showControlList = !action.fObject.showControlList;
      console.log('testtttt', state.template.sections, action);
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'CLONE_CONTROL':
      action.fArray.push(Object.assign({}, action.fObject));
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'UPDATE_TABLE_FLYOUT_VISIBLITY':
      console.log('action', action)
      return {
        ...state, template: {
          ...state.template,
          isTableFlyoutOpen: action.flag
        }
      };
    case 'ADD_TABLE_FUNCTIONALITY':
      return {
        ...state, template: {
          ...state.template,
          addTableFunc: action.func
        }
      };
    case 'DELETE_COLUMN':
      //console.log('action', action)
      action.fObject.table[0].rows.splice(action.columnIndex, 1);
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'UPDATE_COLUMN_NAME':
      action.fObject.table[0].rows[action.columnIndex].column_name = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };

    case 'CLEAR_FIELD_NAME':
       //temItems.fields.field_label= ''
      action.fObject.field_type = action.value;
      action.fObject.field_label = '';
      //console.log("clear field name",action.fObject.field_label)
      return {
        ...state, template: {
          ...state.template,
        }
      }

    case 'UPDATE_ADDED_COLUMN_DETAILS':
      action.fObject.table[0].rows[action.columnIndex].column_name = action.value;
      action.fObject.table[0].rows[action.columnIndex].column_header = action.value;
      // action.fObject.table[0].rows[action.columnIndex].column_col_name = "additional_col";
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'SHOW_COLUMN_CONTROLS':
      action.fObject.table[0].rows[action.columnIndex].showControlList = !action.fObject.table[0].rows[action.columnIndex].showControlList
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'ATTACH_FIELD':
      //action.fObject.table[0].rows[action.columnIndex].column_col_name = action.value
      // action.fObject.table[0].rows[action.columnIndex].attachedField = action.value
      // action.fObject.table[0].rows[action.columnIndex].showControlList = !action.fObject.table[0].rows[action.columnIndex].showControlList
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'UPDATE_COLUMN_TYPE':
      action.fObject.table[0].rows[action.columnIndex].column_type = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'UPDATE_COLUMN_SOURCE_GROUP':
      action.fObject.table[0].rows[action.columnIndex].source_group = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'ADD_COLUMN':
      action.fObject.table[0].rows.push(new TableRow())
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'TOGGLE_ORIENTATION_ICONS':
      action.secObj.isShowOrientationIcons = !action.secObj.isShowOrientationIcons;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'UPDATE_ORIENTATION_TYPE':
      action.secObj.orientation = action.oType;
      console.log("Updating orientaion type =>", state);
      return {
        ...state, template: {
          ...state.template
        }
      };

    case 'SHOW_HIDE_FLYOUT_SEARCH_MODAL':
      action.fObject.showFlyoutSearchModal = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };

    case 'OPEN_POPUP':
      action.fObject.showPopup = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };

    case 'SHOW_HIDE_CONFIGURE_FIELDS':
      action.fObject.showConfigureFieldPopup = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'ADD_CONFIGURE_FIELDS':
      action.fObject.holds_state = action.value.holds_state;
      action.fObject.key_field = action.value.key_field;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'HANDLE_SEARCH_SELECT':
      console.log(state);
      return {
        ...state,
        autoCompleteBoxValue: action.result.title
      };
    case 'HANDLE_SEARCH_CHANGE':
      console.log(action)
      let value = action.value.toLowerCase()
      let results;
      console.log(temItems)
      results = state.sampleData.filter(item => {
        console.log( item.title.toLowerCase())
        return item.title.toLowerCase().includes(value)
      });
      console.log(results)
      return { ...state, results: results, autoCompleteBoxValue: action.value }
    case 'ADD_RESULT':
      console.log(action)
      console.log(state);
      return {
        ...state, sampleData: [...state.sampleData, {
          title: action.value
        }],
        autoCompleteBoxValue: ""
      }
    case 'ADD_TECHNOLOGY':
      console.log(state, action.value);
      return {
        ...state, template: {
          ...state.template,
          technology_name: action.value
        }
      };

    case 'UPDATE_TECHNOLOGY':
      console.log(action.value);
      return { ...state, technologyArray: action.value};
    case 'SET_MODULE_DATA':
      let getTemplateGrid = action.payload;
      let setTempGrid = [];
      getTemplateGrid.sections.forEach((secItem, secIndex) => {
        if (secItem.section_name === 'Template Grid') {
          setTempGrid = secItem;
        }
      })
      return { ...state, template: action.payload, templateGrid: setTempGrid }
    case 'UPDATE_FIELD_VALUE':
      console.log(state, action.value);
      console.log(action.fObject);
      action.fObject.field_value = action.value;
      console.log("state", state);
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'RESET_RENDER_DATA':
      temItems.fields.forEach(item => {
        if (item.holds_state) {
          item.field_value = '';
        }
        if (item.field_type === 'Action Buttons' && item.field_label === 'UPDATE') {
          item.field_label = "SAVE";
        }
      });
      temItems.sections.forEach(secItem => {
        secItem.fields.forEach(item => {
          if (item.holds_state) {
            item.field_value = '';
          }
          if (item.field_type === 'Action Buttons' && item.field_label === 'UPDATE') {
            item.field_label = "SAVE";
          }
        });
      });
      temItems.sections.forEach(secItem => {
        secItem.sub_sections.forEach(subItem => {
          subItem.fields.forEach(item => {
            if (item.holds_state) {
              item.field_value = '';
            }
            if (item.field_type === 'Action Buttons' && item.field_label === 'UPDATE') {
              item.field_label = "SAVE";
            }
          });
        });
      });
      return { ...state, template: temItems }
    case 'RESET_BUTTON_NAME':
      if (temItems.fields.length > 0) {
        temItems.fields.forEach((item, index) => {
          if (item.field_type === 'Action Buttons' && item.field_label === 'UPDATE') {
            item.field_label = "SAVE";
          }
        });
      } else if (temItems.sections.length > 0) {
        temItems.sections.forEach((item, i) => {
          item.fields.forEach((fItem, fIndex) => {
            if (fItem.field_type === 'Action Buttons' && fItem.field_label === 'UPDATE') {
              fItem.field_label = "SAVE";
            }
          });
          if (item.sub_sections.length > 0) {
            item.sub_sections.forEach((sItem, sIndex) => {
              sItem.fields.forEach((fItem, fIndex) => {
                if (fItem.field_type === 'Action Buttons' && fItem.field_label === 'UPDATE') {
                  fItem.field_label = "SAVE";
                }
              });
            });
          }
        });
      }
      return { ...state, template: temItems }; 
    case 'GRID_DISPLAY_DATA':
      const obj = action.payload.data;
      const tableNames = temItems.tableName.split(".");
      temItems.render_seq_id = obj[tableNames[1].toLowerCase() + "_id_sq"];
      if (temItems.fields.length > 0) {
        temItems.fields.forEach((item, index) => {
          item.field_value = obj[item.field_col_name];
          if (item.field_type === 'Action Buttons' && item.field_label === 'SAVE') {
            item.field_label = "UPDATE";
          }
        });
      } else if (temItems.sections.length > 0) {
        temItems.sections.forEach((item, i) => {
          item.fields.forEach((fItem, fIndex) => {
            fItem.field_value = obj[fItem.field_col_name];
            if (fItem.field_type === 'Action Buttons' && fItem.field_label === 'SAVE') {
              fItem.field_label = "UPDATE";
            }
          });
          if (item.sub_sections.length > 0) {
            item.sub_sections.forEach((sItem, sIndex) => {
              sItem.fields.forEach((fItem, fIndex) => {
                fItem.field_value = obj[fItem.field_col_name];
                if (fItem.field_type === 'Action Buttons' && fItem.field_label === 'SAVE') {
                  fItem.field_label = "UPDATE";
                }
              });
            });
          }
        });
      }
      return { ...state, template: temItems };
      case 'CREATE_TRIPLE_DROPDOWN':
        console.log(action.fObject);
        action.fObject.sourceGroupArr = action.value.sourceGroupArr;
        action.fObject.fieldNameArr = action.value.fieldNameArr;
       console.log("State =>", state);
        return { ...state };
       case 'CREATE_RADIOBUTTON':
          action.fObject.fieldNameArr = action.value.fieldNameArr;
        return { ...state };
  
    case 'UPDATE_ACTIVE_SIDEBAR':
      return { ...state, activeSideBar: action.value }
    case 'SHOW_HIDE_FIND_TABLE':
      return { ...state, showHideFindTable: true };
    case 'EDIT_TEMPLATE':
      temItems = JSON.parse(action.payload);
      return {
        ...state, template: temItems
      };
    case 'CLONE_TEMPLATE':
      return { ...state, showHideFindTable: true, ...findTemplateAction('CLONE', temItems, action.payload) };
    case 'DELETE_TEMPLATE':
      return { ...state, showHideFindTable: true, ...findTemplateAction('DELETE', temItems, action.payload) };
    case 'CREATE_TEMPLATE':
      return { ...state, 
                find_results: [...state.find_results, action.value],
                template: {...state.template,
                  template_name: action.value.template_name,
                  template_description: action.value.template_description,
                  fields: [],                 
                  sections: []
                },
                templateGrid: []
              };
    case 'SET_DISPLAY_GRID_DATA':
      return { ...state, module_grid_data: action.payload }
    case 'FETCH_FAMILY_NAME':
      return { ...state };
    case 'UPDATE_FAMILY_NAME':
      return { ...state, template: { ...state.template, familyName: action.value } };
    case 'UPDATE_HOLD_STATE':
      console.log("action: ", action);
      action.fObject.holds_state = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'UPDATE_KEY_STATE':
      console.log("action: ", action);
      action.fObject.key_field = action.value;
      return {
        ...state, template: {
          ...state.template
        }
      };
    case 'SHOW_GLOBAL_LOADER':
      console.log(state)
      return {
        ...state, isShowGlobalLoader: true
      }

    case 'HIDE_GLOBAL_LOADER':
      return {
        ...state, isShowGlobalLoader: false
      }

    case 'SELECT_MASTER_DROPDOWN':
      return { ...state, sampleData: action.payload }

    case 'SET_MODULE_CONFIGURATION':
      return {...state , module_definition: action.payload}
    case 'RENDER_VIEW':
      return {
        ...state, template: {...state.template, render_as: action.payload}
      };
    default:
      return state;
  }
};

export default templateReducer; 