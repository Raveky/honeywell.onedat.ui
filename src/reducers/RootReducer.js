import { combineReducers } from 'redux';

import templateReducer from './Template';
import moduleReducer from './ModulePage';

const rootReducer = combineReducers({
    templateReducer,
    moduleReducer
});

export default rootReducer;