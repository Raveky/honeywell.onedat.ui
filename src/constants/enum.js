const ActionDetails = [
    {
        name: "CatalystScale",
        url: "https://catalystscale01.azurewebsites.net/"
    },
    {
        name: "CatalystShape",
        url: "https://catalystshape.azurewebsites.net"
    },
    {
        name: "CatalystSize",
        url: "https://catalystsize.azurewebsites.net"
    },
    {
        name: "CatalystState",
        url: "https://catalyststate.azurewebsites.net"
    },
    {
        name: "CatalystType",
        url: "https://catalysttype.azurewebsites.net"
    },
    {
        name: "CatalystFamily",
        url: "https://catalystfamily.azurewebsites.net/"
    },
    {
        name: "CatalystAlias",
        url: "https://catalystalias01.azurewebsites.net/"
    },
    {
        name: "template_master",
        url: "https://templateapi01.azurewebsites.net/"
    },
    {
        name: "tech_master",
        url: "https://technologyapi.azurewebsites.net/"
    },
    {
        name: "section_master",
        url: "https://sectionapi.azurewebsites.net/"
    },
    {
        name: "module_master",
        url: "https://moduleapi.azurewebsites.net/"
    },
    {
        name: "ManageVariable",
        url: " https://managevarsapi.azurewebsites.net/"
    },
    {
        name: "ManageVCFType",
        url: "https://vcftypeapi.azurewebsites.net/"
    },  
]
export default ActionDetails;