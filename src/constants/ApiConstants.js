export const ApiUrl = {
    getTableNames: "https://tableapi.azurewebsites.net/",
    sectionMasterDropdown :"https://sectionapi.azurewebsites.net/",
    getTableColNames:"https://commoninfoapi.azurewebsites.net/",
    templateAPi:"https://templateapi01.azurewebsites.net/",
    oneDatPostMethod:"https://technologyapi.azurewebsites.net/",
    getModuleConfigurationData:"https://moduleapi.azurewebsites.net/",
    
  };
  