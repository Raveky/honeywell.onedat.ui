import { createStore } from 'redux';

import rootReducer from '../reducers/RootReducer';

const store = createStore(rootReducer);
console.log("Store>>>>>", store);

export default store;