import React from 'react';
import Home from '../containers/Home';
import ModulePage from '../components/ModulePage';
import CreateTemplateForm from '../containers/CreateTemplateForm';
import BuildTemplateForm from '../containers/BuildTemplateForm';
import PreviewTemplate from '../components/PreviewTemplate';
import ModuleConfiguration from '../containers/ModuleConfiguration';
// import ManageCatalystStorage from '../containers/ManageCatalystStorageLocation '

import {
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import TestCreation from '../containers/HWCalculations/TestCreation';
import LogInPage from '../containers/LogInPage';
import CheckRawData from '../containers/HWCalculations/CheckRawData';

const Routes = (props) => {
    //console.log(props);
    return (
        <>            
            <Switch>
                <Route path="/templates">
                    <CreateTemplateForm goHome={props.goHome}></CreateTemplateForm>
                </Route>
                {/*<Route path="/manageCatalystStorage" exact>
                    <ManageCatalystStorage /></Route>*/}
                <Route path="/buildTemplate/:id" render={({ match }) => <BuildTemplateForm goHome={props.goHome} id={match.params.id} />} />
                <Route path="/buildTemplate">
                    <BuildTemplateForm goHome={props.goHome}></BuildTemplateForm>
                </Route>
                <Route path="/buildTemplate/:id">
                    <BuildTemplateForm goHome={props.goHome}></BuildTemplateForm>
                </Route>
                <Route path="/previewTemplate/:id" render={({match})=><PreviewTemplate goHome={props.goHome} id={match.params.id} />} />
                    {/* <PreviewTemplate goHome={props.goHome}></PreviewTemplate>
                </Route> */}
                <Route path="/moduleConfiguration">
                    <ModuleConfiguration goHome={props.goHome}></ModuleConfiguration>
                </Route>
				<Route path="/testCreation">
                    <TestCreation goHome={props.goHome}></TestCreation>
                </Route>
                <Route path="/checkRawData">
                    <CheckRawData goHome={props.goHome}></CheckRawData>
                </Route>
                <Route path="/:name/:id" render={({match})=><ModulePage id={match.params.id} name = {match.params.name}/>} />

                {/* <Route path="/UOMTemplate/:id"  render={({match})=><ModulePage id={match.params.id} />} />

                <Route path="/UOM/:id"  render={({match})=><ModulePage id={match.params.id} />} /> */}

                <Route path="/" exact>
                    <Home />
                </Route>

                <Redirect to="/" />
            </Switch>           
        </>
    )
}

const myRoutePath = [
    {
        path: "Templates",
        url: "/templates"
    },
    {
        path: "Module Configuration",
        url: "/moduleConfiguration"
    },
    {
        path: "Home",
        url: "/"
    },
    {
        path: "UOM",
        url: "/uom/23"
    },
    {
        path: "UOMTemplate",
        url: "/uomtemplate/80"
    },
    {
        path: "HW Calculations",
        url: "/testCreation"
    },
]

export  {Routes, myRoutePath};